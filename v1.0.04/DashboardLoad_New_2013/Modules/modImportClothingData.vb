﻿Imports System.IO.File
Imports System.IO
Imports Microsoft.Office.Interop
Imports System.Data.SqlClient
Imports System.Text

Public Module modImportClothingData

    Private m_DBConn As SqlConnection
    Private Property DBConn() As SqlConnection
        Get
            Return m_DBConn
        End Get
        Set(ByVal value As SqlConnection)
            m_DBConn = value
        End Set
    End Property

    Public Sub ImportClothingData()

        Call ImportClothingSales()
        Call ImportClothingShrink()

    End Sub

    Private Sub ImportClothingShrink()

        '   Import data into Sansbury's Clothing Staging table
        Dim xsRtnMsg As String = ""
        Dim xsClothingShrink As String = ReadField("SELECT CustomValue FROM Admin_Config WHERE CustomName='ClothingShrink'", "")
        Dim xnWSCount As Integer = 0
        Dim xnIPRowCount As Integer = 0
        Dim xnAddCount As Integer = 0
        Dim xbValid As Boolean = False

        Dim xadDates() As Date = Nothing
        Dim xanDateCol() As Integer = Nothing
        Dim xanDateValSales() As Decimal = Nothing
        Dim xasDateCodeCol() As String = Nothing
        'Dim xanDateValExposure() As Decimal = Nothing
        Dim xanDateValClExpAdj() As Decimal = Nothing
        Dim xanDateValRGISExpAdj() As Decimal = Nothing
        Dim xasCountYear() As String = Nothing
        Dim xasPeriodNo() As String = Nothing
        Dim xsOpFilename As String = String.Empty

        Try

            For Each sFile In Directory.GetFiles(xsClothingShrink)

                If sFile.EndsWith("xls") OrElse sFile.EndsWith("xlsx") Then

                    If Not File.Exists(sFile) Then
                        MessageBox.Show("File name not found. Job cancelled.")
                        Exit Sub
                    End If

                    xsOpFilename = sFile.Replace("\ClientData\", "\ClientData_Processed\")

                    '   Clear the work tables
                    DBConn = New SqlConnection(oConn.ConnectionString)
                    Dim xoCmd As New SqlCommand

                    DBConn.Open()


                    '   Open the workbook
                    Dim xoExcel As Excel.Application
                    Dim xoExWS As New Excel.Worksheet
                    Dim xoExWB As Excel.Workbook

                    StatusBarText = "Opening Excel file. "

                    xoExcel = New Excel.Application

                    xoExWB = xoExcel.Workbooks.Open(sFile)
                    xoExcel.Visible = True
                    xoExcel.Application.DisplayAlerts = False

                    StatusBarText = "Processing the worksheets."

                    For Each xoExWS In xoExWB.Worksheets

                        '   Work through each worksheet in the workbook
                        '   There will be a number of sheets for Sales and a number for Shrink
                        '   The different types have different column layouts as the Shrink has 2 columns per day

                        Dim xsWSName As String = xoExWS.Name
                        Dim xbDatesFound As Boolean = False
                        Dim xnDateCount As Integer = 0

                        ReDim xadDates(xnDateCount)
                        ReDim xanDateCol(xnDateCount)
                        ReDim xasDateCodeCol(xnDateCount)
                        ReDim xanDateValSales(xnDateCount)
                        'ReDim xanDateValExposure(xnDateCount)
                        ReDim xanDateValClExpAdj(xnDateCount)
                        ReDim xanDateValRGISExpAdj(xnDateCount)
                        ReDim xasCountYear(xnDateCount)
                        ReDim xasPeriodNo(xnDateCount)

                        '   Determine the sheet type
                        Dim xnWrk As Integer = 0
                        ' txtOutput.Text += xoExWS.Name & " " & vbCrLf
                        StatusBarText = "Processing worksheet " & xoExWS.Name & "."

                        '   Get the date range
                        Dim xsDate As String = ""
                        Dim xnDateStartCol As Integer = 6
                        Dim xnDateStartRow As Integer = 1
                        Dim xnDateEndCol As Integer = 0
                        Dim xnRow As Integer = 1
                        Dim xnCol As Integer = 1
                        Dim xnColForThisDate As Integer = 0

                        '   Find the "Day" row
                        '   The word "Day" appears in the cell preceding the row of dates. This can be in any row from 5-10 and any column from E(5) to K(11)
                        xnRow = 5
                        Dim xbFound As Boolean = False
                        For xnRow = 5 To 10
                            For xnCol = 5 To 11
                                If Not IsNothing(xoExWS.Cells(xnRow, xnCol).value) AndAlso xoExWS.Cells(xnRow, xnCol).value.ToString.ToUpper = "DAY" Then
                                    xbFound = True
                                    Exit For
                                End If
                            Next
                            If xbFound Then Exit For
                        Next

                        '   Confirm that the word "Reason" is two cells below this. We use this to deduce what type of shrink transaction is being given
                        If IsNothing(xoExWS.Cells(xnRow + 2, xnCol).value) OrElse xoExWS.Cells(xnRow + 2, xnCol).value.ToString.ToUpper <> "REASON" Then
                            MessageBox.Show("The 'Reason' row wasn't found two rows below 'Day'. The worksheet " & xsWSName & " is not in the correct format")
                            Exit Sub
                        End If


                        '   Now extract the dates from the cells. There can be 1-3 columns per date with no rhyme or reason as to why (!)
                        '   We need to record what date is relevant to each column so that the figures on each row can be totalled for the table update.
                        '   The assumption is that all columns for a given date are to be added together and that they appear together (i.e. are not jumbled up in the column order)
                        xnDateStartCol = xnCol + 1
                        xnDateStartRow = xnRow
                        Dim xdsvDate As Date = Nothing
                        Dim xdWrk As Date = Nothing
                        xnDateCount = -1

                        For xnCol = xnDateStartCol To 30

                            If IsNothing(xoExWS.Cells(xnRow, xnCol).value) Then
                                Exit For
                            End If

                            xsDate = xoExWS.Cells(xnRow, xnCol).value
                            If xsDate.Length = 0 Or Not IsDate(xsDate) Then
                                Exit For
                            End If

                            xdWrk = CDate(xsDate)
                            If IsNothing(xdsvDate) OrElse xdsvDate <> xdWrk Then

                                xnDateCount += 1
                                ReDim Preserve xadDates(xnDateCount)
                                Dim xdDate As Date = CDate(xsDate)
                                xadDates(xnDateCount) = xdDate
                                xbDatesFound = True
                                xdsvDate = xdWrk

                                '   Get CurrentYear/Period from date
                                ReDim Preserve xasCountYear(xnDateCount)
                                ReDim Preserve xasPeriodNo(xnDateCount)

                                Try
                                    xoCmd = New SqlCommand
                                    xoCmd.Connection = DBConn
                                    xoCmd.CommandType = CommandType.StoredProcedure

                                    xoCmd.CommandText = "p_GetPeriodAndCYForDate"
                                    xoCmd.Parameters.Add("InputDate", SqlDbType.Date).Value = xadDates(xnDateCount)
                                    xoCmd.Parameters.Add("Count_Year", SqlDbType.NVarChar, 10).Direction = ParameterDirection.Output
                                    xoCmd.Parameters.Add("Period_No", SqlDbType.NVarChar, 10).Direction = ParameterDirection.Output
                                    xoCmd.ExecuteNonQuery()

                                    Dim xsCurrentCountYear As String = ""
                                    Dim xsWrk As String = xoCmd.Parameters("Count_Year").Value.ToString
                                    If xsWrk.Length > 0 Then
                                        xasCountYear(xnDateCount) = xsWrk
                                    End If

                                    Dim xsCurrentPeriod As String = ""
                                    xsWrk = xoCmd.Parameters("Period_No").Value.ToString
                                    If xsWrk.Length > 0 Then
                                        xasPeriodNo(xnDateCount) = xsWrk
                                    End If

                                Catch ex As Exception
                                    xsRtnMsg += "Problem when processing header data. Error " & ex.Message & " "
                                End Try
                            End If

                            ReDim Preserve xanDateCol(xnCol)
                            xanDateCol(xnCol) = xnDateCount

                            ReDim Preserve xasDateCodeCol(xnCol)
                            xasDateCodeCol(xnCol) = xoExWS.Cells(xnRow + 2, xnCol).value

                        Next

                        xnDateEndCol = xnCol

                        If Not xbDatesFound Then
                            xsRtnMsg += "No dates found on worksheet " & xsWSName
                            ' txtOutput.Text += "No dates found in " & xsWSName & vbCrLf
                            Exit For
                        End If

                        '   Extract the data
                        Dim xnRowCount As Integer = xoExWS.Rows.Count
                        xnRow = 1
                        Do While xoExWS.Cells(xnRow, 1).value <> "SSL" And xnRow < xnRowCount
                            xnRow += 1
                        Loop

                        If xnRow >= xnRowCount Then
                            ' txtOutput.Text += "No data found in " & xsWSName & vbCrLf
                        End If

                        '   First data row found so process all the following non blank rows
                        Do While xnRow < xnRowCount And xoExWS.Cells(xnRow, 1).value.Length > 0
                            Dim xdDate As Date? = Nothing
                            Dim xsStore As String = ""
                            Dim xnStore As Integer = 0

                            xnIPRowCount += 1

                            'xnRow += 1
                            xsRtnMsg = "a" & xnRow

                            xsStore = xoExWS.Cells(xnRow, 4).value
                            If Not IsNothing(xsStore) And xsStore.Length > 0 Then
                                xnStore = CInt(xsStore)
                                If xnStore > 0 Then
                                    '   Get the figure for each day
                                    '   Note that (at this time) there will be an output row for each day, even for zero values
                                    '   The s-p Inserts if there's no row there or updates if one does exist
                                    '   We work through all columns to accumulate date and then output all dates at one time

                                    ReDim xanDateValClExpAdj(xadDates.GetUpperBound(0))
                                    ReDim xanDateValRGISExpAdj(xadDates.GetUpperBound(0))

                                    For xnCol = xnDateStartCol To xnDateEndCol - 1

                                        Dim xnVal As Decimal = 0
                                        Dim xsWrk As String = xoExWS.Cells(xnRow, xnCol).value
                                        If Not IsNothing(xsWrk) AndAlso xsWrk.Length > 0 Then
                                            xnVal = Val(xsWrk)
                                        End If

                                        'xanDateValExposure(xanDateCol(xnCol)) += xnVal
                                        Select Case xasDateCodeCol(xnCol)
                                            Case Is = "900" : xanDateValClExpAdj(xanDateCol(xnCol)) += xnVal
                                            Case Is = "902" : xanDateValClExpAdj(xanDateCol(xnCol)) += xnVal
                                            Case Is = "903" : xanDateValRGISExpAdj(xanDateCol(xnCol)) += xnVal
                                        End Select

                                    Next

                                    For xnWrk = 0 To xnDateCount '- 1

                                        If DBConn.State = ConnectionState.Closed Then
                                            DBConn.Open()
                                        End If

                                        xoCmd = New SqlCommand
                                        xoCmd.Connection = DBConn
                                        xoCmd.CommandType = CommandType.StoredProcedure

                                        Try
                                            xoCmd.CommandText = "p_StageClothingInsertOrUpdate"
                                            xoCmd.Parameters.Add("MetricDate", SqlDbType.DateTime).Value = xadDates(xnWrk)
                                            xoCmd.Parameters.Add("StoreNumber", SqlDbType.Int).Value = xnStore
                                            ' xoCmd.Parameters.Add("TotalSales", SqlDbType.Decimal).Value = Nothing
                                            xoCmd.Parameters.Add("ClientExposureAdj", SqlDbType.Decimal).Value = xanDateValClExpAdj(xnWrk)
                                            xoCmd.Parameters.Add("RGISExposureAdj", SqlDbType.Decimal).Value = xanDateValRGISExpAdj(xnWrk)
                                            xoCmd.Parameters.Add("Count_Year", SqlDbType.NVarChar).Value = xasCountYear(xnWrk)
                                            xoCmd.Parameters.Add("Period_No", SqlDbType.NVarChar).Value = xasPeriodNo(xnWrk)
                                            xoCmd.Parameters.Add("RowsUpdated", SqlDbType.Int).Direction = ParameterDirection.Output
                                            'xoCmd.Parameters.Add("SqlRtn", SqlDbType.NVarChar).Direction = ParameterDirection.Output
                                            xoCmd.ExecuteNonQuery()

                                            Dim xsWrk As String = xoCmd.Parameters("RowsUpdated").Value.ToString
                                            If xsWrk.Length > 0 AndAlso IsNumeric(xsWrk) Then
                                                Dim xnWrk2 As Integer = CInt(xsWrk)
                                                If xnWrk2 = 1 Then
                                                    xnAddCount += 1
                                                Else
                                                    xsRtnMsg += "Problem during p_StageClothingInsert/Update. "
                                                    xbValid = False
                                                End If
                                            End If
                                        Catch ex As Exception
                                            xsRtnMsg += "Problem when updating data for Row " & xnRow & " in sheet " & xsWSName & ". Error " & ex.Message & " "
                                        End Try

                                    Next


                                    xsRtnMsg += "c"

                                End If
                            End If
                            xnRow += 1
                            If xoExWS.Cells(xnRow, 1).value <> "SSL" Then
                                Exit Do
                            End If
                        Loop

                        'txtOutput.Text += "Worksheet Done. IP Rows=" & xnIPRowCount & ", Added=" & xnAddCount & "." & vbCrLf
                        xnIPRowCount = 0
                        xnAddCount = 0

                    Next

                    xoExWB.Close()
                    xoExcel.Quit()
                    xoExWS = Nothing
                    xoExWB = Nothing
                    xoExcel = Nothing

                    If xsRtnMsg.Length > 0 Then
                        MessageBox.Show(xsRtnMsg)
                    Else
                        File.Move(sFile, xsOpFilename)
                    End If

                End If

            Next

            'txtOutput.Text += "Done. " & vbCrLf

            'MessageBox.Show("Done")

        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message & ": " & xsRtnMsg)
        Finally
            If Not IsNothing(DBConn) AndAlso DBConn.State <> ConnectionState.Closed Then
                DBConn.Close()
            End If

        End Try

    End Sub

    Private Sub ImportClothingSales()

        Try

            '   Import data into Sansbury's Clothing Staging table
            Dim xsRtnMsg As String = ""
            Dim xsClothingSales As String = ReadField("SELECT CustomValue FROM Admin_Config WHERE CustomName='ClothingSales'", "")
            Dim xnWSCount As Integer = 0
            Dim xbValid As Boolean = False

            Dim xadDates() As Date = Nothing
            Dim xanDateCol() As Integer = Nothing
            Dim xasCY() As String = Nothing
            Dim xasPeriod() As String = Nothing
            Dim xanDateValSales() As Decimal = Nothing
            Dim xasDateCodeCol() As String = Nothing
            'Dim xanDateValExposure() As Decimal = Nothing
            Dim xanDateValClExpAdj() As Decimal = Nothing
            Dim xanDateValRGISExpAdj() As Decimal = Nothing
            Dim xsOPFileName As String = String.Empty

            Try

                For Each sFile In Directory.GetFiles(xsClothingSales)

                    If sFile.EndsWith("xls") OrElse sFile.EndsWith("xlsx") Then

                        If Not File.Exists(sFile) Then
                            MessageBox.Show("File name not found. Job cancelled.")
                            Return
                        End If

                        xsOPFileName = sFile.Replace("\ClientData\", "\ClientData_Processed\")

                        '   Clear the work tables
                        DBConn = New SqlConnection(oConn.ConnectionString)
                        Dim xoCmd As New SqlCommand

                        DBConn.Open()

                        '   Open the workbook
                        Dim xoExcel As Excel.Application
                        Dim xoExWS As New Excel.Worksheet
                        Dim xoExWB As Excel.Workbook

                        StatusBarText = "Opening Excel file. "

                        xoExcel = New Excel.Application

                        xoExWB = xoExcel.Workbooks.Open(sFile)
                        xoExcel.Visible = True
                        xoExcel.Application.DisplayAlerts = False

                        StatusBarText = "Processing the worksheets."

                        For Each xoExWS In xoExWB.Worksheets

                            '   Work through each worksheet in the workbook
                            '   There will be a number of sheets for Sales and a number for Shrink
                            '   The different types have different column layouts as the Shrink has 2 columns per day

                            Dim xsWSName As String = xoExWS.Name
                            Dim xbDatesFound As Boolean = False
                            Dim xnDateCount As Integer = 0

                            ReDim xadDates(xnDateCount)
                            ReDim xanDateCol(xnDateCount)
                            ReDim xasDateCodeCol(xnDateCount)
                            ReDim xanDateValSales(xnDateCount)
                            'ReDim xanDateValExposure(xnDateCount)
                            ReDim xanDateValClExpAdj(xnDateCount)
                            ReDim xanDateValRGISExpAdj(xnDateCount)

                            '   Determine the sheet type
                            Dim xnWrk As Integer = 0
                            'txtOutput.Text += xoExWS.Name & " " & vbCrLf
                            StatusBarText = "Processing worksheet " & xoExWS.Name & "."


                            '   Get the date range
                            Dim xsDate As String = ""
                            Dim xnDateStartCol As Integer = 6
                            Dim xnDateStartRow As Integer = 1
                            Dim xnDateEndCol As Integer = 0
                            Dim xnRow As Integer = 1
                            Dim xnCol As Integer = 1
                            Dim xnColForThisDate As Integer = 0

                            '   Find the "Day" row
                            '   The word "Day" appears in the cell preceding the row of dates. This can be in any row from 5-10 and any column from E(5) to K(11)
                            xnRow = 5
                            Dim xbFound As Boolean = False
                            For xnRow = 5 To 10
                                For xnCol = 5 To 11
                                    If Not IsNothing(xoExWS.Cells(xnRow, xnCol).value) AndAlso xoExWS.Cells(xnRow, xnCol).value.ToString.ToUpper = "DAY" Then
                                        xbFound = True
                                        Exit For
                                    End If
                                Next
                                If xbFound Then Exit For
                            Next

                            '   Now extract the dates from the cells. There can be 1-3 columns per date with no rhyme or reason as to why (!)
                            '   We need to record what date is relevant to each column so that the figures on each row can be totalled for the table update.
                            '   The assumption is that all columns for a given date are to be added together and that they appear together (i.e. are not jumbled up in the column order)
                            xnDateStartCol = xnCol + 1
                            xnDateStartRow = xnRow
                            Dim xdsvDate As Date = Nothing
                            Dim xdWrk As Date = Nothing
                            xnDateCount = -1

                            For xnCol = xnDateStartCol To 30

                                If IsNothing(xoExWS.Cells(xnRow, xnCol).value) Then
                                    Exit For
                                End If

                                xsDate = xoExWS.Cells(xnRow, xnCol).value
                                If xsDate.Length = 0 Or Not IsDate(xsDate) Then
                                    Exit For
                                End If

                                xdWrk = CDate(xsDate)
                                If IsNothing(xdsvDate) OrElse xdsvDate <> xdWrk Then

                                    xnDateCount += 1
                                    ReDim Preserve xadDates(xnDateCount)
                                    ReDim Preserve xasCY(xnDateCount)
                                    ReDim Preserve xasPeriod(xnDateCount)
                                    Dim xdDate As Date = CDate(xsDate)
                                    xadDates(xnDateCount) = xdDate
                                    'p_GetPeriodAndCYForDate

                                    xoCmd = New SqlCommand
                                    xoCmd.Connection = DBConn
                                    xoCmd.CommandType = CommandType.StoredProcedure

                                    Try
                                        ' Dim xdReportDate As Date
                                        xoCmd.CommandText = "p_GetPeriodAndCYForDate"
                                        xoCmd.Parameters.Add("InputDate", SqlDbType.Date).Value = xdDate
                                        xoCmd.Parameters.Add("Count_Year", SqlDbType.NVarChar, 10).Direction = ParameterDirection.Output
                                        xoCmd.Parameters.Add("Period_No", SqlDbType.NVarChar, 10).Direction = ParameterDirection.Output
                                        xoCmd.ExecuteNonQuery()

                                        Dim xsCurrentCountYear As String = ""
                                        Dim xsWrk As String = xoCmd.Parameters("Count_Year").Value.ToString
                                        If xsWrk.Length > 0 Then
                                            xsCurrentCountYear = xsWrk
                                        End If

                                        Dim xsCurrentPeriod As String = ""
                                        xsWrk = xoCmd.Parameters("Period_No").Value.ToString
                                        If xsWrk.Length > 0 Then
                                            xsCurrentPeriod = xsWrk
                                        End If

                                        xasCY(xnDateCount) = xsCurrentCountYear
                                        xasPeriod(xnDateCount) = xsCurrentPeriod

                                    Catch ex As Exception
                                        xsRtnMsg += "Problem when processing header data. Error " & ex.Message & " "
                                    End Try


                                    xbDatesFound = True
                                    xdsvDate = xdWrk

                                End If

                                ReDim Preserve xanDateCol(xnCol)
                                xanDateCol(xnCol) = xnDateCount


                            Next

                            xnDateEndCol = xnCol

                            If Not xbDatesFound Then
                                xsRtnMsg += "No dates found on worksheet " & xsWSName
                                'txtOutput.Text += "No dates found in " & xsWSName & vbCrLf
                                Exit For
                            End If

                            '   Extract the data
                            Dim xnRowCount As Integer = xoExWS.Rows.Count
                            xnRow = 1
                            Do While xoExWS.Cells(xnRow, 1).value <> "SSL" And xnRow < xnRowCount
                                xnRow += 1
                            Loop

                            If xnRow >= xnRowCount Then
                                'txtOutput.Text += "No data found in " & xsWSName & vbCrLf
                            End If

                            '   First data row found so process all the following non blank rows
                            Do While xnRow < xnRowCount And xoExWS.Cells(xnRow, 1).value.Length > 0
                                Dim xdDate As Date? = Nothing
                                Dim xsStore As String = ""
                                Dim xnStore As Integer = 0

                                'xnRow += 1
                                'xsRtnMsg = "a" & xnRow

                                xsStore = xoExWS.Cells(xnRow, 4).value
                                If Not IsNothing(xsStore) And xsStore.Length > 0 Then
                                    xnStore = CInt(xsStore)
                                    If xnStore > 0 Then
                                        '   Get the figure for each day
                                        '   Note that (at this time) there will be an output row for each day, even for zero values
                                        '   The s-p Inserts if there's no row there or updates if one does exist
                                        '   We work through all columns to accumulate date and then output all dates at one time

                                        ReDim xanDateValSales(xadDates.GetUpperBound(0))

                                        For xnCol = xnDateStartCol To xnDateEndCol - 1

                                            Dim xnVal As Decimal = 0
                                            Dim xsWrk As String = xoExWS.Cells(xnRow, xnCol).value
                                            If Not IsNothing(xsWrk) AndAlso xsWrk.Length > 0 Then
                                                xnVal = Val(xsWrk)
                                            End If

                                            xanDateValSales(xanDateCol(xnCol)) += xnVal


                                        Next

                                        For xnWrk = 0 To xnDateCount '- 1
                                            xoCmd = New SqlCommand
                                            xoCmd.Connection = DBConn
                                            xoCmd.CommandType = CommandType.StoredProcedure

                                            Try
                                                xoCmd.CommandText = "p_StageClothingInsertOrUpdate"
                                                xoCmd.Parameters.Add("MetricDate", SqlDbType.DateTime).Value = xadDates(xnWrk)
                                                xoCmd.Parameters.Add("StoreNumber", SqlDbType.Int).Value = xnStore
                                                xoCmd.Parameters.Add("TotalSales", SqlDbType.Decimal).Value = xanDateValSales(xnWrk)
                                                xoCmd.Parameters.Add("Count_Year", SqlDbType.NVarChar, 10).Value = xasCY(xnWrk)
                                                xoCmd.Parameters.Add("Period_No", SqlDbType.NVarChar, 10).Value = xasPeriod(xnWrk)
                                                xoCmd.Parameters.Add("RowsUpdated", SqlDbType.Int).Direction = ParameterDirection.Output
                                                xoCmd.ExecuteNonQuery()

                                                Dim xsWrk As String = xoCmd.Parameters("RowsUpdated").Value.ToString
                                                If xsWrk.Length > 0 AndAlso IsNumeric(xsWrk) Then
                                                    Dim xnWrk2 As Integer = CInt(xsWrk)
                                                    If xnWrk2 <> 1 Then
                                                        xsRtnMsg += "Problem during p_StageClothingInsert/Update. "
                                                        xbValid = False
                                                    End If
                                                End If
                                            Catch ex As Exception
                                                xsRtnMsg += "Problem when updating data for row " & xnRow & " in sheet " & xsWSName & ", store " & xnStore & ", date " & xadDates(xnWrk) & _
                                                    ", value " & xanDateValSales(xnWrk) & ". Error " & ex.Message & ". "
                                            End Try

                                            If xsRtnMsg.Length > 0 Then
                                                xsRtnMsg += " Job cancelled."
                                                MessageBox.Show(xsRtnMsg)
                                                'txtOutput.Text += xsRtnMsg
                                                Exit Sub
                                            End If

                                        Next

                                    End If
                                End If
                                xnRow += 1
                                If xoExWS.Cells(xnRow, 1).value <> "SSL" Then
                                    Exit Do
                                End If
                            Loop
                        Next

                        xoExWB.Close()
                        xoExcel.Quit()
                        xoExWS = Nothing
                        xoExWB = Nothing
                        xoExcel = Nothing

                        ' txtOutput.Text += "Done." & vbCrLf

                        'MessageBox.Show("Done")
                        If xsRtnMsg.Length > 0 Then
                            MessageBox.Show(xsRtnMsg)
                        Else
                            File.Move(sFile, xsOPFileName)
                        End If

                    End If

                Next

            Catch ex As Exception
                MessageBox.Show("Error: " & ex.Message & ": " & xsRtnMsg)
            Finally
                If Not IsNothing(DBConn) AndAlso DBConn.State <> ConnectionState.Closed Then
                    DBConn.Close()
                End If

            End Try

        Catch ex As Exception

        End Try

    End Sub

    Public Sub PiqaImport()


        Try

            '   Process the Daily Report

            Dim xnWSCount As Integer = 0
            Dim xsRtnMsg As String = ""
            Dim xbOKToProcess As Boolean = False
            Dim xbProcessThisSheet As Boolean = False
            Dim xsIPFileName As String = ""
            Dim xsOPFileName As String = ""

            Dim xadDates() As Date = Nothing
            Dim xanDateCol() As Integer = Nothing
            Dim xanDateValSales() As Decimal = Nothing
            Dim xasDateCodeCol() As String = Nothing
            'Dim xanDateValExposure() As Decimal = Nothing
            Dim xanDateValClExpAdj() As Decimal = Nothing
            Dim xanDateValRGISExpAdj() As Decimal = Nothing
            Dim sFilename As String = ReadField("SELECT CustomValue FROM Admin_Config WHERE CustomName='PIQA'", "")

            Try

                xsIPFileName = sFilename
                xsOPFileName = xsIPFileName.Replace("\ClientData\", "\ClientData_Processed\")

                If sFilename.Length = 0 Then
                    MessageBox.Show("Missing file name. Job cancelled.")
                    Return
                End If

                If Not File.Exists(sFilename) Then
                    MessageBox.Show("File name not found. Job cancelled.")
                    Return
                End If
            Catch ex As Exception
                MessageBox.Show("Problem getting the excel file . Job cancelled.")
                Return
            End Try


            '   Open the file and confirm it is the right one - by checking the worksheets within it
            '   Open the workbook
            Dim xoExcel As Excel.Application = Nothing
            Dim xoExWS As New Excel.Worksheet
            Dim xoExWB As Excel.Workbook = Nothing

            Try

                StatusBarText = "Opening Excel file. "

                xoExcel = New Excel.Application

                xoExWB = xoExcel.Workbooks.Open(sFilename)
                xoExcel.Visible = True
                xoExcel.Application.DisplayAlerts = False

                StatusBarText = "Processing the worksheets."

                Dim xbFoundME As Boolean = False
                Dim xbFoundCS As Boolean = False
                Dim xbFoundPFS As Boolean = False
                Dim xbFoundPIQA As Boolean = False

                For Each xoExWS In xoExWB.Worksheets

                    '   Work through each worksheet in the workbook
                    '   There are the following w/s expected:
                    '       MAIN ESTATE, C STORE, PFS, PIQA
                    '   There may be more w/s but as long as the above are found we will process the workbook

                    Dim xsWSName As String = xoExWS.Name.ToUpper
                    xbProcessThisSheet = False

                    Select Case xsWSName
                        Case Is = "MAIN ESTATE" : xbFoundME = True
                        Case Is = "C STORE" : xbFoundCS = True
                        Case Is = "PFS" : xbFoundPFS = True
                        Case Is = "PIQA" : xbFoundPIQA = True
                    End Select

                Next

                If Not xbFoundME Or Not xbFoundCS Or Not xbFoundPFS Or Not xbFoundPIQA Then
                    MessageBox.Show("Missing worksheet in workbook. The following are expected:  MAIN ESTATE, C STORE, PFS, PIQA")
                    Return
                End If

                ''   Get the DB Connection string
                ''   Currently this is via the Radio buttons to vary Dev/Live. The client may only be Sainsburys
                'Dim xsClientName As String
                'If radDev.Checked Then
                '    xsClientName = "SSDev"
                'Else
                '    xsClientName = "SS"
                'End If

                'Dim xbValid As Boolean = False
                'Dim xoLic = New RGISLic.LicVal

                'If xoLic.GetDBForClient(xsClientName, "Data Source=smartspacedatacenter.com\BOE140;Initial Catalog=RGIS_Dashboard; Integrated Security=False; User ID=sa; Password=CjC7vHYB2yESaBgq") Then
                '    xbValid = True
                'Else
                '    '   If bad, then exit
                '    MessageBox.Show("Error getting the DB details: " & xoLic.RtnMsg)
                '    Exit Sub
                'End If


                ''   Build the connection string
                'Dim xsConnStr As String = "Data Source=" & xoLic.DBLocn & ";Initial Catalog=" & xoLic.DBName & "; Integrated Security=False; User ID=sa; Password=CjC7vHYB2yESaBgq"

                '   Clear the work tables

                'DBConn = New SqlConnection(xsConnStr)
                DBConn = New SqlConnection(oConn.ConnectionString)
                Dim xoCmd As New SqlCommand

                DBConn.Open()

                '   Process the data in the workbook. Each worksheet contains different types of data.

                For Each xoExWS In xoExWB.Worksheets

                    Dim xsWSName As String = xoExWS.Name.ToUpper
                    Dim xnRow As Integer = 3
                    Dim xnCol As Integer = 1

                    Do While Not IsNothing(xoExWS.Cells(xnRow, xnCol).value) AndAlso xoExWS.Cells(xnRow, xnCol).value.ToString.Length > 0

                        Dim xdDate As DateTime = Nothing

                        If IsDate(xoExWS.Cells(xnRow, xnCol).value) Then
                            xdDate = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                        End If

                        Try
                            xoCmd = New SqlCommand
                            xoCmd.Connection = DBConn
                            xoCmd.CommandType = CommandType.StoredProcedure

                            Select Case xsWSName
                                '   -------------------------------------------------------------------------------------------------------------------------------------------------
                                Case Is = "MAIN ESTATE", "C STORE"
                                    '   Process Main Estate
                                    '   Create and fill the vars
                                    'Dim xdDate As DateTime = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)

                                    xbProcessThisSheet = True
                                    xnCol = 1
                                    Dim xdCountDate As DateTime = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 2
                                    Dim xnDist As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 3
                                    Dim xnStoreNumber As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 4
                                    Dim xsLocation As String = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 5
                                    Dim xnZone As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 6   '   F
                                    Dim xnRegion As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 8   '   H
                                    Dim xnTotalUnitAccuracy As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value * 100)
                                    xnCol = 13   '   M
                                    Dim xnRMVUnitAccuracy As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value * 100)
                                    xnCol = 14
                                    Dim xnTotalSKUAccuracy As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value * 100)
                                    xnCol = 15
                                    Dim xnRMVSKUAccuracy As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value * 100)
                                    xnCol = 16
                                    Dim xnPcntSKUsCheckedInBackRoom As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value * 100)
                                    xnCol = 17
                                    Dim xnPcntSKUsCheckedOnSalesfloor As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value * 100)
                                    xnCol = 18
                                    Dim xnTotalNoUnitsDeletedAtCloseout As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 19
                                    Dim xnNoRMVUsed As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 20  '   T
                                    Dim xnPcntOfAreasChecked As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value * 100)
                                    xnCol = 22  '   V
                                    Dim xnTotalUnitsChecked As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 23
                                    Dim xnNoOfAreasCounted As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 24
                                    Dim xnPcntOfUnitsChecked As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value * 100)
                                    xnCol = 25
                                    Dim xnTotalSKUsChecked As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 26
                                    Dim xnPcntOfSKUsChecked As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value * 100)
                                    xnCol = 27
                                    Dim xdSFStartTime As DateTime = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, CombineDateAndTime(xdCountDate, xoExWS.Cells(xnRow, xnCol).value, Nothing))
                                    xnCol = 28
                                    Dim xdSFFinishTime As DateTime = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, CombineDateAndTime(xdCountDate, xoExWS.Cells(xnRow, xnCol).value, xdSFStartTime))
                                    xnCol = 29
                                    Dim xdLeaveStoreTime As DateTime? = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, CombineDateAndTime(xdCountDate, xoExWS.Cells(xnRow, xnCol).value, xdSFStartTime))
                                    xnCol = 30
                                    Dim xdSFCountLength As Long = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, ConvertDecimalFractionTolong(xoExWS.Cells(xnRow, xnCol).value))
                                    xnCol = 31
                                    Dim xdLastXmitTime As DateTime = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, CombineDateAndTime(xdCountDate, xoExWS.Cells(xnRow, xnCol).value, xdSFStartTime))
                                    xnCol = 32
                                    Dim xdDeltaLastXmitAndSFFinish As Long = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, ConvertDecimalFractionTolong(xoExWS.Cells(xnRow, xnCol).value))
                                    xnCol = 33
                                    Dim xdTotalTimeInStore As Long = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, ConvertDecimalFractionTolong(xoExWS.Cells(xnRow, xnCol).value))
                                    xnCol = 34
                                    Dim xnEstimatesValue As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 35
                                    Dim xnActualValue As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 36
                                    Dim xnVariance As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 37  '   AK
                                    Dim xnVariancePcnt As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value * 100)
                                    xnCol = 39  '   AM
                                    Dim xnPcntOfBackRoom As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value * 100)
                                    xnCol = 40
                                    Dim xnBackRoomPrepGrade As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 41
                                    Dim xnSalesFloorPrepGuide As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 42
                                    Dim xsStoreManagerName As String = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 43
                                    Dim xsRGISSupervisorName As String = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 44  '   AR
                                    Dim xbStoreWalkCompleted As Boolean = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, IIf(xoExWS.Cells(xnRow, xnCol).value = "Y", True, False))
                                    xnCol = 46  '   AT
                                    Dim xbDidStoreRcvFullSetVarianceRpts As Boolean = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, IIf(xoExWS.Cells(xnRow, xnCol).value = "Y", True, False))
                                    xnCol = 47  '   AU
                                    Dim xbEnvLeftWithManager As Boolean = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, IIf(xoExWS.Cells(xnRow, xnCol).value = "Y", True, False))

                                    Try
                                        xoCmd.CommandText = "p_Stage_DailyReport_MainConv_Insert"
                                        xoCmd.Parameters.Add("Worksheet", SqlDbType.NVarChar).Value = xsWSName
                                        xoCmd.Parameters.Add("CountDate", SqlDbType.DateTime).Value = xdCountDate
                                        xoCmd.Parameters.Add("Dist", SqlDbType.Int).Value = xnDist
                                        xoCmd.Parameters.Add("StoreNumber", SqlDbType.Int).Value = xnStoreNumber
                                        xoCmd.Parameters.Add("Location", SqlDbType.NVarChar).Value = xsLocation
                                        xoCmd.Parameters.Add("Zone", SqlDbType.Int).Value = xnZone
                                        xoCmd.Parameters.Add("Region", SqlDbType.Int).Value = xnRegion
                                        xoCmd.Parameters.Add("TotalUnitAccuracy", SqlDbType.Decimal).Value = xnTotalUnitAccuracy
                                        xoCmd.Parameters.Add("RMVUnitAccuracy", SqlDbType.Decimal).Value = xnRMVUnitAccuracy
                                        xoCmd.Parameters.Add("TotalSKUAccuracy", SqlDbType.Decimal).Value = xnTotalSKUAccuracy
                                        xoCmd.Parameters.Add("RMVSKUAccuracy", SqlDbType.Decimal).Value = xnRMVSKUAccuracy
                                        xoCmd.Parameters.Add("PcntSKUsCheckedInBackRoom", SqlDbType.Decimal).Value = xnPcntSKUsCheckedInBackRoom
                                        xoCmd.Parameters.Add("PcntSKUsCheckedOnSalesfloor", SqlDbType.Decimal).Value = xnPcntSKUsCheckedOnSalesfloor
                                        xoCmd.Parameters.Add("TotalNoUnitsDeletedAtCloseout", SqlDbType.Int).Value = xnTotalNoUnitsDeletedAtCloseout
                                        xoCmd.Parameters.Add("NoRMVUsed", SqlDbType.Int).Value = xnNoRMVUsed
                                        xoCmd.Parameters.Add("PcntOfAreasChecked", SqlDbType.Decimal).Value = xnPcntOfAreasChecked
                                        xoCmd.Parameters.Add("TotalUnitsChecked", SqlDbType.Int).Value = xnTotalUnitsChecked
                                        xoCmd.Parameters.Add("NoOfAreasCounted", SqlDbType.Int).Value = xnNoOfAreasCounted
                                        xoCmd.Parameters.Add("PcntOfUnitsChecked", SqlDbType.Decimal).Value = xnPcntOfUnitsChecked
                                        xoCmd.Parameters.Add("TotalSKUsChecked", SqlDbType.Int).Value = xnTotalSKUsChecked
                                        xoCmd.Parameters.Add("PcntOfSKUsChecked", SqlDbType.Decimal).Value = xnPcntOfSKUsChecked
                                        xoCmd.Parameters.Add("SFStartTime", SqlDbType.DateTime).Value = xdSFStartTime
                                        xoCmd.Parameters.Add("SFFinishTime", SqlDbType.DateTime).Value = xdSFFinishTime
                                        xoCmd.Parameters.Add("LeaveStoreTime", SqlDbType.DateTime).Value = IIf(IsNothing(xoExWS.Cells(xnRow, 29).value), Nothing, Date.FromOADate(xoExWS.Cells(xnRow, 29).value)) ' xdLeaveStoreTime
                                        xoCmd.Parameters.Add("SFCountLength", SqlDbType.BigInt).Value = xdSFCountLength
                                        xoCmd.Parameters.Add("LastXmitTime", SqlDbType.DateTime).Value = xdLastXmitTime
                                        xoCmd.Parameters.Add("DeltaLastXmitAndSFFinish", SqlDbType.BigInt).Value = xdDeltaLastXmitAndSFFinish
                                        xoCmd.Parameters.Add("TotalTimeInStore", SqlDbType.BigInt).Value = xdTotalTimeInStore
                                        xoCmd.Parameters.Add("EstimatesValue", SqlDbType.Decimal).Value = xnEstimatesValue
                                        xoCmd.Parameters.Add("ActualValue", SqlDbType.Decimal).Value = xnActualValue
                                        xoCmd.Parameters.Add("Variance", SqlDbType.Decimal).Value = xnVariance
                                        xoCmd.Parameters.Add("VariancePcnt", SqlDbType.Decimal).Value = xnVariancePcnt
                                        xoCmd.Parameters.Add("PcntOfBackRoom", SqlDbType.Decimal).Value = xnPcntOfBackRoom
                                        xoCmd.Parameters.Add("BackRoomPrepGrade", SqlDbType.Int).Value = xnBackRoomPrepGrade
                                        xoCmd.Parameters.Add("SalesFloorPrepGuide", SqlDbType.Int).Value = xnSalesFloorPrepGuide
                                        xoCmd.Parameters.Add("StoreManagerName", SqlDbType.NVarChar).Value = xsStoreManagerName
                                        xoCmd.Parameters.Add("RGISSupervisorName", SqlDbType.NVarChar).Value = xsRGISSupervisorName
                                        xoCmd.Parameters.Add("StoreWalkCompleted", SqlDbType.Bit).Value = xbStoreWalkCompleted
                                        xoCmd.Parameters.Add("DidStoreRcvFullSetVarianceRpts", SqlDbType.Bit).Value = xbDidStoreRcvFullSetVarianceRpts
                                        xoCmd.Parameters.Add("EnvLeftWithManager", SqlDbType.Bit).Value = xbEnvLeftWithManager

                                    Catch ex As Exception
                                        xsRtnMsg += "Problem when building the s-p M+C parms for Row " & xnRow & " in sheet " & xsWSName & ". Error " & ex.Message & " "
                                    End Try


                                    '   -------------------------------------------------------------------------------------------------------------------------------------------------
                                    ' Case Is = "C STORE"
                                    '   Process C Store


                                    '   -------------------------------------------------------------------------------------------------------------------------------------------------
                                Case Is = "PFS"
                                    '   Process PFS

                                    Try
                                        xbProcessThisSheet = True
                                        xnCol = 1
                                        Dim xdCountDate As DateTime? = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 2
                                        Dim xnDist As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 3
                                        Dim xnStoreNumber As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 4
                                        Dim xsLocation As String = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 5
                                        Dim xnZone As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 6
                                        Dim xnRegion As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 7
                                        Dim xnPcntAccuracy As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 8
                                        Dim xnNoOfAreasChecked As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 9
                                        Dim xnPcntOfAreasChecked As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 10
                                        Dim xnNoOfAuditors As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 11
                                        Dim xdSalesfloorStartTime As DateTime? = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, CombineDateAndTime(xdCountDate, xoExWS.Cells(xnRow, xnCol).value, Nothing))
                                        xnCol = 12
                                        Dim xdSalesfloorEndTime As DateTime? = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, CombineDateAndTime(xdCountDate, xoExWS.Cells(xnRow, xnCol).value, xdSalesfloorStartTime))
                                        xnCol = 13
                                        Dim xdLastXmitTime As DateTime? = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, CombineDateAndTime(xdCountDate, xoExWS.Cells(xnRow, xnCol).value, xdSalesfloorStartTime))
                                        xnCol = 14
                                        Dim xdInventoryLength As Long = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, ConvertDecimalFractionTolong(xoExWS.Cells(xnRow, xnCol).value))
                                        xnCol = 15
                                        Dim xnEstimatesHistoric As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 16
                                        Dim xnActualValue As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 17
                                        Dim xnVariance As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 18
                                        Dim xnVariancePcnt As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 19
                                        Dim xnTotalBackroomValue As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 20
                                        Dim xnTotalSalesFloorValue As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 21
                                        Dim xsSupervisorName As String = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 22
                                        Dim xsStoreManagerName As String = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)

                                        xoCmd.CommandText = "p_Stage_DailyReport_PFS_Insert"

                                        xoCmd.Parameters.Add("CountDate", SqlDbType.DateTime).Value = xdCountDate
                                        xoCmd.Parameters.Add("Dist", SqlDbType.Int).Value = xnDist
                                        xoCmd.Parameters.Add("StoreNumber", SqlDbType.Int).Value = xnStoreNumber
                                        xoCmd.Parameters.Add("Location", SqlDbType.NVarChar).Value = xsLocation
                                        xoCmd.Parameters.Add("Zone", SqlDbType.Int).Value = xnZone
                                        xoCmd.Parameters.Add("Region", SqlDbType.Int).Value = xnRegion
                                        xoCmd.Parameters.Add("PcntAccuracy", SqlDbType.Decimal).Value = xnPcntAccuracy
                                        xoCmd.Parameters.Add("NoOfAreasChecked", SqlDbType.Int).Value = xnNoOfAreasChecked
                                        xoCmd.Parameters.Add("PcntOfAreasChecked", SqlDbType.Decimal).Value = xnPcntOfAreasChecked
                                        xoCmd.Parameters.Add("NoOfAuditors", SqlDbType.Int).Value = xnNoOfAuditors
                                        xoCmd.Parameters.Add("SalesfloorStartTime", SqlDbType.DateTime).Value = xdSalesfloorStartTime
                                        xoCmd.Parameters.Add("SalesfloorEndTime", SqlDbType.DateTime).Value = xdSalesfloorEndTime
                                        xoCmd.Parameters.Add("LastXmitTime", SqlDbType.DateTime).Value = xdLastXmitTime
                                        xoCmd.Parameters.Add("InventoryLength", SqlDbType.BigInt).Value = xdInventoryLength
                                        xoCmd.Parameters.Add("EstimatesHistoric", SqlDbType.Decimal).Value = xnEstimatesHistoric
                                        xoCmd.Parameters.Add("ActualValue", SqlDbType.Decimal).Value = xnActualValue
                                        xoCmd.Parameters.Add("Variance", SqlDbType.Decimal).Value = xnVariance
                                        xoCmd.Parameters.Add("VariancePcnt", SqlDbType.Decimal).Value = xnVariancePcnt
                                        xoCmd.Parameters.Add("TotalBackroomValue", SqlDbType.Decimal).Value = xnTotalBackroomValue
                                        xoCmd.Parameters.Add("TotalSalesFloorValue", SqlDbType.Decimal).Value = xnTotalSalesFloorValue
                                        xoCmd.Parameters.Add("SupervisorName", SqlDbType.NVarChar).Value = xsSupervisorName
                                        xoCmd.Parameters.Add("StoreManagerName", SqlDbType.NVarChar).Value = xsStoreManagerName

                                    Catch ex As Exception
                                        xsRtnMsg += "Problem when building the s-p Conv parms for Row " & xnRow & " in sheet " & xsWSName & ". Error " & ex.Message & " "
                                    End Try


                                    '   -------------------------------------------------------------------------------------------------------------------------------------------------
                                Case Is = "PIQA"
                                    '   Process PIQA
                                    Try
                                        xbProcessThisSheet = True
                                        xnCol = 1
                                        Dim xdInventoryDate As DateTime? = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 2
                                        Dim xnDist As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 3
                                        Dim xnStoreNumber As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 4
                                        Dim xsStoreType As String = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 5
                                        Dim xsLocation As String = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 6
                                        Dim xnZone As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 7
                                        Dim xnRegion As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 8
                                        Dim xsStoreContact As String = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 9
                                        Dim xnAccuracy As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 10
                                        Dim xnAdheranceToProcedure As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 11
                                        Dim xnEfficiency As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 12
                                        Dim xnCourtesy As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 13
                                        Dim xnAppearance As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 14
                                        Dim xnRGISSupervisor As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 15
                                        Dim xnRGISOverall As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 16
                                        Dim xnPIQAAvge As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                        xnCol = 17
                                        Dim xsComments As String = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)

                                        xoCmd.CommandText = "p_Stage_DailyReport_PIQA_Insert"

                                        xoCmd.Parameters.Add("InventoryDate", SqlDbType.DateTime).Value = xdInventoryDate
                                        xoCmd.Parameters.Add("Dist", SqlDbType.Int).Value = xnDist
                                        xoCmd.Parameters.Add("StoreNumber", SqlDbType.Int).Value = xnStoreNumber
                                        xoCmd.Parameters.Add("StoreType", SqlDbType.NVarChar).Value = xsStoreType
                                        xoCmd.Parameters.Add("Location", SqlDbType.NVarChar).Value = xsLocation
                                        xoCmd.Parameters.Add("Zone", SqlDbType.Int).Value = xnZone
                                        xoCmd.Parameters.Add("Region", SqlDbType.Int).Value = xnRegion
                                        xoCmd.Parameters.Add("StoreContact", SqlDbType.NVarChar).Value = xsStoreContact
                                        xoCmd.Parameters.Add("Accuracy", SqlDbType.Int).Value = xnAccuracy
                                        xoCmd.Parameters.Add("AdheranceToProcedure", SqlDbType.Int).Value = xnAdheranceToProcedure
                                        xoCmd.Parameters.Add("Efficiency", SqlDbType.Int).Value = xnEfficiency
                                        xoCmd.Parameters.Add("Courtesy", SqlDbType.Int).Value = xnCourtesy
                                        xoCmd.Parameters.Add("Appearance", SqlDbType.Int).Value = xnAppearance
                                        xoCmd.Parameters.Add("RGISSupervisor", SqlDbType.Int).Value = xnRGISSupervisor
                                        xoCmd.Parameters.Add("RGISOverall", SqlDbType.Int).Value = xnRGISOverall
                                        xoCmd.Parameters.Add("PIQAAvge", SqlDbType.Decimal).Value = xnPIQAAvge
                                        xoCmd.Parameters.Add("Comments", SqlDbType.NVarChar).Value = xsComments

                                    Catch ex As Exception
                                        xsRtnMsg += "Problem when building the s-p PIQA parms for Row " & xnRow & " in sheet " & xsWSName & ". Error " & ex.Message & " "
                                    End Try

                                Case Else
                                    'MessageBox.Show("Unexpected worksheet found: " & xsWSName)
                                    Exit For
                            End Select

                            If xbProcessThisSheet Then
                                xoCmd.Parameters.Add("RowsUpdated", SqlDbType.Int).Direction = ParameterDirection.Output
                                xoCmd.ExecuteNonQuery()

                                Dim xsWrk As String = xoCmd.Parameters("RowsUpdated").Value.ToString
                                If xsWrk.Length > 0 AndAlso IsNumeric(xsWrk) Then
                                    Dim xnWrk2 As Integer = CInt(xsWrk)
                                    If xnWrk2 <> 1 Then
                                        Throw New Exception("Problem during p_Stage_DailyReport_Insert.")
                                    End If
                                End If
                            End If

                        Catch ex As Exception
                            xsRtnMsg += "Problem when updating data for Row " & xnRow & " in sheet " & xsWSName & ". Error " & ex.Message & " "
                            MessageBox.Show("Error: " & xsRtnMsg)
                            Throw New Exception(xsRtnMsg)
                        End Try

                        xnRow += 1
                        xnCol = 1
                    Loop

                Next

                '   The assumption is that if we've got this far, then we're OK to move the file!
                xbOKToProcess = True

                MessageBox.Show("Done")

            Catch ex As Exception
                MessageBox.Show("Error: " & ex.Message & ": " & xsRtnMsg)
            Finally
                If Not IsNothing(DBConn) AndAlso DBConn.State <> ConnectionState.Closed Then
                    DBConn.Close()
                End If

                If Not IsNothing(xoExWB) Then
                    xoExWB.Close()
                    xoExcel.Quit()
                    xoExWS = Nothing
                    xoExWB = Nothing
                    xoExcel = Nothing
                End If

                '   Move file to Processed folder
                If xbOKToProcess Then
                    File.Move(xsIPFileName, xsOPFileName)
                End If
                StatusBarText = "Done"
            End Try

        Catch ex As Exception
            MsgBox("ImportPIQA: " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Function CombineDateAndTime(ByVal dDate As Date?, ByVal sTime As String, ByVal dFromDate As Date?) As Date?

        Dim xdRtn As Date? = Nothing

        ' Combine two separate fields into a single date/time field. If either inputs are blank a blank is returned.
        '   If the From date is give, then it means that the inbound time is related to a previous one. It is possible that the new time
        '   is on another day so we need to work out what day to use. The assumption is that a count won't take more than 24 hrs.
        If Not IsNothing(dDate) AndAlso Not IsDBNull(dDate) AndAlso Not IsDBNull(sTime) AndAlso Not IsNothing(sTime) Then
            Dim xdDate As Date
            Dim xdTime As Date
            Dim xdDateFrom As Date
            Dim xdDateTo As Date
            xdDate = dDate

            Dim xnWrk As Integer = sTime.IndexOf(":")
            If xnWrk > 0 Then
                xdTime = CType(sTime, Date)
                Dim xnWrk2 As Decimal
                Dim xnWrk3 As Decimal
                xnWrk2 = xdTime.ToOADate
                xnWrk3 = xdDate.ToOADate + xnWrk2
                xdDate = Date.FromOADate(xnWrk3)
                xdRtn = xdDate
            Else
                Dim xnWrk4 As Decimal
                Dim xnWrk5 As Decimal
                xnWrk4 = CType(sTime, Decimal)
                xnWrk5 = xdDate.ToOADate + xnWrk4
                xdDate = Date.FromOADate(xnWrk5)
                xdRtn = xdDate
            End If

            If Not IsNothing(dFromDate) Then
                '   If the new date is prior to the FromDate then we need to increment the day
                xdDateFrom = dFromDate
                xdDateTo = xdRtn
                'xdDateOut
                If DateDiff(DateInterval.Minute, xdDateFrom, xdDateTo) < 0 Then
                    xdDateTo = DateAdd(DateInterval.Day, 1, xdDateTo)
                    xdRtn = xdDateTo
                End If
            End If

        End If

        Return xdRtn
    End Function

    Private Function ConvertDecimalFractionToLong(nNumber As Decimal) As Long

        Dim xnRtn As Long = 0
        Dim xsWrkIn As String = ""
        Dim xsWrkOut As String = ""

        If nNumber <> 0 Then
            Dim xnWrkI As Long = Math.Truncate(nNumber)
            Dim xnWrkD As Decimal = nNumber - xnWrkI
            xsWrkIn = CType(xnWrkD, String)
            For xnCount As Integer = 2 To xsWrkIn.Length - 1
                xsWrkOut += xsWrkIn.Substring(xnCount, 1)
            Next
            If xsWrkOut.Length > 0 Then
                Dim xdDate As Date = Date.FromOADate(nNumber)
                Dim xnHH As Integer = xdDate.Hour
                Dim xnMM As Integer = xdDate.Minute
                Dim xnSS As Integer = xdDate.Second
                Dim xoTS As New TimeSpan(xnHH, xnMM, xnSS)
                'xnRtn = CType(xsWrkOut, Long) '* DateTime.ticksperday
                xnRtn = xoTS.Ticks
            End If
        End If

        Return xnRtn
    End Function

End Module
