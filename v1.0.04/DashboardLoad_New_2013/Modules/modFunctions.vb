﻿Imports System.Data.SqlClient

Module modFunctions

    Public Property StatusBarText() As String
        Get
            Return frmMain.lblStatus.Text
        End Get
        Set(ByVal value As String)
            frmMain.lblStatus.Text = value
            System.Windows.Forms.Application.DoEvents()
        End Set
    End Property

#Region "Subs"

    Friend Sub SaveConnectionToRegistry()

        Try

            If oConn.ConnectionString.Length > 0 Then

                SaveSetting(sKeyName, sSectionName, "ConnString", EncryptString(oConn.ConnectionString))
                SaveSetting(sKeyName, sSectionName, "Database", EncryptString(oConn.Database))
                SaveSetting(sKeyName, sSectionName, "Provider", EncryptString(oConn.Provider))
                SaveSetting(sKeyName, sSectionName, "Trusted", EncryptString(oConn.TrustedConnection))
                SaveSetting(sKeyName, sSectionName, "User", EncryptString(oConn.User))
                SaveSetting(sKeyName, sSectionName, "Pwd", EncryptString(oConn.Pwd))
                SaveSetting(sKeyName, sSectionName, "Server", EncryptString(oConn.Server))

            End If

        Catch ex As Exception

        End Try

    End Sub

#End Region

#Region "Functions"

    Friend Function executeQuery(ByVal connection As String, ByVal query As String) As Integer

        Dim conn As System.Data.IDbConnection = Nothing
        Dim cmd As IDbCommand = Nothing
        Dim trans As IDbTransaction = Nothing
        Dim iRes As Int16

        Try

            conn = New SqlConnection
            cmd = New SqlCommand

            conn.ConnectionString = connection
            conn.Open()
            trans = conn.BeginTransaction()

            cmd.CommandText = query
            cmd.Connection = conn
            cmd.Transaction = trans
            cmd.CommandTimeout = 0

            iRes = cmd.ExecuteScalar
            trans.Commit()
            conn.Close()
        Catch ex As Exception
            If Not trans Is Nothing Then trans.Rollback()
            iRes = -1
            Throw New ArgumentException(ex.Message)

        Finally
            executeQuery = iRes
        End Try

    End Function


    Friend Function ExcelCommaFix(ByVal sData() As String, iMaxCount As Integer, iType As eType) As String()

        Dim sRet() As String = sData
        Dim iItemDescCol As Integer = If(iType = eType.ePCVAR, ePCVarColumns.eItemDesc, eOut16Columns.eItemDesc)

        Try

            ' SC - 31/01/2013
            'Clears blanks at array end (some files have ,,, at end (?))
            For i = sRet.GetUpperBound(0) To iMaxCount Step -1
                If (sRet(i).Length = 0) Then
                    ReDim Preserve sRet(sRet.GetUpperBound(0) - 1)
                End If
            Next

            Do Until sRet.GetUpperBound(0) = iMaxCount

                sRet(iItemDescCol) = sRet(iItemDescCol) & "," & sRet((iItemDescCol + 1))

                For i = (iItemDescCol + 1) To sRet.GetUpperBound(0) - 1
                    sRet(i) = sRet(i + 1)
                Next

                ReDim Preserve sRet(sRet.GetUpperBound(0) - 1)
            Loop

        Catch ex As Exception
        Finally
            ExcelCommaFix = sRet
        End Try

    End Function

    Friend Function SqlQuery(ByVal sConnection As String, ByVal sSql As String) As DataTable
        Dim conn As IDbConnection = New SqlConnection
        Dim adapter As New SqlDataAdapter
        Dim DataTable As New DataTable
        Dim cmd As IDbCommand = New SqlCommand

        Try

            conn.ConnectionString = sConnection
            conn.Open()

            cmd.CommandText = sSql

            cmd.CommandTimeout = 1000

            cmd.Connection = conn
            adapter.SelectCommand = cmd

            DataTable.Clear()
            adapter.Fill(DataTable)

        Catch ex As Exception
            Throw ex
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            SqlQuery = DataTable
        End Try

    End Function

    Friend Function ReadField(ByVal query As String, Optional ByVal sReturnValue As Object = Nothing) As Object

        Dim conn As IDbConnection 'Database Connection
        Dim adapter ' As IDbDataAdapter
        Dim dTable As New DataTable
        Dim cmd As IDbCommand 'Query Command
        Dim sField As Object = sReturnValue

        Try

            conn = New SqlConnection
            cmd = New SqlCommand
            adapter = New SqlDataAdapter

            conn.ConnectionString = oConn.ConnectionString 'oProfile.Database.ConnectionString  '.uConnection 'Get current connection String
            conn.Open()

            'Put Query Text into cmd
            cmd.CommandText = query

            cmd.CommandTimeout = 1000

            'Execute Query
            cmd.Connection = conn
            adapter.SelectCommand = cmd

            'Fill DataSet
            dTable.Clear()
            adapter.Fill(dTable)

            conn.Close()

            Try
                sField = dTable.Rows(0).Item(0)
            Catch ex As Exception
            End Try

        Catch ex As Exception 'Execution Failed
            sField = sReturnValue
            'Throw New ArgumentException(ex.Message)
            'DisplayErrorMessage(ex, "SelectQuery", Erl)
        Finally
            ReadField = sField
        End Try
    End Function


    Friend Function EncryptString(ByVal strEncrypted As String) As String
        Try
            Dim b As Byte() = System.Text.ASCIIEncoding.ASCII.GetBytes(strEncrypted)
            Dim encryptedConnectionString As String = Convert.ToBase64String(b)
            Return encryptedConnectionString
        Catch
            Throw
        End Try
    End Function

    Friend Function DecryptString(ByVal encrString As String) As String
        Try
            Dim b As Byte() = Convert.FromBase64String(encrString)
            Dim decryptedConnectionString As String = System.Text.ASCIIEncoding.ASCII.GetString(b)
            Return decryptedConnectionString
        Catch
            Throw
        End Try
    End Function

    Friend Function CreateDataTable() As DataTable

        Dim dTable As New DataTable

        Try

            dTable.Columns.Add("Inventory_Date", Type.GetType("System.DateTime"))
            dTable.Columns.Add("Store_Number", Type.GetType("System.Double"))
            dTable.Columns.Add("RGIS_Dist", Type.GetType("System.Double"))
            dTable.Columns.Add("Zone", Type.GetType("System.Double"))
            dTable.Columns.Add("Region", Type.GetType("System.Double"))
            dTable.Columns.Add("SKU", Type.GetType("System.Double"))
            dTable.Columns.Add("Product_Desc", Type.GetType("System.String"))
            dTable.Columns.Add("Retail_Price_By_Unit", Type.GetType("System.Double"))
            dTable.Columns.Add("SuperCat", Type.GetType("System.Double"))
            dTable.Columns.Add("SubCat", Type.GetType("System.Double"))
            dTable.Columns.Add("On_Hand_Qty", Type.GetType("System.Double"))
            dTable.Columns.Add("Actual_Qty", Type.GetType("System.Double"))
            dTable.Columns.Add("No_Of_Units_From_Backroom", Type.GetType("System.Double"))
            dTable.Columns.Add("Exclude", Type.GetType("System.Boolean"))
            ' JR - 05/07/2017
            ' Added new Trial_Data column
            dTable.Columns.Add("Trial_Data", Type.GetType("System.Int32"))
            ' JR - 01/08/2017
            ' Added new Additional_Count column
            dTable.Columns.Add("Additional_Count", Type.GetType("System.Int32"))

            dTable.TableName = "DataTable"

        Catch ex As Exception
            MsgBox("CreateDataTable: " & ex.Message, MsgBoxStyle.Critical)
        Finally
            CreateDataTable = dTable
        End Try

    End Function

#End Region

    Public Function GenerateConnectionString(sServerName As String, sDbName As String, sUser As String, sPw As String, bTrusted As Boolean) As String
        Try
            Dim sStr As String = "Server = " & sServerName

            sStr = sStr & ";Database = " & sDbName
            sStr = sStr & ";UID = " & sUser
            sStr = sStr & ";PWD = " & sPw
            sStr = sStr & ";Trusted_Connection = " & bTrusted

            Return sStr
        Catch ex As Exception
            Return ""
        End Try
    End Function

End Module
