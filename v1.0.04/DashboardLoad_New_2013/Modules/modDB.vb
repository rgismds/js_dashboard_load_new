Imports System.Data.SqlClient
Imports System.Data.OleDb

Module modDB

#Region "Connection and Dataset functions"
    'Function to create a connection
    Public Function CreateConnection(ByVal sConnString As String, ByRef oConn As Data.IDbConnection, ByVal sProvider As String) As Boolean
        Dim bOk As Boolean = False
        Try

            Select Case sProvider.ToUpper
                Case "MSDASQL"
                    oConn = New SqlConnection
                Case "SQLOLEDB"
                    oConn = New OleDbConnection
                Case "ORACLE"
                    'oConn = New OracleClient.OracleConnection
                Case Else
                    oConn = New SqlConnection
            End Select

            oConn.ConnectionString = sConnString
            oConn.Open()

            bOk = True

        Catch ex As Exception

        Finally
            CreateConnection = bOk
        End Try
    End Function

    '
    Public Function GetDataTable(ByVal oConnInfo As clsConnection, ByVal sSQL As String) As DataTable
        Try
            Dim oConn As IDbConnection = Nothing

            'Create connection
            Call modDB.CreateConnection(oConnInfo.ConnectionString, oConn, oConnInfo.Provider)

            Dim ds As New DataSet

            If modDB.GetDataSetWithConn(sSQL, oConn, ds, oConnInfo.Provider) Then
                '2.2.5
                oConn.Close()
                oConn.Dispose()
                Return ds.Tables(0)
            Else
                '2.2.5
                oConn.Close()
                oConn.Dispose()
                Return Nothing
            End If
        Catch ex As Exception
            Throw New Exception(Err.Description)
        End Try
    End Function
    Public Function GetDataTableWithConn(ByVal oConn As IDbConnection, ByVal sSQL As String, ByVal sProvider As String) As DataTable
        Try
            Dim ds As New DataSet
            If modDB.GetDataSetWithConn(sSQL, oConn, ds, sProvider) Then
                Return ds.Tables(0)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Throw New Exception(Err.Description)
        End Try
    End Function

    Public Function GetDataSetWithConn(ByVal sSQL As String, ByVal oConn As IDbConnection, ByRef ds As DataSet, ByVal sProvider As String) As Boolean
        Try
            Dim adapter As IDbDataAdapter
            Dim cmd As IDbCommand   'Query Command
            Select Case sProvider.ToUpper
                Case "MSDASQL"
                    adapter = New SqlDataAdapter
                    cmd = New SqlCommand 'Query Command
                Case "SQLOLEDB"
                    adapter = New OleDbDataAdapter
                    cmd = New OleDbCommand 'Query Command
                Case Else
                    Return False
            End Select

            cmd.CommandText = sSQL

            'Execute Query
            cmd.Connection = oConn
            cmd.CommandTimeout = 300
            adapter.SelectCommand = cmd

            'Fill DataSet
            ds.Clear()
            adapter.Fill(ds)

            Return True

        Catch ex As Exception 'Execution Failed
            Throw New Exception("Error getting information from the database." & vbCrLf & vbCrLf & ex.Message.ToString)
        End Try
    End Function

    'Public Function GetDataSet(ByVal sSQL As String, ByRef ds As DataSet, ByVal sConnString As String, ByVal sProvider As String) As Boolean
    '    Try
    '        Dim adapter As IDbDataAdapter
    '        Dim cmd As IDbCommand   'Query Command
    '        Dim oConn As IDbConnection = Nothing

    '        Select Case sProvider.ToUpper
    '            Case "MSDASQL"
    '                adapter = New SqlDataAdapter
    '                cmd = New SqlCommand 'Query Command
    '            Case "SQLOLEDB"
    '                adapter = New OleDbDataAdapter
    '                cmd = New OleDbCommand 'Query Command
    '            Case Else
    '                Return False
    '        End Select

    '        If modDB.CreateConnection(sConnString, oConn, sProvider) Then
    '            cmd.CommandText = sSQL

    '            'Execute Query
    '            cmd.Connection = oConn
    '            cmd.CommandTimeout = 300
    '            adapter.SelectCommand = cmd

    '            'Fill DataSet
    '            ds.Clear()
    '            adapter.Fill(ds)

    '            Return True
    '        End If
    '    Catch ex As Exception 'Execution Failed
    '        Throw New Exception("Error getting information from the database." & vbCrLf & vbCrLf & ex.Message.ToString)
    '    End Try
    'End Function

    Public Sub ExecuteSQL(ByVal sSQL As String, ByVal oConn As IDbConnection, ByVal sProvider As String)
        Dim adapter As IDbDataAdapter
        Dim cmd As IDbCommand = Nothing   'Query Command
        Try

            Select Case sProvider.ToUpper
                Case "MSDASQL"
                    adapter = New SqlDataAdapter
                    cmd = New SqlCommand(sSQL, oConn) 'Query Command
                Case "SQLOLEDB"
                    adapter = New OleDbDataAdapter
                    cmd = New OleDbCommand(sSQL, oConn) 'Query Command
                Case Else
                    Throw New Exception("")
            End Select

            cmd.Connection = oConn
            cmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw New Exception("Error executing command." & vbCrLf & vbCrLf & ex.Message.ToString)
        End Try
    End Sub

    Public Function ExecuteSQLReturnIdentity(ByVal sSQL As String, ByVal oConn As IDbConnection, ByVal sProvider As String) As String
        Dim sID As String
        Try
            Dim adapter As IDbDataAdapter
            Dim cmd As IDbCommand   'Query Command

            Select Case sProvider.ToUpper
                Case "MSDASQL"
                    adapter = New SqlDataAdapter
                    cmd = New SqlCommand(sSQL, oConn) 'Query Command
                Case "SQLOLEDB"
                    adapter = New OleDbDataAdapter
                    cmd = New OleDbCommand(sSQL, oConn) 'Query Command
                Case Else
                    Throw New Exception("")
            End Select

            cmd.Connection = oConn
            sID = cmd.ExecuteScalar
            Return sID
        Catch ex As Exception
            Throw New Exception("Error executing command." & vbCrLf & vbCrLf & ex.Message.ToString)
        End Try
    End Function

    'Public Function ConvertConnectionStringToSQL(ByVal Connection As clsConnection) As String
    '    Dim sConnStr As String = ""
    '    Dim dec As New STPL_Encryption.clsEncryption
    '    Dim sPWd As String
    '    Dim sServerName As String
    '    Try

    '        If Connection.Server.ToUpper = "LOCAL" Then
    '            sServerName = "(Local)"
    '        Else
    '            sServerName = Connection.Server
    '        End If

    '        sConnStr = "Data Source = " & sServerName & ";" & _
    '            "Initial Catalog=" & Connection.Database & ";"

    '        If Connection.TrustedConnection Then
    '            sConnStr = sConnStr & "Integrated Security=SSPI"
    '        Else
    '            sPWd = dec.Decrypting_Data(Connection.Pwd, modGlobals.sPW)
    '            sConnStr = sConnStr & "User ID=" & Connection.User & ";" & _
    '                                  "Password=" & sPWd & ";"
    '        End If
    '    Catch ex As Exception
    '    Finally
    '        ConvertConnectionStringToSQL = sConnStr
    '    End Try
    'End Function
#End Region


End Module


