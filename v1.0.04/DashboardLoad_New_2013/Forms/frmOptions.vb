﻿Public Class frmOptions

#Region "Privates"
    Dim bChanged As Boolean = False
    Dim bLoading As Boolean = True
#End Region

    Private Sub AddToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AddToolStripMenuItem.Click
        Call AddFilterToListview()
    End Sub

    Private Sub EditToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles EditToolStripMenuItem.Click
        Call EditFilterInListview()
    End Sub

    Private Sub DeleteToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles DeleteToolStripMenuItem.Click
        Call DeleteFilterInListview()
    End Sub

    Private Sub mnuFilters_Opening(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles mnuFilters.Opening

        Try

            EditToolStripMenuItem.Enabled = lvFilters.SelectedItems.Count = 1
            DeleteToolStripMenuItem.Enabled = lvFilters.SelectedItems.Count = 1

        Catch ex As Exception

        End Try

    End Sub

    Private Sub EditFilterInListview()

        Try

            lvFilters.SelectedItems(0).BeginEdit()

        Catch ex As Exception
        Finally
            If Not bLoading Then bChanged = True
        End Try

    End Sub

    Private Sub AddFilterToListview()

        Try

            Dim sNewFilter As String = InputBox("Enter new filter: ", "New Filter", "*.csv").Trim

            If (sNewFilter.Length <> 0) AndAlso (NotADuplicate(sNewFilter)) Then

                Dim lvItem As New ListViewItem(sNewFilter)
                lvFilters.Items.Add(lvItem)

                bChanged = True

            Else
                MsgBox("Filter already exists below!", MsgBoxStyle.OkOnly + MsgBoxStyle.Information, "New Filter")
            End If


        Catch ex As Exception
        Finally

        End Try

    End Sub

    Private Function NotADuplicate(sFilter As String) As Boolean

        Dim bDuplicate As Boolean = False

        Try

            For Each lvItem As ListViewItem In lvFilters.Items
                If sFilter = lvItem.Text Then Exit Try
            Next

            bDuplicate = True

        Catch ex As Exception
        Finally
            NotADuplicate = bDuplicate
        End Try

    End Function

    Private Sub dtpStart_ValueChanged(sender As System.Object, e As System.EventArgs) Handles dtpStart.ValueChanged
        Try
            txtStartDate.Text = dtpStart.Value.ToString("yyyy-MM-dd")
        Catch ex As Exception
        Finally
            If Not bLoading Then bChanged = True
        End Try
    End Sub

    Private Sub dtpEnd_ValueChanged(sender As System.Object, e As System.EventArgs) Handles dtpEnd.ValueChanged
        Try
            txtEndDate.Text = dtpEnd.Value.ToString("yyyy-MM-dd")
        Catch ex As Exception
        Finally
            If Not bLoading Then bChanged = True
        End Try
    End Sub

    Private Sub DeleteFilterInListview()

        Try

            lvFilters.SelectedItems(0).Remove()

        Catch ex As Exception
        Finally
            bChanged = True
        End Try

    End Sub

    Private Sub lvFilters_AfterLabelEdit(sender As Object, e As System.Windows.Forms.LabelEditEventArgs) Handles lvFilters.AfterLabelEdit

        Try

            If (e.Label.Trim.Length > 0) AndAlso (lvFilters.SelectedItems(0).Text <> e.Label.Trim) AndAlso (NotADuplicate(e.Label.Trim)) Then
                lvFilters.SelectedItems(0).Text = e.Label.Trim
            Else
                MsgBox("Filter already exists below!", MsgBoxStyle.OkOnly + MsgBoxStyle.Information, "Edit Filter")
                e.CancelEdit = True
            End If

        Catch ex As Exception
        Finally
            bChanged = True
        End Try

    End Sub

    Private Sub CloseForm(e As System.Windows.Forms.FormClosingEventArgs)

        Try
            If Not EverythingIsFilledIn() Then
                MsgBox("Please fill everything in!", MsgBoxStyle.Exclamation)
                e.Cancel = True
                Exit Sub
            End If

            If bChanged Then
                If MsgBox("Do you want to save changes?", MsgBoxStyle.YesNo, "Save Changes?") = MsgBoxResult.Yes Then
                    Call Save()
                End If
            End If

        Catch ex As Exception
        Finally
            bChanged = False
        End Try

    End Sub

    Private Sub Save()

        Try

            If Not EverythingIsFilledIn() Then MsgBox("Please fill everything in!", MsgBoxStyle.Exclamation) : Exit Sub

            Dim oUpdate As New clsUpdate

            With oUpdate

                'TA Start Date
                .Reset()
                .TableName("Admin_Config")
                .BuildUpate("CustomValue", CDate(txtStartDate.Text).ToString("yyyy-MM-dd 00:00:00"))
                .Where("WHERE CustomName='" & TA_START_DATE_COL & "'")
                Call executeQuery(oConn.ConnectionString, .GetSql)

                'TA End Date
                .Reset()
                .TableName("Admin_Config")
                .BuildUpate("CustomValue", CDate(txtEndDate.Text).ToString("yyyy-MM-dd 23:59:59"))
                .Where("WHERE CustomName='" & TA_END_DATE_COL & "'")
                Call executeQuery(oConn.ConnectionString, .GetSql)

                sStartTA = CDate(txtStartDate.Text).ToString("yyyy-MM-dd")
                sEndTA = CDate(txtEndDate.Text).ToString("yyyy-MM-dd")

            End With

            Dim oInsert As New clsInsert

            Call executeQuery(oConn.ConnectionString, "DELETE FROM Admin_Config WHERE CustomName LIKE 'FilterLV%'")
            Call executeQuery(oConn.ConnectionString, "DELETE FROM Admin_Config WHERE CustomName LIKE 'DataLoadDir'")

            oFilters.Clear()

            For Each lvItem As ListViewItem In lvFilters.Items

                'LvItems
                With oInsert
                    .Reset()
                    .TableName("Admin_Config")
                    .BuildInsert("CustomName", "FilterLV" & lvItem.Index)
                    .BuildInsert("CustomValue", lvItem.Text)
                    Call executeQuery(oConn.ConnectionString, .GetSql())
                End With
                oFilters.Add(lvItem.Text)
            Next

            With oInsert
                .Reset()
                .TableName("Admin_Config")
                .BuildInsert("CustomName", "DataLoadDir")
                .BuildInsert("CustomValue", txtLoadDir.Text)
                Call executeQuery(oConn.ConnectionString, .GetSql())
            End With

            sDirectoryPath = txtLoadDir.Text

            bChanged = False

            'clear listview in main form
            frmMain.lvData.Items.Clear()

            Me.Close()

        Catch ex As Exception
        Finally
         
        End Try

    End Sub

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub frmOptions_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Call CloseForm(e)
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        Call Save()
    End Sub

    Private Sub frmOptions_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Call LoadForm()
    End Sub

    Private Sub LoadForm()

        Try

            Call LoadTaDates()
            Call LoadFilterListview()
            Call LoadDataDir()

        Catch ex As Exception
        Finally
            bLoading = False
        End Try

    End Sub

    Private Sub LoadDataDir()

        Try

            txtLoadDir.Text = ReadField("SELECT CustomValue FROM Admin_Config WHERE CustomName='DataLoadDir'", "")

        Catch ex As Exception

        End Try

    End Sub

    Private Sub LoadFilterListview()

        Try
            Dim dtTemp As New DataTable
            Dim oItems As New List(Of ListViewItem)

            lvFilters.Items.Clear()
            dtTemp = SqlQuery(oConn.ConnectionString, "SELECT CustomValue FROM Admin_Config WHERE CustomName LIKE 'FilterLV%'")

            If (dtTemp IsNot Nothing) AndAlso (dtTemp.Rows.Count > 0) Then
                For Each dRow As DataRow In dtTemp.Rows
                    Dim lvItem As New ListViewItem(dRow(0).ToString)
                    oItems.Add(lvItem)
                Next
                lvFilters.Items.AddRange(oItems.ToArray)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub LoadTaDates()

        Try

            txtStartDate.Text = ReadField("SELECT CustomValue FROM Admin_Config WHERE CustomName='" & TA_START_DATE_COL & "'", "")
            txtEndDate.Text = ReadField("SELECT CustomValue FROM Admin_Config WHERE CustomName='" & TA_END_DATE_COL & "'", "")

            dtpStart.Value = txtStartDate.Text
            dtpEnd.Value = txtEndDate.Text

        Catch ex As Exception

        End Try

    End Sub

    Private Sub lvFilters_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles lvFilters.KeyUp

        Try

            If e.KeyCode = Keys.F2 AndAlso lvFilters.SelectedItems.Count = 1 Then
                lvFilters.SelectedItems(0).BeginEdit()
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Function EverythingIsFilledIn()

        Dim bFilledIn As Boolean = False

        Try

            bFilledIn = (txtStartDate.Text.Length > 0) AndAlso _
                      (txtEndDate.Text.Length > 0) AndAlso _
                      (lvFilters.Items.Count > 0) AndAlso _
                      (txtLoadDir.Text.Length > 0)

        Catch ex As Exception
        Finally
            EverythingIsFilledIn = bFilledIn
        End Try

    End Function

    Private Sub txtLoadDir_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtLoadDir.TextChanged
        If Not bLoading Then bChanged = True
    End Sub

    Private Sub btnBrowse_Click(sender As System.Object, e As System.EventArgs) Handles btnBrowse.Click
        Call BrowseForDir()
    End Sub

    Private Sub BrowseForDir()

        Try

            Dim ofd As New FolderBrowserDialog

            ofd.RootFolder = Environment.SpecialFolder.DesktopDirectory

            If ofd.ShowDialog Then
                txtLoadDir.Text = ofd.SelectedPath
            End If

        Catch ex As Exception

        End Try

    End Sub

End Class