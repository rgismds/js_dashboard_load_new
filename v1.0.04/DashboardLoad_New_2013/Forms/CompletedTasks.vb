﻿Public Class CompletedTasks
    Private bShown As Boolean


    Public Property FormShown() As Boolean
        Get
            Return Me.bShown
        End Get
        Set(ByVal Value As Boolean)
            Me.bShown = Value
        End Set
    End Property

    Public Sub UpdateStatus(sStatus As String)
        tsLabel.Text = sStatus
        tsLabel.Invalidate()
        Windows.Forms.Application.DoEvents()
        Cursor = Cursors.WaitCursor
    End Sub

    Public Sub CompleteTask(nLine As Integer)
        chkTasks.SetItemCheckState(nLine - 1, CheckState.Checked)
    End Sub

    Private Sub CompletedTasks_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        bShown = False
        chkTasks.Items.Add("Remove previous files from Import Area")
        chkTasks.Items.Add("Copy files to Import Area")
        chkTasks.Items.Add("Copy Outlook Orridge files to Import Area")
        chkTasks.Items.Add("Import Clothing Sales file")
        chkTasks.Items.Add("Import Clothing Shrink file")
        chkTasks.Items.Add("Import SuperCat Verifier files")
    End Sub

    Private Sub CompletedTasks_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        bShown = True
    End Sub
End Class


