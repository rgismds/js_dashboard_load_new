﻿Public Class frmDatabase

    Public bReturn As Boolean = False

    Private Sub frmDatabase_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call LoadForm()
    End Sub

    Private Sub LoadForm()

        Try

            Dim oItems As New List(Of clsCboItem)
            Dim dTable As DataTable = SqlQuery(oConn.ConnectionString, "SELECT DISTINCT EnvDescLong, DBName FROM tbClientEnvironment")

            cboDB.Items.Clear()

            If dTable IsNot Nothing AndAlso dTable.Rows.Count > 0 Then
                For Each dRow As DataRow In dTable.Rows
                    Dim oItem As New clsCboItem(dRow("EnvDescLong").ToString.Trim, dRow("DBName").ToString.Trim)
                    oItems.Add(oItem)
                Next
                cboDB.Items.AddRange(oItems.ToArray)
            End If

        Catch ex As Exception
            MsgBox("Invalid Database Connection!", MsgBoxStyle.Critical)
            Me.Close()
        End Try

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        Try

            'Reset connection
            oConn.ConnectionString = GenerateConnectionString(oConn.Server, CType(cboDB.SelectedItem, clsCboItem).DbName, oConn.User, oConn.Pwd, oConn.TrustedConnection)
            oConn.Database = CType(cboDB.SelectedItem, clsCboItem).DbName

            'if open, then try to close
            If oConn.Connection.State <> ConnectionState.Closed Then
                Try
                    oConn.Connection.Close()
                Catch ex As Exception

                End Try
            End If

            bReturn = True
            Me.Close()

        Catch ex As Exception

        End Try

    End Sub
End Class