﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DatabaseConfigToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InvalidSubCatCheckToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OptionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UtilitiesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BatchRebuildDataToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RunWeeklyStoredProcedureToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RunClothingStoredProcedureToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.gbDates = New System.Windows.Forms.GroupBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkSkipEmailNotification = New System.Windows.Forms.CheckBox()
        Me.chkSkipQlikViewStagingRefresh = New System.Windows.Forms.CheckBox()
        Me.chkSkipSuperCatVerifierImport = New System.Windows.Forms.CheckBox()
        Me.chkSkipClothingImport = New System.Windows.Forms.CheckBox()
        Me.chkSkipPreLoad = New System.Windows.Forms.CheckBox()
        Me.btnLoad = New System.Windows.Forms.Button()
        Me.dtpEnd = New System.Windows.Forms.DateTimePicker()
        Me.dtpStart = New System.Windows.Forms.DateTimePicker()
        Me.txtEndDate = New System.Windows.Forms.TextBox()
        Me.txtStartDate = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.gbData = New System.Windows.Forms.GroupBox()
        Me.lvData = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader8 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.mnuExceptions = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ClearExceptionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.EmergencyCountToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.C1FullCountToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.C2CountToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TACountToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TrialToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AdditionalCountToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.RemoveDupeDataToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnRun = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.labelStatus = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblStatus = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tspb = New System.Windows.Forms.ToolStripProgressBar()
        Me.bgwRun = New System.ComponentModel.BackgroundWorker()
        Me.bgwLoadFiles = New System.ComponentModel.BackgroundWorker()
        Me.btnDeDupe = New System.Windows.Forms.Button()
        Me.HelpProvider1 = New System.Windows.Forms.HelpProvider()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.MenuStrip1.SuspendLayout()
        Me.gbDates.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.gbData.SuspendLayout()
        Me.mnuExceptions.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.UtilitiesToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(719, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DatabaseConfigToolStripMenuItem, Me.InvalidSubCatCheckToolStripMenuItem, Me.OptionsToolStripMenuItem, Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "&File"
        '
        'DatabaseConfigToolStripMenuItem
        '
        Me.DatabaseConfigToolStripMenuItem.Name = "DatabaseConfigToolStripMenuItem"
        Me.DatabaseConfigToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.DatabaseConfigToolStripMenuItem.Text = "&Database Config"
        '
        'InvalidSubCatCheckToolStripMenuItem
        '
        Me.InvalidSubCatCheckToolStripMenuItem.Name = "InvalidSubCatCheckToolStripMenuItem"
        Me.InvalidSubCatCheckToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.InvalidSubCatCheckToolStripMenuItem.Text = "Invalid SubCat Check"
        '
        'OptionsToolStripMenuItem
        '
        Me.OptionsToolStripMenuItem.Name = "OptionsToolStripMenuItem"
        Me.OptionsToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.OptionsToolStripMenuItem.Text = "&Options"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.ExitToolStripMenuItem.Text = "&Exit"
        '
        'UtilitiesToolStripMenuItem
        '
        Me.UtilitiesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BatchRebuildDataToolStripMenuItem, Me.RunWeeklyStoredProcedureToolStripMenuItem, Me.RunClothingStoredProcedureToolStripMenuItem})
        Me.UtilitiesToolStripMenuItem.Name = "UtilitiesToolStripMenuItem"
        Me.UtilitiesToolStripMenuItem.Size = New System.Drawing.Size(58, 20)
        Me.UtilitiesToolStripMenuItem.Text = "Utilities"
        '
        'BatchRebuildDataToolStripMenuItem
        '
        Me.BatchRebuildDataToolStripMenuItem.Image = Global.SainsburysDashboardLoad.My.Resources.Resources.DataCollection
        Me.BatchRebuildDataToolStripMenuItem.Name = "BatchRebuildDataToolStripMenuItem"
        Me.BatchRebuildDataToolStripMenuItem.Size = New System.Drawing.Size(238, 22)
        Me.BatchRebuildDataToolStripMenuItem.Text = "Batch Rebuild Data"
        '
        'RunWeeklyStoredProcedureToolStripMenuItem
        '
        Me.RunWeeklyStoredProcedureToolStripMenuItem.Image = Global.SainsburysDashboardLoad.My.Resources.Resources.Calendar
        Me.RunWeeklyStoredProcedureToolStripMenuItem.Name = "RunWeeklyStoredProcedureToolStripMenuItem"
        Me.RunWeeklyStoredProcedureToolStripMenuItem.Size = New System.Drawing.Size(238, 22)
        Me.RunWeeklyStoredProcedureToolStripMenuItem.Text = "Run Weekly Stored Procedure"
        '
        'RunClothingStoredProcedureToolStripMenuItem
        '
        Me.RunClothingStoredProcedureToolStripMenuItem.Image = Global.SainsburysDashboardLoad.My.Resources.Resources.LayerUnfreeze
        Me.RunClothingStoredProcedureToolStripMenuItem.Name = "RunClothingStoredProcedureToolStripMenuItem"
        Me.RunClothingStoredProcedureToolStripMenuItem.Size = New System.Drawing.Size(238, 22)
        Me.RunClothingStoredProcedureToolStripMenuItem.Text = "Run Clothing Stored Procedure"
        '
        'gbDates
        '
        Me.gbDates.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbDates.Controls.Add(Me.Button1)
        Me.gbDates.Controls.Add(Me.GroupBox1)
        Me.gbDates.Controls.Add(Me.chkSkipPreLoad)
        Me.gbDates.Controls.Add(Me.btnLoad)
        Me.gbDates.Controls.Add(Me.dtpEnd)
        Me.gbDates.Controls.Add(Me.dtpStart)
        Me.gbDates.Controls.Add(Me.txtEndDate)
        Me.gbDates.Controls.Add(Me.txtStartDate)
        Me.gbDates.Controls.Add(Me.Label2)
        Me.gbDates.Controls.Add(Me.Label1)
        Me.gbDates.Location = New System.Drawing.Point(12, 27)
        Me.gbDates.Name = "gbDates"
        Me.gbDates.Size = New System.Drawing.Size(695, 157)
        Me.gbDates.TabIndex = 1
        Me.gbDates.TabStop = False
        Me.gbDates.Text = "Load Dates"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkSkipEmailNotification)
        Me.GroupBox1.Controls.Add(Me.chkSkipQlikViewStagingRefresh)
        Me.GroupBox1.Controls.Add(Me.chkSkipSuperCatVerifierImport)
        Me.GroupBox1.Controls.Add(Me.chkSkipClothingImport)
        Me.GroupBox1.Location = New System.Drawing.Point(353, 62)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(319, 89)
        Me.GroupBox1.TabIndex = 10
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Tasks to omit"
        '
        'chkSkipEmailNotification
        '
        Me.chkSkipEmailNotification.AutoSize = True
        Me.chkSkipEmailNotification.Location = New System.Drawing.Point(155, 66)
        Me.chkSkipEmailNotification.Name = "chkSkipEmailNotification"
        Me.chkSkipEmailNotification.Size = New System.Drawing.Size(107, 17)
        Me.chkSkipEmailNotification.TabIndex = 12
        Me.chkSkipEmailNotification.Text = "Email Notification"
        Me.chkSkipEmailNotification.UseVisualStyleBackColor = True
        '
        'chkSkipQlikViewStagingRefresh
        '
        Me.chkSkipQlikViewStagingRefresh.AutoSize = True
        Me.HelpProvider1.SetHelpKeyword(Me.chkSkipQlikViewStagingRefresh, "QlikView")
        Me.chkSkipQlikViewStagingRefresh.Location = New System.Drawing.Point(10, 66)
        Me.chkSkipQlikViewStagingRefresh.Name = "chkSkipQlikViewStagingRefresh"
        Me.HelpProvider1.SetShowHelp(Me.chkSkipQlikViewStagingRefresh, True)
        Me.chkSkipQlikViewStagingRefresh.Size = New System.Drawing.Size(139, 17)
        Me.chkSkipQlikViewStagingRefresh.TabIndex = 11
        Me.chkSkipQlikViewStagingRefresh.Text = "QlikView staging refresh"
        Me.chkSkipQlikViewStagingRefresh.UseVisualStyleBackColor = True
        '
        'chkSkipSuperCatVerifierImport
        '
        Me.chkSkipSuperCatVerifierImport.AutoSize = True
        Me.chkSkipSuperCatVerifierImport.Location = New System.Drawing.Point(155, 28)
        Me.chkSkipSuperCatVerifierImport.Name = "chkSkipSuperCatVerifierImport"
        Me.chkSkipSuperCatVerifierImport.Size = New System.Drawing.Size(151, 17)
        Me.chkSkipSuperCatVerifierImport.TabIndex = 10
        Me.chkSkipSuperCatVerifierImport.Text = "Supercat Verifier file import"
        Me.chkSkipSuperCatVerifierImport.UseVisualStyleBackColor = True
        '
        'chkSkipClothingImport
        '
        Me.chkSkipClothingImport.AutoSize = True
        Me.chkSkipClothingImport.Location = New System.Drawing.Point(10, 28)
        Me.chkSkipClothingImport.Name = "chkSkipClothingImport"
        Me.chkSkipClothingImport.Size = New System.Drawing.Size(111, 17)
        Me.chkSkipClothingImport.TabIndex = 9
        Me.chkSkipClothingImport.Text = "Clothing file import"
        Me.chkSkipClothingImport.UseVisualStyleBackColor = True
        '
        'chkSkipPreLoad
        '
        Me.chkSkipPreLoad.AutoSize = True
        Me.chkSkipPreLoad.Location = New System.Drawing.Point(363, 27)
        Me.chkSkipPreLoad.Name = "chkSkipPreLoad"
        Me.chkSkipPreLoad.Size = New System.Drawing.Size(121, 17)
        Me.chkSkipPreLoad.TabIndex = 8
        Me.chkSkipPreLoad.Text = "Skip Pre-Load tasks"
        Me.chkSkipPreLoad.UseVisualStyleBackColor = True
        '
        'btnLoad
        '
        Me.btnLoad.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnLoad.Location = New System.Drawing.Point(616, 27)
        Me.btnLoad.Name = "btnLoad"
        Me.btnLoad.Size = New System.Drawing.Size(56, 23)
        Me.btnLoad.TabIndex = 6
        Me.btnLoad.Text = "Load"
        Me.btnLoad.UseVisualStyleBackColor = True
        '
        'dtpEnd
        '
        Me.dtpEnd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtpEnd.Location = New System.Drawing.Point(300, 58)
        Me.dtpEnd.Name = "dtpEnd"
        Me.dtpEnd.Size = New System.Drawing.Size(19, 20)
        Me.dtpEnd.TabIndex = 5
        '
        'dtpStart
        '
        Me.dtpStart.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtpStart.Location = New System.Drawing.Point(300, 29)
        Me.dtpStart.Name = "dtpStart"
        Me.dtpStart.Size = New System.Drawing.Size(19, 20)
        Me.dtpStart.TabIndex = 4
        '
        'txtEndDate
        '
        Me.txtEndDate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtEndDate.Location = New System.Drawing.Point(70, 58)
        Me.txtEndDate.Name = "txtEndDate"
        Me.txtEndDate.ReadOnly = True
        Me.txtEndDate.Size = New System.Drawing.Size(224, 20)
        Me.txtEndDate.TabIndex = 3
        Me.txtEndDate.TabStop = False
        '
        'txtStartDate
        '
        Me.txtStartDate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtStartDate.Location = New System.Drawing.Point(70, 29)
        Me.txtStartDate.Name = "txtStartDate"
        Me.txtStartDate.ReadOnly = True
        Me.txtStartDate.Size = New System.Drawing.Size(224, 20)
        Me.txtStartDate.TabIndex = 2
        Me.txtStartDate.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 61)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(55, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "End Date:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(58, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Start Date:"
        '
        'gbData
        '
        Me.gbData.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbData.Controls.Add(Me.lvData)
        Me.gbData.Location = New System.Drawing.Point(12, 190)
        Me.gbData.Name = "gbData"
        Me.gbData.Size = New System.Drawing.Size(695, 392)
        Me.gbData.TabIndex = 2
        Me.gbData.TabStop = False
        Me.gbData.Text = "Data"
        '
        'lvData
        '
        Me.lvData.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader6, Me.ColumnHeader3, Me.ColumnHeader8, Me.ColumnHeader4, Me.ColumnHeader5, Me.ColumnHeader7})
        Me.lvData.ContextMenuStrip = Me.mnuExceptions
        Me.lvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvData.FullRowSelect = True
        Me.lvData.Location = New System.Drawing.Point(3, 16)
        Me.lvData.Name = "lvData"
        Me.lvData.Size = New System.Drawing.Size(689, 373)
        Me.lvData.TabIndex = 0
        Me.lvData.UseCompatibleStateImageBehavior = False
        Me.lvData.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Tag = "2"
        Me.ColumnHeader1.Text = "Inv Date"
        Me.ColumnHeader1.Width = 71
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Tag = "1"
        Me.ColumnHeader2.Text = "Store No"
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Tag = "0"
        Me.ColumnHeader6.Text = "Store Type"
        Me.ColumnHeader6.Width = 69
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Tag = "0"
        Me.ColumnHeader3.Text = "Exception"
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "Source File"
        Me.ColumnHeader8.Width = 154
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Tag = "0"
        Me.ColumnHeader4.Text = "Table"
        Me.ColumnHeader4.Width = 101
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Tag = "0"
        Me.ColumnHeader5.Text = "Duplicate"
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "Dupe Date"
        Me.ColumnHeader7.Width = 109
        '
        'mnuExceptions
        '
        Me.mnuExceptions.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.mnuExceptions.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClearExceptionToolStripMenuItem, Me.ToolStripSeparator1, Me.EmergencyCountToolStripMenuItem, Me.C1FullCountToolStripMenuItem, Me.C2CountToolStripMenuItem, Me.TACountToolStripMenuItem, Me.TrialToolStripMenuItem, Me.AdditionalCountToolStripMenuItem, Me.ToolStripSeparator2, Me.RemoveDupeDataToolStripMenuItem})
        Me.mnuExceptions.Name = "mnuExceptions"
        Me.mnuExceptions.Size = New System.Drawing.Size(235, 272)
        '
        'ClearExceptionToolStripMenuItem
        '
        Me.ClearExceptionToolStripMenuItem.Image = Global.SainsburysDashboardLoad.My.Resources.Resources.Clear
        Me.ClearExceptionToolStripMenuItem.Name = "ClearExceptionToolStripMenuItem"
        Me.ClearExceptionToolStripMenuItem.Size = New System.Drawing.Size(234, 32)
        Me.ClearExceptionToolStripMenuItem.Text = "Clear Exception"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(231, 6)
        '
        'EmergencyCountToolStripMenuItem
        '
        Me.EmergencyCountToolStripMenuItem.Image = Global.SainsburysDashboardLoad.My.Resources.Resources.Decline_v2
        Me.EmergencyCountToolStripMenuItem.Name = "EmergencyCountToolStripMenuItem"
        Me.EmergencyCountToolStripMenuItem.Size = New System.Drawing.Size(234, 32)
        Me.EmergencyCountToolStripMenuItem.Text = "Emergency Count"
        '
        'C1FullCountToolStripMenuItem
        '
        Me.C1FullCountToolStripMenuItem.Image = Global.SainsburysDashboardLoad.My.Resources.Resources.File3tick
        Me.C1FullCountToolStripMenuItem.Name = "C1FullCountToolStripMenuItem"
        Me.C1FullCountToolStripMenuItem.Size = New System.Drawing.Size(234, 32)
        Me.C1FullCountToolStripMenuItem.Text = "C1 / Full Count"
        '
        'C2CountToolStripMenuItem
        '
        Me.C2CountToolStripMenuItem.Image = Global.SainsburysDashboardLoad.My.Resources.Resources.File2tickfade
        Me.C2CountToolStripMenuItem.Name = "C2CountToolStripMenuItem"
        Me.C2CountToolStripMenuItem.Size = New System.Drawing.Size(234, 32)
        Me.C2CountToolStripMenuItem.Text = "C2 Count"
        '
        'TACountToolStripMenuItem
        '
        Me.TACountToolStripMenuItem.Image = Global.SainsburysDashboardLoad.My.Resources.Resources.File5tick
        Me.TACountToolStripMenuItem.Name = "TACountToolStripMenuItem"
        Me.TACountToolStripMenuItem.Size = New System.Drawing.Size(234, 32)
        Me.TACountToolStripMenuItem.Text = "TA Count"
        '
        'TrialToolStripMenuItem
        '
        Me.TrialToolStripMenuItem.Image = Global.SainsburysDashboardLoad.My.Resources.Resources.Question1
        Me.TrialToolStripMenuItem.Name = "TrialToolStripMenuItem"
        Me.TrialToolStripMenuItem.Size = New System.Drawing.Size(234, 32)
        Me.TrialToolStripMenuItem.Text = "Trial"
        '
        'AdditionalCountToolStripMenuItem
        '
        Me.AdditionalCountToolStripMenuItem.Image = CType(resources.GetObject("AdditionalCountToolStripMenuItem.Image"), System.Drawing.Image)
        Me.AdditionalCountToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.AdditionalCountToolStripMenuItem.Name = "AdditionalCountToolStripMenuItem"
        Me.AdditionalCountToolStripMenuItem.Size = New System.Drawing.Size(234, 32)
        Me.AdditionalCountToolStripMenuItem.Text = "Additional Count"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(231, 6)
        '
        'RemoveDupeDataToolStripMenuItem
        '
        Me.RemoveDupeDataToolStripMenuItem.Image = Global.SainsburysDashboardLoad.My.Resources.Resources.InvalidMerchTable_16__2_
        Me.RemoveDupeDataToolStripMenuItem.Name = "RemoveDupeDataToolStripMenuItem"
        Me.RemoveDupeDataToolStripMenuItem.Size = New System.Drawing.Size(234, 32)
        Me.RemoveDupeDataToolStripMenuItem.Text = "Remove Dupe Data From DB"
        '
        'btnRun
        '
        Me.btnRun.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRun.Enabled = False
        Me.btnRun.Location = New System.Drawing.Point(551, 590)
        Me.btnRun.Name = "btnRun"
        Me.btnRun.Size = New System.Drawing.Size(75, 23)
        Me.btnRun.TabIndex = 3
        Me.btnRun.Text = "Run"
        Me.btnRun.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(632, 590)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 23)
        Me.btnExit.TabIndex = 4
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.labelStatus, Me.lblStatus, Me.tspb})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 622)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(719, 22)
        Me.StatusStrip1.TabIndex = 5
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'labelStatus
        '
        Me.labelStatus.Name = "labelStatus"
        Me.labelStatus.Size = New System.Drawing.Size(0, 17)
        '
        'lblStatus
        '
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(121, 17)
        Me.lblStatus.Text = "ToolStripStatusLabel1"
        '
        'tspb
        '
        Me.tspb.Name = "tspb"
        Me.tspb.Size = New System.Drawing.Size(165, 16)
        Me.tspb.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.tspb.Visible = False
        '
        'bgwRun
        '
        Me.bgwRun.WorkerReportsProgress = True
        '
        'bgwLoadFiles
        '
        '
        'btnDeDupe
        '
        Me.btnDeDupe.Location = New System.Drawing.Point(478, 590)
        Me.btnDeDupe.Margin = New System.Windows.Forms.Padding(2)
        Me.btnDeDupe.Name = "btnDeDupe"
        Me.btnDeDupe.Size = New System.Drawing.Size(68, 23)
        Me.btnDeDupe.TabIndex = 7
        Me.btnDeDupe.Text = "De-dupe"
        Me.btnDeDupe.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(508, 27)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 11
        Me.Button1.Text = "Supercat"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(719, 644)
        Me.Controls.Add(Me.btnDeDupe)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnRun)
        Me.Controls.Add(Me.gbData)
        Me.Controls.Add(Me.gbDates)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MinimumSize = New System.Drawing.Size(281, 581)
        Me.Name = "frmMain"
        Me.Text = "Dashboard Data Load"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.gbDates.ResumeLayout(False)
        Me.gbDates.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.gbData.ResumeLayout(False)
        Me.mnuExceptions.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OptionsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents gbDates As System.Windows.Forms.GroupBox
    Friend WithEvents DatabaseConfigToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents gbData As System.Windows.Forms.GroupBox
    Friend WithEvents btnRun As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tspb As System.Windows.Forms.ToolStripProgressBar
    Friend WithEvents bgwRun As System.ComponentModel.BackgroundWorker
    Friend WithEvents lvData As System.Windows.Forms.ListView
    Friend WithEvents dtpEnd As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpStart As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtEndDate As System.Windows.Forms.TextBox
    Friend WithEvents txtStartDate As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents bgwLoadFiles As System.ComponentModel.BackgroundWorker
    Friend WithEvents btnLoad As System.Windows.Forms.Button
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents mnuExceptions As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ClearExceptionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents EmergencyCountToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents C1FullCountToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents C2CountToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TACountToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InvalidSubCatCheckToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader7 As ColumnHeader
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents RemoveDupeDataToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents UtilitiesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents BatchRebuildDataToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RunWeeklyStoredProcedureToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RunClothingStoredProcedureToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents btnDeDupe As System.Windows.Forms.Button
    Friend WithEvents labelStatus As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblStatus As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents TrialToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AdditionalCountToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents chkSkipPreLoad As System.Windows.Forms.CheckBox
    Friend WithEvents ColumnHeader8 As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkSkipClothingImport As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chkSkipSuperCatVerifierImport As System.Windows.Forms.CheckBox
    Friend WithEvents chkSkipQlikViewStagingRefresh As System.Windows.Forms.CheckBox
    Friend WithEvents HelpProvider1 As System.Windows.Forms.HelpProvider
    Friend WithEvents chkSkipEmailNotification As System.Windows.Forms.CheckBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
