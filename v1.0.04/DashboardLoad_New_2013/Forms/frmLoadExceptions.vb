﻿Public Class frmLoadExceptions

    Private m_StartDate As String
    Public Property StartDate() As String
        Get
            Return m_StartDate
        End Get
        Set(value As String)
            m_StartDate = value
        End Set
    End Property

    Private m_EndDate As String
    Public Property EndDate() As String
        Get
            Return m_EndDate
        End Get
        Set(value As String)
            m_EndDate = value
        End Set
    End Property

    Private dtLoadExceptions As New DataTable

    Private Sub frmLoadExceptions_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub tmrLoad_Tick(sender As Object, e As EventArgs) Handles tmrLoad.Tick
        Call LoadForm()
    End Sub

    Private Sub LoadForm()

        Try

            Dim sSQL As String = String.Empty

            tmrLoad.Enabled = False
            Me.Cursor = Cursors.WaitCursor

            sSQL = "SELECT * FROM TB_Load_Exceptions WHERE Inventory_Date >= '" & StartDate & "' AND Inventory_Date <= '" & EndDate & "'"
            dtLoadExceptions = SqlQuery(oConn.ConnectionString, sSQL)

            If dtLoadExceptions.Rows.Count > 0 Then
                dgvLoadExceptions.DataSource = dtLoadExceptions
                dgvLoadExceptions.Refresh()
            Else
                dgvLoadExceptions.Enabled = False
            End If

            btnExport.Enabled = dgvLoadExceptions.Rows.Count > 0

        Catch ex As Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message)
        Finally
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        Call ExportToDesktop()
    End Sub

    Private Sub ExportToDesktop()

        Try

            If dtLoadExceptions.Rows.Count > 0 Then
                dtLoadExceptions.TableName = "LoadExceptions"
                dtLoadExceptions.WriteXml(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) & "\LoadExceptions_" & Now.ToString("yyyy_MM_dd_HH_mm_ss") & ".xml")
                MsgBox("Exported to desktop!", MsgBoxStyle.Information)
            Else
                MsgBox("No data to export!", MsgBoxStyle.Exclamation)
            End If

        Catch ex As Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message)
        End Try

    End Sub

End Class