﻿Imports System.ComponentModel

Public Class frmBatchBuild
    Private m_StartDate As String = ""
    Private m_EndDate As String = ""
    Private m_Days As String = ""

    Private Sub dtpStart_ValueChanged(sender As System.Object, e As System.EventArgs) Handles dtpStart.ValueChanged
        Try
            txtStartDate.Text = dtpStart.Value.ToString("yyyy-MM-dd")
        Catch ex As Exception
        End Try
    End Sub

    Private Sub dtpEnd_ValueChanged(sender As System.Object, e As System.EventArgs) Handles dtpEnd.ValueChanged
        Try
            txtEndDate.Text = dtpEnd.Value.ToString("yyyy-MM-dd")
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnRun_Click(sender As Object, e As EventArgs) Handles btnRun.Click
        Call Rebuild()
    End Sub

    Private Sub Rebuild()

        Try
            m_StartDate = txtStartDate.Text
            m_EndDate = txtEndDate.Text
            m_Days = txtDays.Text

            tssProgress.Text = "Rebuilding..."
            tssProgress.Visible = True
            pbProgress.Visible = True
            gbDates.Enabled = False

            bgwRebuild.RunWorkerAsync()

        Catch ex As Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub frmBatchBuild_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        tssProgress.Text = ""
    End Sub

    Private Sub frmBatchBuild_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        e.Cancel = bgwRebuild.IsBusy
    End Sub

    Private Sub bgwRebuild_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgwRebuild.DoWork
        Call BatchRebuild()
    End Sub

    Private Sub BatchRebuild()

        Try

            Dim sSQL As String = ""

            sSQL = "EXEC usp_BatchRebuild " &
                   "@StartDate = N'" & m_StartDate & "', " &
                   "@EndDate = N'" & m_EndDate & "', " &
                   "DaysPerBatch = " & m_Days

            Call executeQuery(oConn.ConnectionString, sSQL)

        Catch ex As Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub bgwRebuild_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgwRebuild.RunWorkerCompleted
        Try
            gbDates.Enabled = True
            tssProgress.Visible = False
            pbProgress.Visible = False
            MsgBox("Finished Rebuilding!", MsgBoxStyle.Information)
        Catch ex As Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub
End Class