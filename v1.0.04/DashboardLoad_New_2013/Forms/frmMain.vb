﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Windows
Imports System.Text
Imports Microsoft.Office.Interop.Outlook
Imports Microsoft.Office.Interop
Imports System.Runtime.InteropServices
Imports System.ComponentModel



Public Class frmMain
    Dim cImportLog As clsLogItem

#Region "Privates"
    Dim dlgTasks As CompletedTasks
    Dim WithEvents bgwInfoWin As BackgroundWorker
    Dim sProcessStartDate As String
    Dim sProcessEndDate As String
    Dim oLvItems As New List(Of ListViewItem)
    Dim oFilesToProcess As New List(Of String)
    Dim bErrors As Boolean = False
    Dim htExceptions As New Hashtable
#End Region

    Private Sub DatabaseConfigToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles DatabaseConfigToolStripMenuItem.Click

        Try

            Dim frm As New frmConnection

            frm.udpConnection = oConn
            frm.udpTableName = "tbClientEnvironment"

            If frm.ShowDialog() = Windows.Forms.DialogResult.OK Then
                Call LoadForm()
            End If
            ' Set the main window title to refer to the current connected database
            Me.Text = "Dashboard Data Load - " & oConn.Database


        Catch ex As System.Exception

        End Try

    End Sub

    Private Sub OptionsToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles OptionsToolStripMenuItem.Click

        Try

            Dim frm As New frmOptions
            frm.ShowDialog()

        Catch ex As System.Exception

        End Try

    End Sub

    Private Sub frmMain_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        Try

            If bgwLoadFiles.IsBusy OrElse bgwRun.IsBusy Then
                MsgBox("A process is running, please wait...", MsgBoxStyle.Information + MsgBoxStyle.OkOnly)
                e.Cancel = True
            End If

        Catch ex As System.Exception

        End Try

    End Sub

    Private Sub frmMain_KeyUp(sender As Object, e As KeyEventArgs) Handles Me.KeyUp

        Try

            If e.KeyCode = Keys.C AndAlso e.Control Then

                Dim headers = (From ch In lvData.Columns Let header = DirectCast(ch, ColumnHeader) Select header.Text).ToArray()

                Dim items = (From item In lvData.Items Let lvi = DirectCast(item, ListViewItem) Select (From subitem In lvi.SubItems _
                            Let si = DirectCast(subitem, ListViewItem.ListViewSubItem) Select si.Text).ToArray()).ToArray()

                Dim table As String = String.Join(vbTab, headers) & Environment.NewLine

                For Each a In items
                    table &= String.Join(vbTab, a) & Environment.NewLine
                Next

                table = table.TrimEnd(CChar(Environment.NewLine))

                Clipboard.SetText(table)

            End If

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub frmMain_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = False
        Call LoadForm()
        Me.Text = "Dashboard Data Load - " & oConn.Database
    End Sub

    Private Sub LoadForm()
        CenterToScreen()
        Try

            lblStatus.Text = ""

            Call SetupListView()
            ' Set default date period to be previous Sunday to Saturday
            dtpStart.Value = DateTime.Now.AddDays(-7 - Weekday(Date.Now()) + 1)
            dtpEnd.Value = dtpStart.Value.AddDays(6)

            ' Call SetupErrorLogs()
            Call LoadSavedConnectionDetailsFromRegistry()

            If CreateConnection(oConn.ConnectionString, oConn.Connection, oConn.Provider) Then

                'Close connection
                oConn.Connection.Close()

                '2014-08-14 SC
                'Choose Database
                '#2015# Call ChangeToSelectedDatabase()

                Dim dtTemp As DataTable = SqlQuery(oConn.ConnectionString, "SELECT CustomValue FROM Admin_Config WHERE CustomName LIKE 'FilterLV%'")
                ' JR - 26/09/2017
                ' Ensure oFilters array is cleared down as this was causing main list to be populated twice
                ' if the database connection was tested/changed after the user started the application
                oFilters.Clear()
                If dtTemp IsNot Nothing AndAlso dtTemp.Rows.Count > 0 Then
                    For Each dRow As DataRow In dtTemp.Rows
                        oFilters.Add(dRow(0))
                    Next
                End If

                sDirectoryPath = ReadField("SELECT CustomValue FROM Admin_Config WHERE CustomName='DataLoadDir'", "")
                sStartTA = ReadField("SELECT CustomValue FROM Admin_Config WHERE CustomName='" & TA_START_DATE_COL & "'", "")
                sEndTA = ReadField("SELECT CustomValue FROM Admin_Config WHERE CustomName='" & TA_END_DATE_COL & "'", "")

                gbDates.Enabled = True

                Call LoadExceptions()

                Call LoadStoreTypes()

            Else
                MsgBox("Invalid database configuration!", MsgBoxStyle.Exclamation)
                gbDates.Enabled = False
            End If

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        Finally
            OptionsToolStripMenuItem.Enabled = gbDates.Enabled
            UtilitiesToolStripMenuItem.Enabled = gbData.Enabled
            gbData.Enabled = False
        End Try
        btnLoad.Focus()

    End Sub

    Private Sub ChangeToSelectedDatabase()

        Try

            Dim oFrm As New frmDatabase
            oFrm.StartPosition = FormStartPosition.CenterScreen
            oFrm.ShowDialog()

            If Not oFrm.bReturn Then
                btnLoad.Enabled = False
                gbDates.Enabled = False
                Throw New System.Exception("")
            End If

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub LoadStoreTypes()

        Try

            dtStoreTypes = New DataTable
            dtStoreTypes = SqlQuery(oConn.ConnectionString, "SELECT Store_Number, Store_Type FROM TB_Retail_Hierarchy WHERE Count_Year IN (SELECT TOP(1) Count_Year FROM Financial_Calender ORDER BY Count_Year DESC)")

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub LoadExceptions()

        Try

            Dim dTable As DataTable = SqlQuery(oConn.ConnectionString, "SELECT Store_Number, Inventory_Date, ExclusionType FROM Inventory_Data_OUT15_Exclusions")

            If dTable IsNot Nothing AndAlso dTable.Rows.Count > 0 Then
                For Each dRow As DataRow In dTable.Rows

                    Dim oTag As New udtExceptionProps
                    oTag.iStoreNumber = dRow(0)
                    oTag.sInvDate = dRow(1)
                    oTag.sExclusionType = dRow(2)
                    If Not htExceptions.ContainsKey(dRow(0) & ":" & CDate(dRow(1)).ToString("yyyy-MM-dd")) Then
                        ' Add key if it doesnt exist alreadt in hash table
                        htExceptions.Add(dRow(0) & ":" & CDate(dRow(1)).ToString("yyyy-MM-dd"), oTag)
                    End If


                Next
            End If

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub SetupErrorLogs()

        Try

            With cLog

                .Reset()
                .p_Path = Forms.Application.StartupPath & "\Log_" & Now.ToString("yyyy_MM_dd hh_mm_ss") & ".xml"

                'add Tables
                .p_Tables = "Files ¬ Errors ¬ NotImported ¬ ImportedFiles"

                'File Columns
                .p_Columns = "Files ¬ Name"

                'Errors Columns
                .p_Columns = "Errors ¬ Name ¬ LineError"

                'NotImported Columns
                .p_Columns = "NotImported ¬ StoreNumber ¬ Name ¬ Reason"

                'ImportedFiles Columns
                .p_Columns = "ImportedFiles ¬ StoreNumber ¬ Name"

                'Add relation FilesErrors between table Files and table Errors column = Name
                .AddRelation("FilesErrors", "Files", "Errors", "Name")

                'Add relation FilesNotImported between table Files and table NotImported column = Name
                .AddRelation("FilesNotImported", "Files", "NotImported", "Name")

                'Add relation FilesImportedFiles between table Files and table ImportedFiles column = Name
                .AddRelation("FilesImportedFiles", "Files", "ImportedFiles", "Name")

            End With

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub SetupListView()

        Try

            lvData.Columns.Clear()

            Dim oColumn1, oColumn2, oColumn3, oColumn4, oColumn5, oColumn6, oColumn7, oColumn8 As New ColumnHeader

            oColumn1.Text = "Inv Date"
            oColumn1.Width = 64
            oColumn1.Tag = eColumnTag.eDate
            lvData.Columns.Add(oColumn1)

            oColumn2.Text = "Store No"
            oColumn2.Width = 55
            oColumn2.Tag = eColumnTag.eInteger
            lvData.Columns.Add(oColumn2)

            oColumn3.Text = "Store Type"
            oColumn3.Width = 68
            oColumn3.Tag = eColumnTag.eString
            lvData.Columns.Add(oColumn3)

            oColumn4.Text = "Exception"
            oColumn4.Width = 60
            oColumn4.Tag = eColumnTag.eString
            lvData.Columns.Add(oColumn4)

            oColumn8.Text = "Source File"
            oColumn8.Width = 155
            oColumn8.Tag = eColumnTag.eString
            lvData.Columns.Add(oColumn8)

            oColumn5.Text = "Dest Table"
            oColumn5.Width = 165
            oColumn5.Tag = eColumnTag.eString
            lvData.Columns.Add(oColumn5)

            oColumn6.Text = "Duplicate"
            oColumn6.Width = 60
            oColumn6.Tag = eColumnTag.eString
            lvData.Columns.Add(oColumn6)

            oColumn7.Text = "Dupe Date"
            oColumn7.Width = 110
            oColumn7.Tag = eColumnTag.eDate
            lvData.Columns.Add(oColumn7)

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub LoadSavedConnectionDetailsFromRegistry()

        Try
            Dim oIdb As IDbConnection = Nothing
            oConn = New clsConnection
            With oConn
                .ConnectionString = DecryptString(GetSetting(sKeyName, sSectionName, "ConnString", ""))
                .Database = DecryptString(GetSetting(sKeyName, sSectionName, "Database", ""))
                .Provider = DecryptString(GetSetting(sKeyName, sSectionName, "Provider", ""))
                .Pwd = DecryptString(GetSetting(sKeyName, sSectionName, "Pwd", ""))
                .User = DecryptString(GetSetting(sKeyName, sSectionName, "User", ""))
                .TrustedConnection = If(DecryptString(GetSetting(sKeyName, "Settings", "Trusted", "")) = "", False, DecryptString(GetSetting(sKeyName, sSectionName, "Trusted", "")))
                .Server = DecryptString(GetSetting(sKeyName, sSectionName, "Server", ""))
                .Connection = oIdb
            End With

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub txtStartDate_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtStartDate.TextChanged, txtEndDate.TextChanged

        Try

            btnLoad.Enabled = (txtStartDate.Text.Length > 0) AndAlso (txtEndDate.Text.Length > 0)

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub dtpStart_ValueChanged(sender As System.Object, e As System.EventArgs) Handles dtpStart.ValueChanged

        Try

            txtStartDate.Text = dtpStart.Value.ToString("yyyy-MM-dd")

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub dtpEnd_ValueChanged(sender As System.Object, e As System.EventArgs) Handles dtpEnd.ValueChanged

        Try

            txtEndDate.Text = dtpEnd.Value.ToString("yyyy-MM-dd")

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub btnLoad_Click(sender As System.Object, e As System.EventArgs) Handles btnLoad.Click

        If Not chkSkipPreLoad.Checked Then
            ' JR - 26/09/2017
            ' If for whatever reason we do not want to run all the tasks before the list is populated
            ' then if the Skip Pre-Load tasks check box is ticked, this will go straight to LoadFiles()
            Call DoPreDataLoadTasks()
        End If
        Call LoadFiles()
    End Sub

    Private Sub bgwInfoWin_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwInfoWin.DoWork
        dlgTasks = New CompletedTasks()
        dlgTasks.Show(Me)
    End Sub

    Private Sub bgwInfoWin_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwInfoWin.RunWorkerCompleted
        If e.Result IsNot Nothing Then
            '
        End If
    End Sub

    Private Sub DoPreDataLoadTasks_1()

    End Sub


    Private Sub DoPreDataLoadTasks()
        Dim sSourceFolder, sTargetFolder As String
        Dim nFilesCopied, nMailItems As Integer
        Dim dtFrom, dtTo, dtToday, dtExcelFrom As Date
        Dim answer As DialogResult


        'Following tasks are taken from the code of other applications that were run before
        ' the main load on the Sainsbury's Dashboard i.e. in this application

        ' Create completed tasks window to indicate to the user which pre data load task
        ' is being performed. The CompleteTask() function will tick the relevant item in the 
        ' check list of the message window to give user idea of overall progress
        dlgTasks = New CompletedTasks()
        dlgTasks.Show(Me)
        Windows.Forms.Application.DoEvents()
        Cursor = Cursors.WaitCursor

        dlgTasks.UpdateStatus("Deleting csv files ..")
        DeleteFiles("\\EU-FPS\Client_Data\Sainsburys\Dashboard\ImportArea\PCVAR", "csv")
        dlgTasks.UpdateStatus("Deleting out15 files ..")
        DeleteFiles("\\EU-FPS\Client_Data\Sainsburys\Dashboard\ImportArea\PCVAR", "out15")
        dlgTasks.UpdateStatus("Deleting out36 and out59 files ..")
        DeleteFiles("\\EU-FPS\Client_Data\Sainsburys\Dashboard\ImportArea\supercatverifier", "out36")
        DeleteFiles("\\EU-FPS\Client_Data\Sainsburys\Dashboard\ImportArea\supercatverifier", "out59")
        ' DeleteFiles("\\EU-FPS\Client_Data\Sainsburys\Dashboard\ImportArea\Clothing", "xls")
        dlgTasks.CompleteTask(1)
        Windows.Forms.Application.DoEvents()

        sSourceFolder = "\\EU-FPS\Client_Data\Sainsburys\Dashboard\DAILY_PCVAR"
        sTargetFolder = "\\EU-FPS\Client_Data\Sainsburys\Dashboard\ImportArea\PCVAR"

        ' Following statements replace the functionality in FileLoader.exe
        dlgTasks.UpdateStatus("Copying csv files to " & sTargetFolder)
        nFilesCopied = CopyFiles(sSourceFolder, sTargetFolder, "csv")

        sSourceFolder = "\\custdata.rgis.com\div16$\SharedArea_Division_16\custdata\jsmain"
        dlgTasks.UpdateStatus("Copying out15 files from " & sSourceFolder)
        nFilesCopied += CopyFiles(sSourceFolder, sTargetFolder, "out15")


        sSourceFolder = "\\custdata.rgis.com\div16$\SharedArea_Division_16\custdata\jscsto"
        dlgTasks.UpdateStatus("Copying out15 files from " & sSourceFolder)
        nFilesCopied += CopyFiles(sSourceFolder, sTargetFolder, "out15")

        sSourceFolder = "\\custdata.rgis.com\div16$\SharedArea_Division_16\custdata\jsta"
        dlgTasks.UpdateStatus("Copying out15 files from " & sSourceFolder)
        nFilesCopied += CopyFiles(sSourceFolder, sTargetFolder, "out15")

        sSourceFolder = "\\custdata.rgis.com\div16$\SharedArea_Division_16\custdata\jspfs"
        dlgTasks.UpdateStatus("Copying out15 files from " & sSourceFolder)
        nFilesCopied += CopyFiles(sSourceFolder, sTargetFolder, "out15")

        dlgTasks.CompleteTask(2)
        Windows.Forms.Application.DoEvents()

        ' JR - 21/09/2017
        ' Function code below was taken from the C# EmailReader application I created to 
        ' copy Orridge files from Outlook inbox into the SuperCatVerfier folder
        ' Defaults to 8 days prior to selected end date when searching for Orridge files
        dtFrom = dtpStart.Value
        dtTo = dtpEnd.Value.Date
        dtTo = dtTo.AddDays(3)
        dtToday = New Date(Now.Year, Now.Month, Now.Day, 23, 59, 59)
        dtExcelFrom = dtFrom
        'dtExcelFrom = dtExcelFrom.AddDays(-1)
        dtExcelFrom = New Date(dtExcelFrom.Year, dtExcelFrom.Month, dtExcelFrom.Day, 0, 0, 0)


        dlgTasks.UpdateStatus("Copying Orridge out36 files from Outlook to SuperCatVerifier folder")
        ReadMailItemsFromOutlook("Sainsburys Stocktake Output Files", dtFrom, dtTo, "\\EU-FPS\Client_Data\Sainsburys\Dashboard\ImportArea\SuperCatVerifier", "out36", nMailItems)
        dlgTasks.CompleteTask(3)
        Windows.Forms.Application.DoEvents()
        Cursor = Cursors.WaitCursor
        If Not chkSkipClothingImport.Checked Then
            ' JR - 17/10/2017
            ' Added tick box to allow Clothing File import to be skipped

            dlgTasks.UpdateStatus("Importing Clothing Sales file ..")
            ' Copy Excel files (sent by Sainsburys) from Outlook to the two Clothing folders
            ReadMailItemsFromOutlook("[EXT] - Clothing data", dtFrom, dtTo, "\\EU-FPS\Client_Data\Sainsburys\Dashboard\Clothing File\", "xls", nMailItems)
            answer = Forms.DialogResult.None
            If nMailItems = 0 Then
                ' No Excel files found from today or yesterday, so ask user if previous received file should be used
                answer = MessageBox.Show("No Excel file found in email from today or yesterday. Skip Clothing import (Yes) or use previous received files (No)?", "Question", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
            End If
            If answer = Forms.DialogResult.No Then
                ' Search again but extend date range to be a full week before current date
                ReadMailItemsFromOutlook("[EXT] - Clothing data", dtExcelFrom.AddDays(-8), dtToday, "\\EU-FPS\Client_Data\Sainsburys\Dashboard\Clothing File\", "xls", nMailItems)
                ReadMailItemsFromOutlook("[EXT] - Clothing data", dtExcelFrom.AddDays(-8), dtToday, "\\EU-FPS\Client_Data\Sainsburys\Dashboard\ImportArea\Clothing\", "xls", nMailItems)

            ElseIf answer = Forms.DialogResult.None Then
                ' File was found on first attempt so copy to other folder
                ReadMailItemsFromOutlook("[EXT] - Clothing data", dtFrom, dtTo, "\\EU-FPS\Client_Data\Sainsburys\Dashboard\ImportArea\Clothing\", "xls", nMailItems)
            End If
            ImportClothingSalesFile("\\EU-FPS\Client_Data\Sainsburys\Dashboard\Clothing File\Clothing_Sales_2017.xls")
            dlgTasks.CompleteTask(4)
            Windows.Forms.Application.DoEvents()
            Cursor = Cursors.WaitCursor
            dlgTasks.UpdateStatus("Importing Clothing Shrink file ..")
            ImportClothingShrinkFile("\\EU-FPS\Client_Data\Sainsburys\Dashboard\Clothing File\Clothing_Shrink_2017.xls")
            dlgTasks.CompleteTask(5)
            Windows.Forms.Application.DoEvents()
        End If
        Cursor = Cursors.WaitCursor
        If Not chkSkipSuperCatVerifierImport.Checked Then
            dlgTasks.UpdateStatus("Importing SuperCatVerfier files..")
            ImportSuperCatVerifierDataOut59()
        End If
        
        dlgTasks.CompleteTask(6)
        Windows.Forms.Application.DoEvents()
        Cursor = Cursors.Default
        dlgTasks.Close()


    End Sub


    Private Sub LoadFiles()

        tspb.Visible = True

        Cursor = Cursors.WaitCursor

        'Wait for Background worker
        ' While bgwInfoWin.IsBusy()

        Try

            If (sDirectoryPath.Length = 0) OrElse (Not IO.Directory.Exists(sDirectoryPath)) Then
                MsgBox("Data Load Directory path is not setup!", MsgBoxStyle.Exclamation)
                Exit Sub
            End If

            If oFilters.Count = 0 Then
                MsgBox("No filters have been setup!", MsgBoxStyle.Exclamation)
                Exit Sub
            End If

            If txtStartDate.Text.Length = 0 OrElse txtEndDate.Text.Length = 0 Then
                MsgBox("Invalid Start/End date", MsgBoxStyle.Exclamation)
                Exit Sub
            End If

            'check start and end dates
            If dtpStart.Value.DayOfWeek <> DayOfWeek.Sunday OrElse dtpEnd.Value.DayOfWeek <> DayOfWeek.Saturday Then
                If MsgBox("Warning:" & vbNewLine & vbNewLine & _
                       "The start/end date is not expected Sunday/Saturday respectively." & vbNewLine & vbNewLine & _
                       "Do you want to continue?", MessageBoxIcon.Warning Or MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                    Exit Sub
                End If
            End If
            lvData.Items.Clear()

            sProcessStartDate = Me.txtStartDate.Text
            sProcessEndDate = Me.txtEndDate.Text


            lblStatus.Text = "Loading data from files..."
            tspb.Visible = True
            gbDates.Enabled = False
            gbData.Enabled = False
            FileToolStripMenuItem.Enabled = False

            Windows.Forms.Cursor.Current = Cursors.WaitCursor

            bgwLoadFiles.RunWorkerAsync()

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub
    Private Sub ReadMailItemsFromOutlook(sSubjectSearch As String, dtFrom As Date, dtTo As Date, sOutputFolder As String, sFileType As String, ByRef nMailItems As Integer)

        Dim outlookApplication As Microsoft.Office.Interop.Outlook.Application
        Dim outlookNamespace As Microsoft.Office.Interop.Outlook.NameSpace
        Dim inboxFolder As MAPIFolder
        Dim mailItems As Items
        Dim folderItems As Items
        Dim sSearchCriteria As String
        Dim nFilesRead As Integer
        Dim sAttachmentFileExt As String
        Dim listSavedAttachments As List(Of String)
        Dim xoCmd As SqlCommand
        Dim DBConn As SqlConnection


        'Cursor.Current = Cursors.WaitCursor

        ' MessageBox.Show(dtFrom.Value.ToShortDateString(),"nnn");
        listSavedAttachments = New List(Of String)
        nFilesRead = 0
        Try
            outlookApplication = New Microsoft.Office.Interop.Outlook.Application()
            outlookNamespace = outlookApplication.GetNamespace("MAPI")
            inboxFolder = outlookNamespace.GetDefaultFolder(OlDefaultFolders.olFolderInbox)
            sSearchCriteria = "@SQL=" + Convert.ToChar(34) + "urn:schemas:httpmail:subject" + Convert.ToChar(34) + " like '%" + sSubjectSearch + "%'"

            sSearchCriteria += " AND urn:schemas:httpmail:datereceived >='" + dtFrom.Date.ToString("dd/MM/yyyy") + "'"
            sSearchCriteria += " AND urn:schemas:httpmail:datereceived <='" + dtTo.Date.ToString("dd/MM/yyyy") + "'"

            mailItems = inboxFolder.Items
            folderItems = mailItems.Restrict(sSearchCriteria)
            Dim m As MailItem = TryCast(folderItems.GetFirst(), Microsoft.Office.Interop.Outlook.MailItem)
            'listFiles.Items.Clear()
            nMailItems = 0
            While Not m Is Nothing
                nMailItems += 1
                'MessageBox.Show(sEmailText.ToString(),"Email Details")
                If (m.Attachments.Count > 0) Then
                    For i As Integer = 1 To m.Attachments.Count
                        nFilesRead += 1
                        sAttachmentFileExt = m.Attachments(i).FileName.Split(".")(1)
                        If sAttachmentFileExt = sFileType Then
                            'MessageBox.Show(m.Attachments[i].FileName, "Email Attachment");
                            If listSavedAttachments.Contains(m.Attachments(i).FileName) Then
                                ' If file with same name has already been read during this import period then
                                ' write a record to the database
                                If IsNothing(cImportLog) Then
                                    cImportLog = New clsLogItem(oConn.ConnectionString)
                                End If
                                cImportLog.InsertLogRecord(5, 1, "File " & m.Attachments(i).FileName & " has already been read in import date range " & dtFrom.Date.ToString("dd/MM/yyyy") & " to " & dtTo.Date.ToString("dd/MM/yyyy"))

                            End If
                            ' Need to add code here to make saved file unique
                            ' with date of yyymmdd appended to end 
                            Dim sExt, sFileToSave As String

                            sFileToSave = m.Attachments(i).FileName
                            sExt = Path.GetExtension(sFileToSave)
                            If sExt = ".out59" Or sExt = ".out36" Then
                                sFileToSave = Path.GetFileNameWithoutExtension(sFileToSave)
                                sFileToSave = sFileToSave & "_" + m.ReceivedTime.ToString("MM.dd.yyyy hhmm") & sExt
                            End If
                            m.Attachments(i).SaveAsFile(sOutputFolder + "\\" + sFileToSave)
                            listSavedAttachments.Add(m.Attachments(i).FileName)
                        End If
                    Next
                End If
                m = TryCast(folderItems.GetNext(), MailItem)
            End While
        Catch e As System.Exception
            e = e
        Finally

            Marshal.ReleaseComObject(folderItems)
            Marshal.ReleaseComObject(mailItems)
            Marshal.ReleaseComObject(inboxFolder)
            Marshal.ReleaseComObject(outlookNamespace)
            Marshal.ReleaseComObject(outlookApplication)
        End Try
        'Cursor.Current = Cursors.Default
        'statusStrip1.Items[0].Text = nFilesRead.ToString() + " files read and saved to output folder";
    End Sub

    Private Sub ImportSuperCatVerifierData()
        Dim xsPath As String
        Dim sOrridgePath, sDefaultSuperCatFolder, sSourceFolder, sTargetFolder As String
        Dim nFilesProcessed As Integer
        Dim nFilesCopied As Integer
        Dim nFilesToProcess As Integer
        Dim headerDate As Date
        Dim SundayDate, SaturdayDate As Date
        Dim nDaysToSubtract As Integer
        Dim bDateObtained As Boolean


        ' Out36 files (SupercatVerifier)
        If 1 = 1 Then
            sSourceFolder = "\\custdata.rgis.com\div16$\SharedArea_Division_16\custdata\jsmain"
            sTargetFolder = "\\EU-FPS\Client_Data\Sainsburys\Dashboard\ImportArea\SuperCatVerifier"
            If Not IsNothing(dlgTasks) Then
                dlgTasks.UpdateStatus("Copying out36 files from " & sSourceFolder)
            End If

            nFilesCopied += CopyFiles(sSourceFolder, sTargetFolder, "out36")
            sSourceFolder = "\\custdata.rgis.com\div16$\SharedArea_Division_16\custdata\jscsto"
            If Not IsNothing(dlgTasks) Then
                dlgTasks.UpdateStatus("Copying out36 files from " & sSourceFolder)
            End If
            nFilesCopied += CopyFiles(sSourceFolder, sTargetFolder, "out36")

            sSourceFolder = "\\custdata.rgis.com\div16$\SharedArea_Division_16\custdata\jsta"
            If Not IsNothing(dlgTasks) Then
                dlgTasks.UpdateStatus("Copying out36 files from " & sSourceFolder)
            End If
            nFilesCopied += CopyFiles(sSourceFolder, sTargetFolder, "out36")

            sSourceFolder = "\\custdata.rgis.com\div16$\SharedArea_Division_16\custdata\jspfs"
            If Not IsNothing(dlgTasks) Then
                dlgTasks.UpdateStatus("Copying out36 files from " & sSourceFolder)
            End If
            nFilesCopied += CopyFiles(sSourceFolder, sTargetFolder, "out36")
            'End copy
        End If
        

        SaturdayDate = Now
        nDaysToSubtract = SaturdayDate.DayOfWeek + 1
        SaturdayDate = SaturdayDate.AddDays(-nDaysToSubtract)
        SundayDate = SaturdayDate.AddDays(-6)

        sDefaultSuperCatFolder = "S:\SupercatVerifier"

        ' Copy Orridge files where the date in the header row falls within the date range
        ' Have excluded this code as Orridge files are now read directly from Outlook
        If 1 = 1 Then
            sOrridgePath = "\\EU-FPS\Client_Data\Sainsburys\Dashboard\Orridge Supercat Verifier files"
            If Directory.Exists(sOrridgePath) Then
                Dim xasOrridgeFileList As String() = Directory.GetFiles(sOrridgePath, "*.out36")
                For Each sOrridgeFile As String In xasOrridgeFileList
                    ' Load file
                    Dim line As String
                    Using reader As New StreamReader(sOrridgeFile)
                        line = reader.ReadLine()
                    End Using
                    bDateObtained = True
                    Try
                        headerDate = Date.Parse(line.Substring(108, 9))
                    Catch
                        Dim strFile = Microsoft.VisualBasic.Right(sOrridgeFile, Len(sOrridgeFile) - InStrRev(sOrridgeFile, "\"))
                        MessageBox.Show("The date in file " + strFile + " in " + sOrridgePath + " cannot be extracted")
                        bDateObtained = False
                    End Try


                    If bDateObtained And (headerDate >= SundayDate And headerDate <= SaturdayDate) Then
                        ' If date within file falls within range then copy it to the main Supercat Verifier folder
                        Dim strFile = Microsoft.VisualBasic.Right(sOrridgeFile, Len(sOrridgeFile) - InStrRev(sOrridgeFile, "\"))
                        FileSystem.FileCopy(sOrridgeFile, sDefaultSuperCatFolder + "\" + strFile)

                    End If
                Next
            End If
        End If
       
        ' Now process all RGIS and Orridge out36 files that have been copied to the SuperCatVerifier folder
        xsPath = sDefaultSuperCatFolder
        If Directory.Exists(xsPath) Then
            ' JR - 25/04/2017 - Ensure only .out36 files are selected for processing
            Dim xasFileList As String() = Directory.GetFiles(xsPath, "*.out36")

            '   Count the files in the folder and confirm this is correct
            If xasFileList.Count = 0 Then
                MessageBox.Show("No files found")
                Exit Sub
            Else
                '   Process all the files - one by one
                ' Turn on wait cursor to inform user that application is busy
                tspb.Visible = True
                Me.Cursor = Cursors.WaitCursor

                nFilesToProcess = xasFileList.Count

                For Each xsFile As String In xasFileList
                    'txtFileNameAndPathSuperCat.Text = xsFile
                    'txtOutput.Text += vbCrLf & "Processing " & xsFile
                    'StatusBarText = "Processing " & xsFile
                    ProcessSuperCatVerifierData(xsFile)
                    nFilesProcessed += 1
                    'tspb.Value = (nFilesProcessed / nFilesToProcess) * 100

                Next
                'Me.Cursor = Cursors.Default
                'MessageBox.Show("Done")
                tspb.Visible = False

            End If
        Else
            MessageBox.Show("Entered folder does not exist, please re-select.", "Clothing Importer", MessageBoxButtons.OK, MessageBoxIcon.Warning)


        End If


    End Sub

    Private Sub ImportSuperCatVerifierDataOut59(Optional sDefaultSuperCatFolder As String = "S:\SupercatVerifier")
        Dim xsPath As String
        Dim sSourceFolder, sTargetFolder As String
        Dim nFilesProcessed, nFilesCopied As Integer
        Dim nFilesToProcess As Integer
        Dim SundayDate, SaturdayDate As Date
        Dim nDaysToSubtract As Integer

        ' Out59 files (SupercatVerifier)
        If 1 = 1 Then
            sSourceFolder = "\\custdata.rgis.com\div16$\SharedArea_Division_16\custdata\jsmain"
            sTargetFolder = "\\EU-FPS\Client_Data\Sainsburys\Dashboard\ImportArea\SuperCatVerifier"
            If Not IsNothing(dlgTasks) Then
                dlgTasks.UpdateStatus("Copying out59 files from " & sSourceFolder)
            End If
            nFilesCopied += CopyFiles(sSourceFolder, sTargetFolder, "out59")

            sSourceFolder = "\\custdata.rgis.com\div16$\SharedArea_Division_16\custdata\jscsto"
            If Not IsNothing(dlgTasks) Then
                dlgTasks.UpdateStatus("Copying out59 files from " & sSourceFolder)
            End If
            nFilesCopied += CopyFiles(sSourceFolder, sTargetFolder, "out59")

            sSourceFolder = "\\custdata.rgis.com\div16$\SharedArea_Division_16\custdata\jsta"
            If Not IsNothing(dlgTasks) Then
                dlgTasks.UpdateStatus("Copying out59 files from " & sSourceFolder)
            End If
            nFilesCopied += CopyFiles(sSourceFolder, sTargetFolder, "out59")

            sSourceFolder = "\\custdata.rgis.com\div16$\SharedArea_Division_16\custdata\jspfs"
            If Not IsNothing(dlgTasks) Then
                dlgTasks.UpdateStatus("Copying out59 files from " & sSourceFolder)
            End If
            nFilesCopied += CopyFiles(sSourceFolder, sTargetFolder, "out59")
            'End copy
        End If


        SaturdayDate = Now
        nDaysToSubtract = SaturdayDate.DayOfWeek + 1
        SaturdayDate = SaturdayDate.AddDays(-nDaysToSubtract)
        SundayDate = SaturdayDate.AddDays(-6)

        ' Copy Orridge files where the date in the header row falls within the date range

        ' Now process all RGIS and Orridge out36 files that have been copied to the SuperCatVerifier folder
        xsPath = sDefaultSuperCatFolder
        If Directory.Exists(xsPath) Then
            ' JR - 25/04/2017 - Ensure only .out36 files are selected for processing
            Dim xasFileList As String() = Directory.GetFiles(xsPath, "*.out59")
            Dim xasFileListOut36 As String() = Directory.GetFiles(xsPath, "*.out36")

            '   Process all the files - one by one
            ' Turn on wait cursor to inform user that application is busy
            tspb.Visible = True
            Me.Cursor = Cursors.WaitCursor

            nFilesToProcess = xasFileList.Count

            For Each xsFile As String In xasFileList
                ProcessSuperCatVerifierDataOut59(xsFile)
                nFilesProcessed += 1
            Next

            ' Read Orridge .out36 files, which from Jan 2018 will be replaced by out59 files
            ' This code block below can then be removed after Orridge send out59s
            For Each xsFile As String In xasFileListOut36
                ProcessSuperCatVerifierData(xsFile)
                nFilesProcessed += 1
            Next

            'Me.Cursor = Cursors.Default
            'MessageBox.Show("Done")
            tspb.Visible = False
        Else
            MessageBox.Show("Entered folder does not exist, please re-select.", "Sainsburys Data Import", MessageBoxButtons.OK, MessageBoxIcon.Warning)

        End If

        Me.Cursor = Cursors.Default

    End Sub

    Private Sub ProcessSuperCatVerifierDataOut59(sFileToProcess As String)
        Dim DBConn As SqlConnection
        Dim sLine As String
        Dim nLineCount As Integer
        Dim saItems() As String
        Dim xnStore As Integer = 0
        Dim xnZone As Integer = 0
        Dim xnRegion As Integer = 0
        Dim xsEstateType As String = ""
        Dim xsStoreName As String = ""
        Dim xsSuperCatName As String = ""
        Dim xsCurrentCountYear As String = ""
        Dim xsCurrentPeriod As String = ""
        Dim xbClothing As Boolean = False
        Dim xdReportDate As Date
        Dim xnSuperCatID As Integer
        Dim xnSKUsCounted As Integer
        Dim xnChangesMade As Integer
        Dim xsWrk, xsRtnMsg As String
        Dim xoCmd, xoCmd2, xoCmd3, xoCmd4 As New SqlCommand
        Dim xnUniqueSKUsCheckedBR As Integer
        Dim xnUniqueSKUsCheckedSF As Integer
        Dim xnSKUsCheckedPcnt As Decimal
        Dim xnAccuracy As Decimal
        Dim xnTotalUnitsChecked As Integer
        Dim xnTotalUnitsCounted As Integer
        Dim xnUnitsCheckedPcnt As Decimal
        Dim xnNumAmendmentsUnitsChecked As Integer
        Dim xnUnitACRPcnt As Decimal


        Dim xbIsTA As Boolean = False

        Try

            If Not File.Exists(sFileToProcess) Then
                MessageBox.Show("File name not found. Job cancelled.")
                Return
            End If
        Catch ex As System.Exception
            MessageBox.Show("Problem getting the input Supercat Verifier file . Job cancelled.")
            Return
        End Try
        Dim xbValid As Boolean = False

        xbValid = True
        DBConn = New SqlConnection(oConn.ConnectionString)

        DBConn.Open()
        Dim xoSR As New StreamReader(sFileToProcess)
        xoCmd = New SqlCommand
        xoCmd.Connection = DBConn
        xoCmd.CommandType = CommandType.StoredProcedure
        xoCmd.CommandText = "p_SuperCatVerifier_Insert_New"
        xoCmd.Parameters.Add("Store_Number", SqlDbType.Int)
        ' xoCmd.Parameters.Add("Store_Name", SqlDbType.NVarChar, 50).Value = xsStoreName
        xoCmd.Parameters.Add("ReportDate", SqlDbType.DateTime)
        xoCmd.Parameters.Add("Super_Category", SqlDbType.Int)
        xoCmd.Parameters.Add("Super_Category_Name", SqlDbType.NVarChar, 50)
        xoCmd.Parameters.Add("SKUsCounted", SqlDbType.Int)
        xoCmd.Parameters.Add("SKUsCheckedBR", SqlDbType.Int)
        xoCmd.Parameters.Add("SKUsCheckedSF", SqlDbType.Int)
        xoCmd.Parameters.Add("SKUsCheckedPcnt", SqlDbType.Decimal)
        xoCmd.Parameters.Add("NoOfChanges", SqlDbType.Int)
        xoCmd.Parameters.Add("AccuracyPcnt", SqlDbType.Decimal)
        xoCmd.Parameters.Add("TotalUnitsChecked", SqlDbType.Int)
        xoCmd.Parameters.Add("TotalUnitsCounted", SqlDbType.Int)
        xoCmd.Parameters.Add("UnitsCheckedPcnt", SqlDbType.Decimal)
        xoCmd.Parameters.Add("NumAmendmentsUnitsChecked", SqlDbType.Int)
        xoCmd.Parameters.Add("UnitACRPcnt", SqlDbType.Decimal)
        xoCmd.Parameters.Add("RowsUpdated", SqlDbType.Int).Direction = ParameterDirection.Output

        xoCmd2 = New SqlCommand
        xoCmd2.Connection = DBConn
        xoCmd2.CommandType = CommandType.StoredProcedure
        xoCmd2.CommandText = "p_GetPeriodAndCYForDate"
        xoCmd2.Parameters.Add("InputDate", SqlDbType.Date).Value = xdReportDate
        xoCmd2.Parameters.Add("Count_Year", SqlDbType.NVarChar, 10).Direction = ParameterDirection.Output
        xoCmd2.Parameters.Add("Period_No", SqlDbType.NVarChar, 10).Direction = ParameterDirection.Output

        xoCmd3 = New SqlCommand
        xoCmd3.Connection = DBConn
        xoCmd3.CommandType = CommandType.StoredProcedure
        xoCmd3.CommandText = "p_SuperCatVerifier_DeleteCountYearStorePeriod"
        xoCmd3.Parameters.Add("Count_Year", SqlDbType.NVarChar, 10)
        xoCmd3.Parameters.Add("Store_Number", SqlDbType.Int)
        xoCmd3.Parameters.Add("Period_No", SqlDbType.NVarChar, 10)

        xsRtnMsg = ""
        nLineCount = 0
        ' Read header row first
        sLine = xoSR.ReadLine()
        While xoSR.Peek <> -1
            sLine = xoSR.ReadLine()
            saItems = sLine.Split(vbTab)
            xnStore = Convert.ToInt32(saItems(0))
            xdReportDate = Convert.ToDateTime(saItems(1))
            If nLineCount = 0 Then
                ' First row, so get count year and period so that any existing data can be cleared out
                ' xocmd2
                xoCmd2.Parameters("InputDate").Value = xdReportDate
                xoCmd2.ExecuteNonQuery()

                ' Delete any existing data for Count Year/Period
                xoCmd3.Parameters("Count_Year").Value = xoCmd2.Parameters("Count_Year").Value
                xoCmd3.Parameters("Store_Number").Value = xnStore
                xoCmd3.Parameters("Period_No").Value = xoCmd2.Parameters("Period_No").Value
                xoCmd3.ExecuteNonQuery()

            End If
            xsSuperCatName = saItems(2)
            xnSuperCatID = Convert.ToInt32(saItems(3))
            xnSKUsCounted = Int32.Parse(saItems(4).Replace(",", ""))
            xnUniqueSKUsCheckedBR = Int32.Parse(saItems(5).Replace(",", ""))
            xnUniqueSKUsCheckedSF = Int32.Parse(saItems(6).Replace(",", ""))
            xnSKUsCheckedPcnt = Convert.ToDecimal(saItems(7).Replace("%", ""))
            xnChangesMade = Convert.ToInt32(saItems(8))
            If saItems(9) <> "" Then
                xnAccuracy = Convert.ToDecimal(saItems(9).Replace("%", ""))
                xoCmd.Parameters("AccuracyPcnt").Value = xnAccuracy
            Else
                xoCmd.Parameters("AccuracyPcnt").Value = DBNull.Value

            End If

            xnTotalUnitsChecked = Int32.Parse(saItems(10).Replace(",", ""))
            xnTotalUnitsCounted = Int32.Parse(saItems(11).Replace(",", ""))
            xnUnitsCheckedPcnt = Convert.ToDecimal(saItems(12).Replace("%", ""))
            xnNumAmendmentsUnitsChecked = Int32.Parse(saItems(13).Replace(",", ""))
            If saItems(14) <> "" Then
                xnUnitACRPcnt = Convert.ToDecimal(saItems(14).Replace("%", ""))
                xoCmd.Parameters("UnitACRPcnt").Value = xnUnitACRPcnt
            Else
                xoCmd.Parameters("UnitACRPcnt").Value = DBNull.Value

            End If

            Try
                'xoCmd.Parameters.Add("Count_Year", SqlDbType.NVarChar).Value = xsCurrentCountYear
                xoCmd.Parameters("Store_Number").Value = xnStore
                ' xoCmd.Parameters.Add("Store_Name", SqlDbType.NVarChar, 50).Value = xsStoreName
                xoCmd.Parameters("ReportDate").Value = xdReportDate
                'xoCmd.Parameters.Add("Period_No", SqlDbType.NVarChar).Value = xsCurrentPeriod
                ' xoCmd.Parameters.Add("Clothing", SqlDbType.Bit).Value = xbClothing
                xoCmd.Parameters("Super_Category").Value = xnSuperCatID
                xoCmd.Parameters("Super_Category_Name").Value = xsSuperCatName
                xoCmd.Parameters("SKUsCounted").Value = xnSKUsCounted
                xoCmd.Parameters("SKUsCheckedBR").Value = xnUniqueSKUsCheckedBR
                xoCmd.Parameters("SKUsCheckedSF").Value = xnUniqueSKUsCheckedSF
                xoCmd.Parameters("SKUsCheckedPcnt").Value = xnSKUsCheckedPcnt
                xoCmd.Parameters("NoOfChanges").Value = xnChangesMade
                xoCmd.Parameters("TotalUnitsChecked").Value = xnTotalUnitsChecked
                xoCmd.Parameters("TotalUnitsCounted").Value = xnTotalUnitsCounted
                xoCmd.Parameters("UnitsCheckedPcnt").Value = xnUnitsCheckedPcnt
                xoCmd.Parameters("NumAmendmentsUnitsChecked").Value = xnNumAmendmentsUnitsChecked
                xoCmd.Parameters("RowsUpdated").Direction = ParameterDirection.Output
                xoCmd.ExecuteNonQuery()
            Finally


            End Try

            nLineCount += 1

        End While




    End Sub

    Private Sub ProcessSuperCatVerifierData(sFileToProcess As String)

        '   Get input data/Open the file
        '   Get the file name
        Dim xsRtnMsg As String = ""
        Dim xbFirstDataRowFound As Boolean
        Dim DBConn As SqlConnection

        Try

            If Not File.Exists(sFileToProcess) Then
                MessageBox.Show("File name not found. Job cancelled.")
                Return
            End If
        Catch ex As System.Exception
            MessageBox.Show("Problem getting the input Supercat Verifier file . Job cancelled.")
            Return
        End Try

        '   Delete the output file then load the object ready to write the rows
        'If File.Exists(txtOPFileName.Text) Then
        'Kill(txtOPFileName.Text)
        'End If

        Dim xoSR As New StreamReader(sFileToProcess)

        Dim xbValid As Boolean = False

        'StatusBarText = "Setting up the expected columns and clearing existing data."

        'Dim xsConnStr As String = "Data Source=" & xoLic.DBLocn & ";Initial Catalog=" & xoLic.DBName & "; Integrated Security=False; User ID=sa; Password=CjC7vHYB2yESaBgq"

        'Dim xsConnStr As String = "Server=CND42412H2\STPL_08;Initial Catalog=Sainsburys;Integrated Security=SSPI;"        '   Clear the work tables
        xbValid = True
        DBConn = New SqlConnection(oConn.ConnectionString)
        Dim xoCmd As New SqlCommand
        DBConn.Open()

        '   Open the file as a stream
        Try

            Dim xbStoreFound As Boolean = False
            Dim xbHeadingFound As Boolean = False
            Dim xnWrk As Integer = 0
            Dim xsWrk As String = ""
            Dim xnRowCount As Integer = 0
            Dim xnStore As Integer = 0
            Dim xnZone As Integer = 0
            Dim xnRegion As Integer = 0
            Dim xsEstateType As String = ""
            Dim xsStoreName As String = ""
            Dim xsSuperCatName As String = ""
            Dim xsCurrentCountYear As String = ""
            Dim xsCurrentPeriod As String = ""
            Dim xbClothing As Boolean = False
            Dim xdReportDate As Date
            Dim xbIsTA As Boolean = False

            '   The column splits prior to 5th Dec 2014 were
            '   1-40, 43-51, 53-66, 70-82, 86-99, 102-115, 118-131
            '   Post are 

            Dim xoRptCols As New RptCols
            xoRptCols.RptColsIP = New Collection
            Dim xoRptCol As New RptCol

            xoRptCol.ColHdgInput = "Super Cat Name"
            xoRptCol.ColName = "Super Cat Name"
            xoRptCol.ColStartInput = 0
            xoRptCol.ColEndInput = 40
            xoRptCol.ColDataType = RptCol.ColDataTypes.DTString
            xoRptCols.RptColsIP.Add(xoRptCol)

            xoRptCol = New RptCol
            xoRptCol.ColHdgInput = "Super Cat"
            xoRptCol.ColName = "Super Cat"
            xoRptCol.ColStartInput = 42
            xoRptCol.ColEndInput = 50
            xoRptCol.ColDataType = RptCol.ColDataTypes.DTInteger
            xoRptCols.RptColsIP.Add(xoRptCol)

            xoRptCol = New RptCol
            xoRptCol.ColHdgInput = "Unique # SKUs Counted"
            xoRptCol.ColName = "SKUs Counted"
            xoRptCol.ColStartInput = 52
            xoRptCol.ColEndInput = 64
            xoRptCol.ColDataType = RptCol.ColDataTypes.DTInteger
            xoRptCols.RptColsIP.Add(xoRptCol)

            xoRptCol = New RptCol
            xoRptCol.ColHdgInput = "Unique # SKUs Checked in BR"
            xoRptCol.ColName = "SKUs Checked BR"
            xoRptCol.ColStartInput = 66
            xoRptCol.ColEndInput = 79
            xoRptCol.ColDataType = RptCol.ColDataTypes.DTInteger
            xoRptCols.RptColsIP.Add(xoRptCol)

            xoRptCol = New RptCol
            xoRptCol.ColHdgInput = "Unique # SKUs Checked in SF"
            xoRptCol.ColName = "SKUs Checked SF"
            xoRptCol.ColStartInput = 81
            xoRptCol.ColEndInput = 94
            xoRptCol.ColDataType = RptCol.ColDataTypes.DTInteger
            xoRptCols.RptColsIP.Add(xoRptCol)

            xoRptCol = New RptCol
            xoRptCol.ColHdgInput = "% SKUs Checked"
            xoRptCol.ColName = "SKUs Checked Pcnt"
            xoRptCol.ColStartInput = 96
            xoRptCol.ColEndInput = 104
            xoRptCol.ColDataType = RptCol.ColDataTypes.DTDecimal
            xoRptCol.ColDecimals = 2
            xoRptCols.RptColsIP.Add(xoRptCol)

            xoRptCol = New RptCol
            xoRptCol.ColHdgInput = "# of Ammendment Made"
            xoRptCol.ColName = "Changes made"
            xoRptCol.ColStartInput = 108
            xoRptCol.ColEndInput = 121
            xoRptCol.ColDataType = RptCol.ColDataTypes.DTInteger
            xoRptCols.RptColsIP.Add(xoRptCol)

            xoRptCol = New RptCol
            xoRptCol.ColHdgInput = "Accuracy %"
            xoRptCol.ColName = "Accuracy Pcnt"
            xoRptCol.ColStartInput = 123
            xoRptCol.ColEndInput = 132
            xoRptCol.ColDataType = RptCol.ColDataTypes.DTDecimal
            xoRptCol.ColDecimals = 2
            xoRptCols.RptColsIP.Add(xoRptCol)

            ' S:\SuperCatVerifier\ORRIDGE - SuperCat_Verifier_148.out36

            Dim xsLine As String = xoSR.ReadLine()
            Do While Not xoSR.EndOfStream

                xnRowCount += 1

                '   Get the Store ID
                If Not xbStoreFound Then
                    If xnRowCount > 12 Then
                        MessageBox.Show("Store not found in 1st 12 rows. Probable data format issue. Job cancelled")
                        Exit Sub
                    End If

                    xsWrk = xsLine.Substring(0, 20).Trim
                    If xsWrk.Length > 0 Then
                        xnWrk = xsWrk.IndexOf("Store:")
                        If xnWrk > -1 Then
                            xsWrk = xsWrk.Substring(xnWrk + 7)

                            'JR - 19/09/2017
                            'Remove any leading space(s) before store number
                            xsWrk = LTrim(xsWrk)

                            ' JR - 14/03/2017
                            ' Remove any leading comma before the store number to prevent a conversion error
                            If xsWrk.Contains(",") Then
                                xsWrk = xsWrk.Replace(",", "")
                            End If

                            Dim xnEnd As Integer = xsWrk.IndexOf(" ")

                            If xnEnd > -1 Then
                                xsWrk = xsWrk.Substring(0, xnEnd)
                                xnStore = CType(xsWrk, Integer)

                                '   Get the report date
                                xsWrk = xsLine.Substring(108, 14)
                                xdReportDate = CDate(xsWrk)

                                xoCmd = New SqlCommand
                                xoCmd.Connection = DBConn
                                xoCmd.CommandType = CommandType.StoredProcedure

                                Try
                                    xoCmd.CommandText = "p_GetPeriodAndCYForDate"
                                    xoCmd.Parameters.Add("InputDate", SqlDbType.Date).Value = xdReportDate
                                    xoCmd.Parameters.Add("Count_Year", SqlDbType.NVarChar, 10).Direction = ParameterDirection.Output
                                    xoCmd.Parameters.Add("Period_No", SqlDbType.NVarChar, 10).Direction = ParameterDirection.Output
                                    xoCmd.ExecuteNonQuery()

                                    xsCurrentCountYear = ""
                                    xsWrk = xoCmd.Parameters("Count_Year").Value.ToString
                                    If xsWrk.Length > 0 Then
                                        xsCurrentCountYear = xsWrk
                                    End If

                                    xsCurrentPeriod = ""
                                    xsWrk = xoCmd.Parameters("Period_No").Value.ToString
                                    If xsWrk.Length > 0 Then
                                        xsCurrentPeriod = xsWrk
                                    End If

                                    '   Clear the rows (if any) that are in the table
                                    xoCmd = New SqlCommand
                                    xoCmd.Connection = DBConn
                                    xoCmd.CommandType = CommandType.StoredProcedure

                                    Try
                                        xoCmd.CommandText = "p_SuperCatVerifier_DeleteCountYearStorePeriod"
                                        xoCmd.Parameters.Add("Count_Year", SqlDbType.NVarChar).Value = xsCurrentCountYear.TrimEnd()
                                        xoCmd.Parameters.Add("Store_Number", SqlDbType.Int).Value = xnStore
                                        xoCmd.Parameters.Add("Period_No", SqlDbType.NVarChar).Value = xsCurrentPeriod.TrimEnd()
                                        xoCmd.Parameters.Add("RowsUpdated", SqlDbType.Int).Direction = ParameterDirection.Output
                                        xoCmd.ExecuteNonQuery()

                                    Catch ex As System.Exception
                                        xsRtnMsg += "Problem when clearing TB_SuperCatVerifier. Error " & ex.Message & " "
                                    End Try


                                Catch ex As System.Exception
                                    xsRtnMsg += "Problem when processing header data. Error " & ex.Message & " "
                                End Try

                                xoCmd = New SqlCommand
                                xoCmd.Connection = DBConn
                                xoCmd.CommandType = CommandType.StoredProcedure

                                Try
                                    xoCmd.CommandText = "p_GetZoneRegionForStore"
                                    xoCmd.Parameters.Add("Count_Year", SqlDbType.NVarChar).Value = xsCurrentCountYear
                                    xoCmd.Parameters.Add("Store_Number", SqlDbType.Int).Value = xnStore
                                    xoCmd.Parameters.Add("Estate_Type", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Output
                                    xoCmd.Parameters.Add("Zone", SqlDbType.Int).Direction = ParameterDirection.Output
                                    xoCmd.Parameters.Add("Region", SqlDbType.Int).Direction = ParameterDirection.Output
                                    xoCmd.Parameters.Add("Store_Name", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Output
                                    xoCmd.ExecuteNonQuery()

                                    xsEstateType = xoCmd.Parameters("Estate_Type").Value.ToString
                                    If xsWrk.Length = 0 Then
                                        xsRtnMsg += "Problem during p_GetZoneRegionForStore: Missing Store type for " & xnStore & ". "
                                    End If

                                    xnZone = 0
                                    xsWrk = xoCmd.Parameters("Zone").Value.ToString
                                    If xsWrk.Length > 0 AndAlso IsNumeric(xsWrk) Then
                                        xnZone = CInt(xsWrk)
                                        If xnZone <= 0 Then
                                            xsRtnMsg += "Problem during p_GetZoneRegionForStore: Missing Zone for " & xnStore & ". "
                                            xbValid = False
                                        End If
                                    End If

                                    xnRegion = 0
                                    xsWrk = xoCmd.Parameters("Region").Value.ToString
                                    If xsWrk.Length > 0 AndAlso IsNumeric(xsWrk) Then
                                        xnRegion = CInt(xsWrk)
                                        If xnRegion <= 0 Then
                                            xsRtnMsg += "Problem during p_GetZoneRegionForStore: Missing Region for " & xnStore & ". "
                                            xbValid = False
                                        End If
                                    End If

                                    xsStoreName = ""
                                    xsWrk = xoCmd.Parameters("Store_Name").Value.ToString
                                    If xsWrk.Length > 0 Then
                                        xsStoreName = xsWrk
                                    End If

                                    xoCmd = New SqlCommand
                                    xoCmd.Connection = DBConn
                                    xoCmd.CommandType = CommandType.StoredProcedure

                                    Try
                                        xoCmd.CommandText = "p_IsTransactionTA"
                                        xoCmd.Parameters.Add("Store_Number", SqlDbType.Int).Value = xnStore
                                        xoCmd.Parameters.Add("InputDate", SqlDbType.Date).Value = xdReportDate
                                        xoCmd.Parameters.Add("IsTA", SqlDbType.NVarChar, 5).Direction = ParameterDirection.Output
                                        xoCmd.ExecuteNonQuery()

                                        xbIsTA = False
                                        xsWrk = xoCmd.Parameters("IsTA").Value.ToString
                                        If xsWrk.Length > 0 Then
                                            xbIsTA = CType(xsWrk, Boolean)
                                        End If

                                        '   Clear the rows (if any) that are in the table
                                        xoCmd = New SqlCommand
                                        xoCmd.Connection = DBConn
                                        xoCmd.CommandType = CommandType.StoredProcedure

                                    Catch ex As System.Exception
                                        xsRtnMsg += "Problem when detecting TA date. Error " & ex.Message & " "
                                    End Try


                                    '   Clear the rows (if any) that are in the table
                                    xoCmd = New SqlCommand
                                    xoCmd.Connection = DBConn
                                    xoCmd.CommandType = CommandType.StoredProcedure

                                    Try
                                        xoCmd.CommandText = "p_SuperCatVerifier_DeleteCountYearStorePeriod"
                                        xoCmd.Parameters.Add("Count_Year", SqlDbType.NVarChar).Value = xsCurrentCountYear.TrimEnd()
                                        xoCmd.Parameters.Add("Store_Number", SqlDbType.Int).Value = xnStore
                                        xoCmd.Parameters.Add("Period_No", SqlDbType.NVarChar).Value = xsCurrentPeriod.TrimEnd()
                                        xoCmd.Parameters.Add("RowsUpdated", SqlDbType.Int).Direction = ParameterDirection.Output
                                        xoCmd.ExecuteNonQuery()

                                    Catch ex As System.Exception
                                        xsRtnMsg += "Problem when clearing TB_SuperCatVerifier. Error " & ex.Message & " "
                                    End Try


                                Catch ex As System.Exception
                                    xsRtnMsg += "Problem when processing header data. Error " & ex.Message & " "
                                End Try

                                xbClothing = "False"

                                xbStoreFound = True

                            End If

                        End If

                    End If

                Else

                    '  Ignore blank rows (blank between chars 1-20)
                    xsWrk = xsLine.Substring(0, 20).Trim
                    If xsWrk.Length > 0 Then

                        If Not xbHeadingFound Then
                            xnWrk = xsWrk.IndexOf("------", 1)
                            If xnWrk > 0 Then
                                xbHeadingFound = True
                            End If

                        Else

                            '   Dont process a blank line or the Grand Total line
                            xsWrk = xsLine.Substring(0, 20).Trim
                            If xsWrk.Length > 0 And xsLine.IndexOf("Grand Total") = -1 Then

                                If Not xbFirstDataRowFound Then
                                    'StatusBarText = "Processing report rows."
                                    xbFirstDataRowFound = True
                                End If

                                '   Confirm data format
                                Dim xnColCount As Integer = -1
                                Dim xnSuperCatID As Integer = 0
                                Dim xnSKUsCounted As Integer = 0
                                Dim xnSKUsCheckedPcnt As Decimal = 0
                                Dim xnSKUsCheckedBR As Decimal = 0
                                Dim xnSKUsCheckedSF As Decimal = 0
                                Dim xnSKUsCheckedcalc As Integer = 0
                                Dim xnChangesMade As Integer = 0
                                Dim xnAccuracy As Decimal = 0

                                '   Load data columns

                                For Each xoCol As RptCol In xoRptCols.RptColsIP
                                    xsWrk = xsLine.Substring(xoCol.ColStartInput, xoCol.ColEndInput - xoCol.ColStartInput).Trim
                                    xoCol.ColStringValue = xsWrk
                                    '        MsgBox(xoCol.ColName)
                                    xnColCount += 1
                                    Select Case xoCol.ColName
                                        Case Is = "Super Cat Name"
                                            xsSuperCatName = xoCol.ColStringValue

                                        Case Is = "Super Cat"
                                            If xsWrk.Length > 0 Then
                                                xnSuperCatID = CType(xsWrk, Integer)

                                                xoCmd = New SqlCommand
                                                xoCmd.Connection = DBConn
                                                xoCmd.CommandType = CommandType.StoredProcedure

                                                xoCmd.CommandText = "p_IsSuperCatInClothing"
                                                xoCmd.Parameters.Add("Count_Year", SqlDbType.NVarChar).Value = xsCurrentCountYear
                                                xoCmd.Parameters.Add("Super_Category", SqlDbType.Int).Value = xnSuperCatID
                                                xoCmd.Parameters.Add("Clothing", SqlDbType.Bit).Direction = ParameterDirection.Output
                                                xoCmd.Parameters.Add("Super_Category_Name", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Output
                                                xoCmd.ExecuteNonQuery()

                                                xsWrk = xoCmd.Parameters("Clothing").Value.ToString
                                                xbClothing = False
                                                If xsWrk.Length > 0 Then
                                                    If xsWrk = "1" Or xsWrk = "True" Then
                                                        xbClothing = True
                                                    End If
                                                End If

                                                xsWrk = xoCmd.Parameters("Super_Category_Name").Value.ToString
                                                xsSuperCatName = ""
                                                If xsWrk.Length > 0 Then
                                                    xsSuperCatName = xsWrk
                                                End If
                                            End If

                                        Case Is = "SKUs Counted"
                                            If xsWrk.Length > 0 Then
                                                xnSKUsCounted = CType(xsWrk, Integer)
                                            End If

                                        Case Is = "SKUs Checked BR"
                                            If xsWrk.Length > 0 Then
                                                xnSKUsCheckedBR = CType(xsWrk, Integer)
                                            End If

                                        Case Is = "SKUs Checked SF"
                                            If xsWrk.Length > 0 Then
                                                xnSKUsCheckedSF = CType(xsWrk, Integer)
                                            End If

                                        Case Is = "SKUs Checked Pcnt"
                                            If xsWrk.Length > 0 Then
                                                xnSKUsCheckedPcnt = CType(xsWrk, Decimal)
                                            End If

                                        Case Is = "Changes made"
                                            If xsWrk.Length > 0 Then
                                                xnChangesMade = CType(xsWrk, Integer)
                                            End If

                                        Case Is = "Accuracy Pcnt"
                                            If xsWrk.Length > 0 Then
                                                xnAccuracy = CType(xsWrk, Decimal)
                                            End If
                                    End Select
                                Next

                                'xoRptCols.RptColsIP(0).Item(0).ToString()

                                '   Load the data row

                                '   Write to the SuperCatVerifier table
                                xoCmd = New SqlCommand
                                xoCmd.Connection = DBConn
                                xoCmd.CommandType = CommandType.StoredProcedure

                                Try
                                    xoCmd.CommandText = "p_SuperCatVerifier_Insert"
                                    xoCmd.Parameters.Add("Count_Year", SqlDbType.NVarChar).Value = xsCurrentCountYear
                                    xoCmd.Parameters.Add("Estate_Type", SqlDbType.NVarChar, 50).Value = xsEstateType
                                    xoCmd.Parameters.Add("Zone", SqlDbType.Int).Value = xnZone
                                    xoCmd.Parameters.Add("Region", SqlDbType.Int).Value = xnRegion
                                    xoCmd.Parameters.Add("Store_Number", SqlDbType.Int).Value = xnStore
                                    xoCmd.Parameters.Add("Store_Name", SqlDbType.NVarChar, 50).Value = xsStoreName
                                    xoCmd.Parameters.Add("ReportDate", SqlDbType.DateTime).Value = xdReportDate
                                    xoCmd.Parameters.Add("Period_No", SqlDbType.NVarChar).Value = xsCurrentPeriod
                                    xoCmd.Parameters.Add("Clothing", SqlDbType.Bit).Value = xbClothing
                                    xoCmd.Parameters.Add("Super_Category", SqlDbType.Int).Value = xnSuperCatID
                                    xoCmd.Parameters.Add("Super_Category_Name", SqlDbType.NVarChar, 50).Value = xsSuperCatName
                                    'Change next line to back calculate skus checked from skus counted/skus checked pcnt remember to trap divzero
                                    '     xoCmd.Parameters.Add("SKUsChecked", SqlDbType.Int).Value = xnSKUsCheckedBR + xnSKUsCheckedSF

                                    If xnSKUsCheckedPcnt = 0 Then
                                        xnSKUsCheckedcalc = 0
                                    Else
                                        xnSKUsCheckedcalc = xnSKUsCounted * (xnSKUsCheckedPcnt / 100)
                                    End If
                                    xoCmd.Parameters.Add("SKUsCheckedBR", SqlDbType.Int).Value = xnSKUsCheckedBR
                                    xoCmd.Parameters.Add("SKUsCheckedSF", SqlDbType.Int).Value = xnSKUsCheckedSF
                                    xoCmd.Parameters.Add("SKUsChecked", SqlDbType.Int).Value = xnSKUsCheckedcalc

                                    xoCmd.Parameters.Add("SKUsCounted", SqlDbType.Int).Value = xnSKUsCounted
                                    xoCmd.Parameters.Add("SKUsCheckedPcnt", SqlDbType.Decimal).Value = xnSKUsCheckedPcnt
                                    xoCmd.Parameters.Add("NoOfChanges", SqlDbType.Int).Value = xnChangesMade
                                    xoCmd.Parameters.Add("AccuracyPcnt", SqlDbType.Decimal).Value = xnAccuracy

                                    xoCmd.Parameters.Add("RowsUpdated", SqlDbType.Int).Direction = ParameterDirection.Output


                                    xoCmd.ExecuteNonQuery()

                                    xsWrk = xoCmd.Parameters("RowsUpdated").Value.ToString
                                    If xsWrk.Length > 0 AndAlso IsNumeric(xsWrk) Then

                                        Dim xnWrk2 As Integer = CInt(xsWrk)
                                        If xnWrk2 <> 1 Then
                                            xsRtnMsg += "Problem during p_SuperCatVerifier_Insert. "
                                            xbValid = False
                                        End If
                                    End If

                                Catch ex As System.Exception
                                    xsRtnMsg += "Problem when updating data. Error " & ex.Message & " "
                                    MessageBox.Show("Error during Insert: Error " & ex.Message)
                                End Try

                            End If
                        End If
                    End If

                End If

                '   When the Grand Total line is encountered, terminate the run
                If xsWrk.Length > 0 And xsLine.IndexOf("Grand Total") >= 0 Then
                    Exit Do
                End If

                xsLine = xoSR.ReadLine()

            Loop

        Catch ex As System.Exception
            MessageBox.Show("Problem getting the excel file . Job cancelled. " & ex.Message & " " & sFileToProcess)
            Return
        Finally

            If Not IsNothing(DBConn) Then
                DBConn.Close()
                xoCmd = Nothing
                DBConn = Nothing
            End If
        End Try


    End Sub



    Private Sub ImportClothingShrinkFile(sFileToProcess As String)


        '   Import data into Sansbury's Clothing Staging table
        Dim DBConn As SqlConnection
        Dim xnWSCount As Integer = 0
        Dim xsRtnMsg As String = ""
        Dim xnIPRowCount As Integer = 0
        Dim xnAddCount As Integer = 0

        Dim xadDates() As Date = Nothing
        Dim xanDateCol() As Integer = Nothing
        Dim xanDateValSales() As Decimal = Nothing
        Dim xasDateCodeCol() As String = Nothing
        'Dim xanDateValExposure() As Decimal = Nothing
        Dim xanDateValClExpAdj() As Decimal = Nothing
        Dim xanDateValRGISExpAdj() As Decimal = Nothing
        Dim xasCountYear() As String = Nothing
        Dim xasPeriodNo() As String = Nothing

        Try

            Dim xbValid As Boolean = False

            xbValid = True

            '   Clear the work tables
            DBConn = New SqlConnection(oConn.ConnectionString)
            Dim xoCmd As New SqlCommand

            DBConn.Open()


            '   Open the workbook
            Dim xoExcel As Excel.Application
            Dim xoExWS As New Excel.Worksheet
            Dim xoExWB As Excel.Workbook

            'StatusBarText = "Opening Excel file. "
            Cursor = Cursors.WaitCursor

            xoExcel = New Excel.Application

            xoExWB = xoExcel.Workbooks.Open(sFileToProcess)
            xoExcel.Visible = False
            xoExcel.Application.DisplayAlerts = False

            'StatusBarText = "Processing the worksheets."

            ' Jeremy Rogers - 15/08/2017
            ' Changed code so that it only reads data from last sheet in the Workbook
            xoExWS = xoExWB.Worksheets(xoExWB.Worksheets.Count)


            Dim xsWSName As String = xoExWS.Name
            Dim xbDatesFound As Boolean = False
            Dim xnDateCount As Integer = 0

            ReDim xadDates(xnDateCount)
            ReDim xanDateCol(xnDateCount)
            ReDim xasDateCodeCol(xnDateCount)
            ReDim xanDateValSales(xnDateCount)
            'ReDim xanDateValExposure(xnDateCount)
            ReDim xanDateValClExpAdj(xnDateCount)
            ReDim xanDateValRGISExpAdj(xnDateCount)
            ReDim xasCountYear(xnDateCount)
            ReDim xasPeriodNo(xnDateCount)

            '   Determine the sheet type
            Dim xnWrk As Integer = 0
            'txtOutput.Text += xoExWS.Name & " " & vbCrLf
            StatusBarText = "Processing worksheet " & xoExWS.Name & "."

            '   Get the date range
            Dim xsDate As String = ""
            Dim xnDateStartCol As Integer = 6
            Dim xnDateStartRow As Integer = 1
            Dim xnDateEndCol As Integer = 0
            Dim xnRow As Integer = 1
            Dim xnCol As Integer = 1
            Dim xnColForThisDate As Integer = 0

            Dim CellData As Object(,) = xoExWS.UsedRange.Value
            Dim xnRowCount As Integer = xoExWS.UsedRange.Rows.Count

            xoExWB.Close()
            xoExcel.Quit()
            xoExWS = Nothing
            xoExWB = Nothing
            xoExcel = Nothing

            '   Find the "Day" row
            '   The word "Day" appears in the cell preceding the row of dates. This can be in any row from 5-10 and any column from E(5) to K(11)
            xnRow = 5
            Dim xbFound As Boolean = False
            For xnRow = 5 To 10
                For xnCol = 5 To 11
                    If Not IsNothing(CellData(xnRow, xnCol)) AndAlso CellData(xnRow, xnCol).ToString.ToUpper = "DAY" Then
                        xbFound = True
                        Exit For
                    End If
                Next
                If xbFound Then Exit For
            Next

            '   Confirm that the word "Reason" is two cells below this. We use this to deduce what type of shrink transaction is being given
            If IsNothing(CellData(xnRow + 2, xnCol)) OrElse CellData(xnRow + 2, xnCol).ToString.ToUpper <> "REASON" Then
                Cursor = Cursors.Default
                MessageBox.Show("The 'Reason' row wasn't found two rows below 'Day'. The worksheet " & xsWSName & " is not in the correct format")
                Exit Sub
            End If


            '   Now extract the dates from the cells. There can be 1-3 columns per date with no rhyme or reason as to why (!)
            '   We need to record what date is relevant to each column so that the figures on each row can be totalled for the table update.
            '   The assumption is that all columns for a given date are to be added together and that they appear together (i.e. are not jumbled up in the column order)
            xnDateStartCol = xnCol + 1
            xnDateStartRow = xnRow
            Dim xdsvDate As Date = Nothing
            Dim xdWrk As Date = Nothing
            xnDateCount = -1

            For xnCol = xnDateStartCol To 30

                If IsNothing(CellData(xnRow, xnCol)) Then
                    Exit For
                End If

                xsDate = CellData(xnRow, xnCol).ToString()
                If xsDate.Length = 0 Or Not IsDate(xsDate) Then
                    Exit For
                End If

                xdWrk = CDate(xsDate)
                If IsNothing(xdsvDate) OrElse xdsvDate <> xdWrk Then

                    xnDateCount += 1
                    ReDim Preserve xadDates(xnDateCount)
                    Dim xdDate As Date = CDate(xsDate)
                    xadDates(xnDateCount) = xdDate
                    xbDatesFound = True
                    xdsvDate = xdWrk

                    '   Get CurrentYear/Period from date
                    ReDim Preserve xasCountYear(xnDateCount)
                    ReDim Preserve xasPeriodNo(xnDateCount)

                    Try
                        xoCmd = New SqlCommand
                        xoCmd.Connection = DBConn
                        xoCmd.CommandType = CommandType.StoredProcedure

                        xoCmd.CommandText = "p_GetPeriodAndCYForDate"
                        xoCmd.Parameters.Add("InputDate", SqlDbType.Date).Value = xadDates(xnDateCount)
                        xoCmd.Parameters.Add("Count_Year", SqlDbType.NVarChar, 10).Direction = ParameterDirection.Output
                        xoCmd.Parameters.Add("Period_No", SqlDbType.NVarChar, 10).Direction = ParameterDirection.Output
                        xoCmd.ExecuteNonQuery()

                        Dim xsCurrentCountYear As String = ""
                        Dim xsWrk As String = xoCmd.Parameters("Count_Year").Value.ToString
                        If xsWrk.Length > 0 Then
                            xasCountYear(xnDateCount) = xsWrk
                        End If

                        Dim xsCurrentPeriod As String = ""
                        xsWrk = xoCmd.Parameters("Period_No").Value.ToString
                        If xsWrk.Length > 0 Then
                            xasPeriodNo(xnDateCount) = xsWrk
                        End If

                    Catch ex As System.Exception
                        xsRtnMsg += "Problem when processing header data. Error " & ex.Message & " "
                    End Try
                End If

                ReDim Preserve xanDateCol(xnCol)
                xanDateCol(xnCol) = xnDateCount

                ReDim Preserve xasDateCodeCol(xnCol)
                xasDateCodeCol(xnCol) = CellData(xnRow + 2, xnCol).ToString()

            Next

            xnDateEndCol = xnCol

            If Not xbDatesFound Then
                xsRtnMsg += "No dates found on worksheet " & xsWSName
                'txtOutput.Text += "No dates found in " & xsWSName & vbCrLf
                ' Need to break out function ?
            End If

            xnRow = 1
            Do While CellData(xnRow, 1) <> "SSL" And xnRow < xnRowCount
                xnRow += 1
            Loop

            If xnRow >= xnRowCount Then
                'txtOutput.Text += "No data found in " & xsWSName & vbCrLf
            End If

            '   First data row found so process all the following non blank rows
            Do While xnRow < xnRowCount And CellData(xnRow, 1).Length > 0
                Dim xdDate As Date? = Nothing
                Dim xsStore As String = ""
                Dim xnStore As Integer = 0

                xnIPRowCount += 1

                'xnRow += 1
                xsRtnMsg = "a" & xnRow

                xsStore = CellData(xnRow, 4)
                If Not IsNothing(xsStore) And xsStore.Length > 0 Then
                    xnStore = CInt(xsStore)
                    If xnStore > 0 Then
                        '   Get the figure for each day
                        '   Note that (at this time) there will be an output row for each day, even for zero values
                        '   The s-p Inserts if there's no row there or updates if one does exist
                        '   We work through all columns to accumulate date and then output all dates at one time

                        ReDim xanDateValClExpAdj(xadDates.GetUpperBound(0))
                        ReDim xanDateValRGISExpAdj(xadDates.GetUpperBound(0))

                        For xnCol = xnDateStartCol To xnDateEndCol - 1

                            Dim xnVal As Decimal = 0
                            Dim xsWrk As String = CellData(xnRow, xnCol)
                            If Not IsNothing(xsWrk) AndAlso xsWrk.Length > 0 Then
                                xnVal = Val(xsWrk)
                            End If

                            'xanDateValExposure(xanDateCol(xnCol)) += xnVal
                            Select Case xasDateCodeCol(xnCol)
                                Case Is = "900" : xanDateValClExpAdj(xanDateCol(xnCol)) += xnVal
                                Case Is = "902" : xanDateValClExpAdj(xanDateCol(xnCol)) += xnVal
                                Case Is = "903" : xanDateValRGISExpAdj(xanDateCol(xnCol)) += xnVal
                            End Select

                        Next

                        For xnWrk = 0 To xnDateCount '- 1

                            If DBConn.State = ConnectionState.Closed Then
                                DBConn.Open()
                            End If

                            xoCmd = New SqlCommand
                            xoCmd.Connection = DBConn
                            xoCmd.CommandType = CommandType.StoredProcedure

                            Try
                                xoCmd.CommandText = "p_StageClothingInsertOrUpdate"
                                xoCmd.Parameters.Add("MetricDate", SqlDbType.DateTime).Value = xadDates(xnWrk)
                                xoCmd.Parameters.Add("StoreNumber", SqlDbType.Int).Value = xnStore
                                ' xoCmd.Parameters.Add("TotalSales", SqlDbType.Decimal).Value = Nothing
                                xoCmd.Parameters.Add("ClientExposureAdj", SqlDbType.Decimal).Value = xanDateValClExpAdj(xnWrk)
                                xoCmd.Parameters.Add("RGISExposureAdj", SqlDbType.Decimal).Value = xanDateValRGISExpAdj(xnWrk)
                                xoCmd.Parameters.Add("Count_Year", SqlDbType.NVarChar).Value = xasCountYear(xnWrk)
                                xoCmd.Parameters.Add("Period_No", SqlDbType.NVarChar).Value = xasPeriodNo(xnWrk)
                                xoCmd.Parameters.Add("RowsUpdated", SqlDbType.Int).Direction = ParameterDirection.Output
                                'xoCmd.Parameters.Add("SqlRtn", SqlDbType.NVarChar).Direction = ParameterDirection.Output
                                xoCmd.ExecuteNonQuery()

                                Dim xsWrk As String = xoCmd.Parameters("RowsUpdated").Value.ToString
                                If xsWrk.Length > 0 AndAlso IsNumeric(xsWrk) Then
                                    Dim xnWrk2 As Integer = CInt(xsWrk)
                                    If xnWrk2 = 1 Then
                                        xnAddCount += 1
                                    Else
                                        xsRtnMsg += "Problem during p_StageClothingInsert/Update. "
                                        xbValid = False
                                    End If
                                End If
                            Catch ex As System.Exception
                                xsRtnMsg += "Problem when updating data for Row " & xnRow & " in sheet " & xsWSName & ". Error " & ex.Message & " "
                            End Try

                        Next


                        xsRtnMsg += "c"

                    End If
                End If
                xnRow += 1
                If CellData(xnRow, 1) <> "SSL" Then
                    Exit Do
                End If
            Loop

            'txtOutput.Text += "Worksheet Done. IP Rows=" & xnIPRowCount & ", Added=" & xnAddCount & "." & vbCrLf
            xnIPRowCount = 0
            xnAddCount = 0


            'txtOutput.Text += "Done. " & vbCrLf
            'Cursor = Cursors.Default
            'MessageBox.Show("Done")

        Catch ex As System.Exception
            MessageBox.Show("Error: " & ex.Message & ": " & xsRtnMsg)
        Finally
            If Not IsNothing(DBConn) AndAlso DBConn.State <> ConnectionState.Closed Then
                DBConn.Close()
            End If

        End Try

    End Sub

    Private Sub ImportClothingSalesFile(sFileToProcess As String)

        '   Import data into Sansbury's Clothing Staging table
        Dim DBConn As SqlConnection
        Dim xnWSCount As Integer = 0
        Dim xsRtnMsg As String = ""

        Dim xadDates() As Date = Nothing
        Dim xanDateCol() As Integer = Nothing
        Dim xasCY() As String = Nothing
        Dim xasPeriod() As String = Nothing
        Dim xanDateValSales() As Decimal = Nothing
        Dim xasDateCodeCol() As String = Nothing
        'Dim xanDateValExposure() As Decimal = Nothing
        Dim xanDateValClExpAdj() As Decimal = Nothing
        Dim xanDateValRGISExpAdj() As Decimal = Nothing

        Try
            Cursor = Cursors.WaitCursor
            'txtOutput.Text = "Output" & vbCrLf


            If Not File.Exists(sFileToProcess) Then
                Cursor = Cursors.Default
                MessageBox.Show("File name not found. Job cancelled.")
                Return
            End If

            Dim xbValid As Boolean = False

            xbValid = True

            DBConn = New SqlConnection(oConn.ConnectionString)
            Dim xoCmd As New SqlCommand

            DBConn.Open()


            '   Open the workbook
            Dim xoExcel As Excel.Application
            Dim xoExWS As New Excel.Worksheet
            Dim xoExWB As Excel.Workbook

            StatusBarText = "Opening Excel file. "

            xoExcel = New Excel.Application

            xoExWB = xoExcel.Workbooks.Open(sFileToProcess)
            xoExcel.Visible = False
            xoExcel.Application.DisplayAlerts = False

            StatusBarText = "Processing the worksheets."

            ' Jeremy Rogers - 15/08/2017
            ' Changed code so that it only reads data from last sheet in the Workbook
            xoExWS = xoExWB.Worksheets(xoExWB.Worksheets.Count)

            '   Work through each worksheet in the workbook
            '   There will be a number of sheets for Sales and a number for Shrink
            '   The different types have different column layouts as the Shrink has 2 columns per day

            Dim xsWSName As String = xoExWS.Name
            Dim xbDatesFound As Boolean = False
            Dim xnDateCount As Integer = 0

            ReDim xadDates(xnDateCount)
            ReDim xanDateCol(xnDateCount)
            ReDim xasDateCodeCol(xnDateCount)
            ReDim xanDateValSales(xnDateCount)
            'ReDim xanDateValExposure(xnDateCount)
            ReDim xanDateValClExpAdj(xnDateCount)
            ReDim xanDateValRGISExpAdj(xnDateCount)

            '   Determine the sheet type
            Dim xnWrk As Integer = 0
            ' txtOutput.Text += xoExWS.Name & " " & vbCrLf
            'StatusBarText = "Processing worksheet " & xoExWS.Name & "."


            '   Get the date range
            Dim xsDate As String = ""
            Dim xnDateStartCol As Integer = 6
            Dim xnDateStartRow As Integer = 1
            Dim xnDateEndCol As Integer = 0
            Dim xnRow As Integer = 1
            Dim xnCol As Integer = 1
            Dim xnColForThisDate As Integer = 0

            ' JR - 18/09/2017
            ' Copy Excel data to a 2 dimensional object array to avoid having to call Excel fuctions
            ' to access each cell item, as this has a time and resource overhead
            Dim CellData As Object(,) = xoExWS.UsedRange.Value
            Dim xnRowCount As Integer = xoExWS.UsedRange.Rows.Count

            ' Close Excel file and related objects as data is now in array CellData
            xoExWB.Close()
            xoExcel.Quit()
            xoExWS = Nothing
            xoExWB = Nothing
            xoExcel = Nothing

            '   Find the "Day" row
            '   The word "Day" appears in the cell preceding the row of dates. This can be in any row from 5-10 and any column from E(5) to K(11)
            xnRow = 5
            Dim xbFound As Boolean = False
            For xnRow = 5 To 10
                For xnCol = 5 To 11
                    If Not IsNothing(CellData(xnRow, xnCol)) AndAlso CellData(xnRow, xnCol).ToString.ToUpper = "DAY" Then
                        xbFound = True
                        Exit For
                    End If
                Next
                If xbFound Then Exit For
            Next

            '   Now extract the dates from the cells. There can be 1-3 columns per date with no rhyme or reason as to why (!)
            '   We need to record what date is relevant to each column so that the figures on each row can be totalled for the table update.
            '   The assumption is that all columns for a given date are to be added together and that they appear together (i.e. are not jumbled up in the column order)
            xnDateStartCol = xnCol + 1
            xnDateStartRow = xnRow
            Dim xdsvDate As Date = Nothing
            Dim xdWrk As Date = Nothing
            xnDateCount = -1

            For xnCol = xnDateStartCol To 30

                If IsNothing(CellData(xnRow, xnCol)) Then
                    Exit For
                End If

                xsDate = CellData(xnRow, xnCol).ToString()
                If xsDate.Length = 0 Or Not IsDate(xsDate) Then
                    Exit For
                End If

                xdWrk = CDate(xsDate)
                If IsNothing(xdsvDate) OrElse xdsvDate <> xdWrk Then

                    xnDateCount += 1
                    ReDim Preserve xadDates(xnDateCount)
                    ReDim Preserve xasCY(xnDateCount)
                    ReDim Preserve xasPeriod(xnDateCount)
                    Dim xdDate As Date = CDate(xsDate)
                    xadDates(xnDateCount) = xdDate
                    'p_GetPeriodAndCYForDate

                    xoCmd = New SqlCommand
                    xoCmd.Connection = DBConn
                    xoCmd.CommandType = CommandType.StoredProcedure

                    Try
                        ' Dim xdReportDate As Date
                        xoCmd.CommandText = "p_GetPeriodAndCYForDate"
                        xoCmd.Parameters.Add("InputDate", SqlDbType.Date).Value = xdDate
                        xoCmd.Parameters.Add("Count_Year", SqlDbType.NVarChar, 10).Direction = ParameterDirection.Output
                        xoCmd.Parameters.Add("Period_No", SqlDbType.NVarChar, 10).Direction = ParameterDirection.Output
                        xoCmd.ExecuteNonQuery()

                        Dim xsCurrentCountYear As String = ""
                        Dim xsWrk As String = xoCmd.Parameters("Count_Year").Value.ToString
                        If xsWrk.Length > 0 Then
                            xsCurrentCountYear = xsWrk
                        End If

                        Dim xsCurrentPeriod As String = ""
                        xsWrk = xoCmd.Parameters("Period_No").Value.ToString
                        If xsWrk.Length > 0 Then
                            xsCurrentPeriod = xsWrk
                        End If

                        xasCY(xnDateCount) = xsCurrentCountYear
                        xasPeriod(xnDateCount) = xsCurrentPeriod

                    Catch ex As System.Exception
                        xsRtnMsg += "Problem when processing header data. Error " & ex.Message & " "
                    End Try


                    xbDatesFound = True
                    xdsvDate = xdWrk

                End If

                ReDim Preserve xanDateCol(xnCol)
                xanDateCol(xnCol) = xnDateCount


            Next

            xnDateEndCol = xnCol

            If Not xbDatesFound Then
                xsRtnMsg += "No dates found on worksheet " & xsWSName
                ' txtOutput.Text += "No dates found in " & xsWSName & vbCrLf
                ' Need to break out of function here ?
            End If

            '   Extract the data
            xnRow = 1
            Do While CellData(xnRow, 1) <> "SSL" And xnRow < xnRowCount
                xnRow += 1
            Loop

            If xnRow >= xnRowCount Then
                'txtOutput.Text += "No data found in " & xsWSName & vbCrLf
            End If

            '   First data row found so process all the following non blank rows
            Do While xnRow < xnRowCount And CellData(xnRow, 1).ToString().Length > 0
                Dim xdDate As Date? = Nothing
                Dim xsStore As String = ""
                Dim xnStore As Integer = 0

                'xnRow += 1
                'xsRtnMsg = "a" & xnRow

                xsStore = CellData(xnRow, 4)
                If Not IsNothing(xsStore) And xsStore.Length > 0 Then
                    xnStore = CInt(xsStore)
                    If xnStore > 0 Then
                        '   Get the figure for each day
                        '   Note that (at this time) there will be an output row for each day, even for zero values
                        '   The s-p Inserts if there's no row there or updates if one does exist
                        '   We work through all columns to accumulate date and then output all dates at one time

                        ReDim xanDateValSales(xadDates.GetUpperBound(0))

                        For xnCol = xnDateStartCol To xnDateEndCol - 1

                            Dim xnVal As Decimal = 0
                            Dim xsWrk As String = CellData(xnRow, xnCol)
                            If Not IsNothing(xsWrk) AndAlso xsWrk.Length > 0 Then
                                xnVal = Val(xsWrk)
                            End If

                            xanDateValSales(xanDateCol(xnCol)) += xnVal


                        Next

                        For xnWrk = 0 To xnDateCount '- 1
                            xoCmd = New SqlCommand
                            xoCmd.Connection = DBConn
                            xoCmd.CommandType = CommandType.StoredProcedure

                            Try
                                xoCmd.CommandText = "p_StageClothingInsertOrUpdate"
                                xoCmd.Parameters.Add("MetricDate", SqlDbType.DateTime).Value = xadDates(xnWrk)
                                xoCmd.Parameters.Add("StoreNumber", SqlDbType.Int).Value = xnStore
                                xoCmd.Parameters.Add("TotalSales", SqlDbType.Decimal).Value = xanDateValSales(xnWrk)
                                xoCmd.Parameters.Add("Count_Year", SqlDbType.NVarChar, 10).Value = xasCY(xnWrk)
                                xoCmd.Parameters.Add("Period_No", SqlDbType.NVarChar, 10).Value = xasPeriod(xnWrk)
                                xoCmd.Parameters.Add("RowsUpdated", SqlDbType.Int).Direction = ParameterDirection.Output
                                xoCmd.ExecuteNonQuery()

                                Dim xsWrk As String = xoCmd.Parameters("RowsUpdated").Value.ToString
                                If xsWrk.Length > 0 AndAlso IsNumeric(xsWrk) Then
                                    Dim xnWrk2 As Integer = CInt(xsWrk)
                                    If xnWrk2 <> 1 Then
                                        xsRtnMsg += "Problem during p_StageClothingInsert/Update. "
                                        xbValid = False
                                    End If
                                End If
                            Catch ex As System.Exception
                                xsRtnMsg += "Problem when updating data for row " & xnRow & " in sheet " & xsWSName & ", store " & xnStore & ", date " & xadDates(xnWrk) & _
                                    ", value " & xanDateValSales(xnWrk) & ". Error " & ex.Message & ". "
                            End Try

                            If xsRtnMsg.Length > 0 Then
                                xsRtnMsg += " Job cancelled."
                                Cursor = Cursors.Default
                                MessageBox.Show(xsRtnMsg)
                                'txtOutput.Text += xsRtnMsg
                                Exit Sub
                            End If

                        Next

                        'xsRtnMsg += "c"

                    End If
                End If
                xnRow += 1
                If CellData(xnRow, 1) <> "SSL" Then
                    Exit Do
                End If
            Loop


            'txtOutput.Text += "Done." & vbCrLf
            'Cursor = Cursors.Default


            'MessageBox.Show("Done")
            If xsRtnMsg.Length > 0 Then
                MessageBox.Show(xsRtnMsg)
            End If

        Catch ex As System.Exception
            Cursor = Cursors.Default
            MessageBox.Show("Error: " & ex.Message & ": " & xsRtnMsg)
        Finally
            If Not IsNothing(DBConn) AndAlso DBConn.State <> ConnectionState.Closed Then
                DBConn.Close()
            End If

        End Try

    End Sub


    Private Sub bgwLoadFiles_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgwLoadFiles.DoWork
        Call GetFiles(e)
    End Sub

    Private Sub GetFiles(e As System.ComponentModel.DoWorkEventArgs)

        Try

            oFilesToProcess.Clear()
            oLvItems.Clear()

            For Each oFilter In oFilters
                oFilesToProcess.AddRange(IO.Directory.GetFiles(sDirectoryPath, oFilter, IO.SearchOption.TopDirectoryOnly))
            Next

            Call ProcessFiles(oFilesToProcess, e)

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub ProcessFiles(oFiles As List(Of String), e As System.ComponentModel.DoWorkEventArgs)

        Try

            Dim iStoreNumberIndex As Integer = -1
            Dim sFileDate As String = ""
            Dim nFiles, nFilesProcessed As Integer

            nFiles = oFiles.Count
            nFilesProcessed = 0

            Cursor = Cursors.WaitCursor

            For Each oFile In oFiles

                If ShouldFileBeProcessed(oFile, iStoreNumberIndex, sFileDate) Then

                    Dim oStoreNumbers As SortedList = GetStoreNumbersInFile(oFile, iStoreNumberIndex, sFileDate)

                    Call AddDataToListview(oStoreNumbers, oFile)

                End If
                nFilesProcessed += 1
                tspb.Value = (nFilesProcessed / nFiles) * 100
            Next

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try
        lblStatus.Text = "Load completed"

        Cursor = Cursors.Default
    End Sub

    Private Function DeleteFiles(sTargetFolder As String, sFileExt As String) As Integer
        Dim myFile As FileInfo
        Dim nFilesDeleted As Integer


        nFilesDeleted = 0
        Dim filePaths = New DirectoryInfo(sTargetFolder).GetFiles("*." & sFileExt)

        For Each myFile In filePaths
            myFile.Delete()
            nFilesDeleted += 1
        Next
        Return nFilesDeleted
    End Function


    Private Function CopyFiles(sSourceFolder As String, sTargetFolder As String, sFileExt As String) As Integer
        Dim dateFrom, dateTo As Date
        Dim nFilesCopied As Integer

        ' JR - 26/09/2017
        ' Set date to to very end of the day to ensure all files with different times on that date are picked up
        dateFrom = New Date(dtpStart.Value.Year, dtpStart.Value.Month, dtpStart.Value.Day, 0, 0, 0)
        dateTo = New Date(dtpEnd.Value.Year, dtpEnd.Value.Month, dtpEnd.Value.Day, 23, 59, 59)

        ' For out15 and out36 files, dates of files will be one day later than inventory date in the file, so increment date by 1
        If sFileExt <> "csv" Then
            ' If file is out15 or out36
            dateFrom = dateFrom.AddDays(1)
            dateTo = dateTo.AddDays(1)
        End If

        ' Copies files of various extensions (.out15,.out36) from central RGIS location to local folders
        ' for later processsing in this applciation
        'Dim txtFiles = Directory.GetFiles(sSourceFolder, sFileExt, SearchOption.TopDirectoryOnly)

        Dim strFileSize As String = ""
        'Dim di As New IO.DirectoryInfo(sSourceFolder).OrderBy(Function(f As FileInfo) f.CreationTime)
        Dim filePaths = New DirectoryInfo(sSourceFolder).GetFiles("*." & sFileExt) _
        'Dim sortedFiles As VariantType = filePaths.ToArray()
        'f.CreationTime.
        Dim orderedFiles = From p In filePaths
                     Order By p.CreationTime
                     Descending

        For Each fi In orderedFiles
            If fi.CreationTime < dateFrom Then
                Exit For
            End If

            If fi.CreationTime >= dateFrom And fi.CreationTime <= dateTo Then
                lblStatus.Text = "Copying " & fi.Name + " to " & fi.FullName
                lblStatus.Invalidate()
                fi.CopyTo(sTargetFolder & "\" & fi.Name, True)
                nFilesCopied += 1
            End If

            'strFileSize = (Math.Round(fi.Length / 1024)).ToString()
            'Console.WriteLine("File Name: {0}", fi.Name)
            'Console.WriteLine("File Full Name: {0}", fi.FullName)
            'Console.WriteLine("File Size (KB): {0}", strFileSize)
            'Console.WriteLine("File Extension: {0}", fi.Extension)
            'Console.WriteLine("Last Accessed: {0}", fi.LastAccessTime)
            'Console.WriteLine("Read Only: {0}", (fi.Attributes.ReadOnly = True).ToString)
        Next
        Return nFilesCopied

    End Function


    Private Function ShouldFileBeProcessed(sFile As String, ByRef iStoreIndex As Integer, ByRef sFileDate As String) As Boolean

        Dim bProcess = False
        'Dim sFileDate As String = ""

        Try

            If (IO.Path.GetFileNameWithoutExtension(sFile).ToUpper.StartsWith("PCVAR")) Then

                'PCVAR - get date from filename (PCVARddMMyyyy.csv)
                Dim sTemp As String = IO.Path.GetFileNameWithoutExtension(sFile).Replace("PCVAR", "")

                'sFileDate = sTemp.Substring(4, 4) & "-" & sTemp.Substring(2, 2) & "-" & sTemp.Substring(0, 2)
                sFileDate = CDate(sTemp.Substring(4, 4) & "-" & sTemp.Substring(2, 2) & "-" & sTemp.Substring(0, 2)).ToString("yyyy-MM-dd")
                bProcess = (sFileDate >= sProcessStartDate) AndAlso (sFileDate <= sProcessEndDate)
                iStoreIndex = ePCVarColumns.eStoreNumber

            Else
                '.Out15 - open file to get date
                Dim oRead As New IO.StreamReader(sFile)
                Dim oLine As String = oRead.ReadLine 'first line is always headers

                sFileDate = CDate(oLine.Split(",")(eOut16Columns.eInventoryDate).Replace("/", "-")).ToString("yyyy-MM-dd")
                bProcess = (sFileDate >= sProcessStartDate) AndAlso (sFileDate <= sProcessEndDate)
                iStoreIndex = eOut16Columns.eStoreNumber
                oRead.Close()

            End If

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        Finally
            ShouldFileBeProcessed = bProcess
        End Try

    End Function

    Private Sub AddDataToListview(oList As SortedList, oFile As String)

        Dim sFilenameWithoutPath As String

        sFilenameWithoutPath = Path.GetFileName(oFile)

        Try

            '0inventorydate
            '1storenumber
            '2storetype
            '3[blank] - exception field
            '4destination table - out15 / ta / N/A (not imported)
            '5Dupe detected

            For Each oDic As DictionaryEntry In oList

                Dim oTag As udtStoreProps = oDic.Value
                Dim sAdd(7) As String
                Dim sImpDate As String = ""

                sAdd(0) = oTag.sInvDate
                sAdd(1) = oTag.iStoreNumber
                sAdd(2) = oTag.sStoreType
                sAdd(3) = ""
                sAdd(4) = sFilenameWithoutPath
                sAdd(5) = GetDestinationTable(oTag.sInvDate, IO.Path.GetExtension(oTag.sFullpath))
                sAdd(6) = If(Not DoesStoreNumberExistInDatabase(oTag.iStoreNumber, sAdd(5), oTag.sInvDate, sImpDate), "", "DUPE")
                sAdd(7) = sImpDate

                Dim lvItem As New ListViewItem(sAdd)
                lvItem.Tag = oTag

                If lvItem.SubItems(6).Text.Length > 0 Then lvItem.BackColor = Color.PaleVioletRed

                'exception already exist in db ?
                If (htExceptions.Contains(oTag.iStoreNumber & ":" & oTag.sInvDate)) Then
                    Call SetStatus(CType(htExceptions(oTag.iStoreNumber & ":" & oTag.sInvDate), udtExceptionProps).sExclusionType, lvItem)
                End If

                oLvItems.Add(lvItem)
                'lvData.Items.Add(lvItem)

            Next

            'lvData.Items.AddRange(oLvItems.ToArray)

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Function GetDestinationTable(sDate As String, sExt As String) As String

        Dim sTable As String = "Err"

        Try
            If sExt.Contains("csv") Then
                If (sDate >= sStartTA) AndAlso (sDate <= sEndTA) Then
                    sTable = eDestinationTables.Inventory_Data_TA_OUT15.ToString
                Else
                    sTable = eDestinationTables.Inventory_Data_OUT15.ToString
                End If
            Else
                If (sDate >= sStartTA) AndAlso (sDate <= sEndTA) Then
                    sTable = eDestinationTables.NOSF_Inventory_Data_TA_OUT15.ToString
                Else
                    sTable = eDestinationTables.NOSF_Inventory_Data_OUT15.ToString
                End If
            End If

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        Finally
            GetDestinationTable = sTable
        End Try

    End Function

    Private Function DoesStoreNumberExistInDatabase(iStoreNumber As Integer, sDestinationTable As String, sInvDate As String, ByRef sImpDate As String) As Boolean

        Dim bExists As Boolean = False

        Try
            If iStoreNumber = vbNull Then
                MessageBox.Show("No store number")
                Exit Try

            End If

            If iStoreNumber = 4275 Then
                iStoreNumber = iStoreNumber
            End If


            If sDestinationTable.Equals("[ignore]") Then Exit Try

            Dim sCurrentCY As String = ReadField("SELECT Count_Year FROM Financial_Calender WHERE [Inventory Date] ='" & sInvDate & "'").ToString.Trim
            Dim htTable As New Hashtable
            Dim sSQL As String = ""

            sSQL = "SELECT distinct(Count_Year), Financial_Calender.[Inventory Date] FROM " & sDestinationTable & " INNER JOIN Financial_Calender ON " & sDestinationTable & ".Inventory_Date = Financial_Calender.[Inventory Date] AND [Inventory Date] ='" & sInvDate & "' WHERE " & sDestinationTable & ".Store_Number='" & iStoreNumber & "'"
            Dim dTemp As DataTable = SqlQuery(oConn.ConnectionString, sSQL)

            If dTemp IsNot Nothing AndAlso dTemp.Rows.Count > 0 Then
                For Each dRow As DataRow In dTemp.Rows
                    htTable.Add(dRow(0).ToString.Trim, dRow(1).ToString.Trim)
                Next
            End If

            bExists = htTable.ContainsKey(sCurrentCY)

            If bExists Then
                sImpDate = CDate(htTable(sCurrentCY)).ToString("yyyy-MM-dd")
            End If

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        Finally
            DoesStoreNumberExistInDatabase = bExists
        End Try

    End Function

    Private Function GetStoreNumbersInFile(sFile As String, iStoreIndex As Integer, sFileDate As String) As SortedList

        Dim oStoreNumbers As New SortedList
        Dim oLine As String = ""
        Dim oRead As New IO.StreamReader(sFile)
        Try

            'read file, get store num
            Dim sStoreNumber As String = ""
            Dim iIndex As Integer = 0

            oLine = oRead.ReadLine 'first line is always headers

            Do Until oLine Is Nothing

                If iStoreIndex = ePCVarColumns.eStoreNumber OrElse iIndex > 0 Then
                    oLine = oRead.ReadLine
                End If

                ' JR - 10/03/2017
                ' Check if No Data Be Reported message is in file, and if so then don't attempt to read any further
                If InStr(oLine, "*****No Data to be Reported*****") >= 1 Then Exit Do

                If oLine Is Nothing Then Exit Do

                sStoreNumber = oLine.Split(",")(iStoreIndex)

                'if not exists in ostorenumbers, add

                If sStoreNumber.Length = 0 Then Exit Do

                If (Not oStoreNumbers.Contains(sStoreNumber)) Then
                    Dim oTag As New udtStoreProps
                    oTag.iStoreNumber = sStoreNumber
                    oTag.sFilename = IO.Path.GetFileName(sFile)
                    oTag.sFullpath = sFile
                    oTag.sInvDate = sFileDate

                    ' JR - 19/07/2017
                    ' Check if store number is in array populated from TB_Retail_Hierarchy first
                    ' and if not then warn user
                    Dim tbrhRow() As DataRow = dtStoreTypes.Select("Store_Number=" & oTag.iStoreNumber)
                    If tbrhRow.Count = 0 Then
                        MsgBox("Store number " & oTag.iStoreNumber & " not found in TB_Retail_Hierarchy table. Store type cannot be obtained.", MsgBoxStyle.Exclamation)

                    Else
                        oTag.sStoreType = dtStoreTypes.Select("Store_Number=" & oTag.iStoreNumber)(0)("Store_Type")
                    End If
                    oStoreNumbers.Add(sStoreNumber, oTag)
                End If
                iIndex = 1
            Loop

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        Finally
            oRead.Close()
            GetStoreNumbersInFile = oStoreNumbers
        End Try

    End Function

    Private Sub bgwLoadFiles_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwLoadFiles.RunWorkerCompleted

        Dim storelist As New List(Of Integer)


        Try

            lblStatus.Text = "Finished loading data."
            tspb.Visible = False

            gbDates.Enabled = True
            gbData.Enabled = True
            FileToolStripMenuItem.Enabled = True

            lvData.Items.Clear()
            lvData.Items.AddRange(oLvItems.ToArray)

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        Finally
            If lvData.Items.Count > 0 Then
                lvData.Items(0).Selected = True
                btnDeDupe.Enabled = DuplicatesExist()
            End If
            Windows.Forms.Cursor.Current = Cursors.Default
        End Try
        ' JR - 27/09/2017
        ' Get unique list of store numbers
        For Each l As ListViewItem In lvData.Items

            If Not storelist.Contains(CInt(l.SubItems(eListviewColumns.eStoreNo).Text)) Then
                storelist.Add(CInt(l.SubItems(eListviewColumns.eStoreNo).Text))
            End If

        Next

        lblStatus.Text = storelist.Count.ToString() & " stores retrieved"


    End Sub

    Private Sub TestToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs)
        MsgBox(lvData.Columns(0).Width)
    End Sub

    Private Sub btnExit_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub btnRun_Click(sender As System.Object, e As System.EventArgs) Handles btnRun.Click
        Call RunProcess()
    End Sub

    Private Sub RunProcess()

        Try

            'If "DUPE" exists then we shall not continue! - Shane Cawser 2013-10-14
            If Not DuplicatesExist() Then

                ' JR - 21/09/2017
                ' Call routine to perform file copying tasks and Clothing Import, all of which 
                ' were previously performed in separate applicatons run before the Sainsbury's Dashboard
                ' data load


                tspb.Visible = True
                lblStatus.Text = "Testing Data Integrity..."

                gbDates.Enabled = False
                gbData.Enabled = False
                FileToolStripMenuItem.Enabled = False
                btnRun.Enabled = False

                dtTableOut15 = CreateDataTable()
                dtTableTaData = CreateDataTable()
                dtTableNOSF = CreateDataTable()
                dtTableNOSFTa = CreateDataTable()

                Windows.Forms.Cursor.Current = Cursors.WaitCursor

                Call SetupErrorLogs()

                bgwRun.RunWorkerAsync()

            Else

                MsgBox("Please resolve duplicates before proceeding with data import.", MsgBoxStyle.Critical)

            End If

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub bgwRun_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgwRun.DoWork
        Call InsertData(e)
    End Sub

    Private Function DuplicatesExist() As Boolean

        Dim oDupes As IEnumerable(Of ListViewItem) = Nothing

        Try
            oDupes = From lvItem In lvData.Items.Cast(Of ListViewItem)() Where lvItem.SubItems(eListviewColumns.eDupe).Text = "DUPE" And lvItem.SubItems(eListviewColumns.eException).Text <> "TRIAL" And lvItem.SubItems(eListviewColumns.eException).Text <> "ADD"

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        Finally
            DuplicatesExist = (oDupes IsNot Nothing) AndAlso (oDupes.Count > 0)
        End Try

    End Function

    Private Sub InsertData(e As System.ComponentModel.DoWorkEventArgs)
        Dim nItemsToProcess As Integer, nItemsProcessed As Integer
        Dim nRgisCount, nOrridgeCount As Integer
        Dim sMessage, sCaption As String
        Dim log_file As StreamWriter
        Dim logFilename, sBody, sFinishTime As String
        Dim sEmailTo, sEmailCc


        Try
            Dim slTemp As New SortedList
            lblStatus.Text = "Import preparation progress"
            tspb.Value = 0
            Cursor = Cursors.WaitCursor
            nItemsToProcess = oLvItems.Count
            For Each lvItem In oLvItems

                If Not slTemp.Contains(CType(lvItem.Tag, udtStoreProps).sFilename) Then
                    cLog.p_Message = "Files ¬ " & CType(lvItem.Tag, udtStoreProps).sFilename
                    slTemp.Add(CType(lvItem.Tag, udtStoreProps).sFilename, CType(lvItem.Tag, udtStoreProps))
                End If

                Select Case lvItem.SubItems(eListviewColumns.eException).Text

                    Case ""
                        'No exception, import as normal
                        Call ImportFileToDataTable(lvItem.SubItems(eListviewColumns.eTable).Text, lvItem.Tag, lvItem.SubItems(eListviewColumns.eException).Text)
                        cLog.p_Message = "ImportedFiles ¬ " & CType(lvItem.Tag, udtStoreProps).iStoreNumber & " ¬ " & CType(lvItem.Tag, udtStoreProps).sFilename

                    Case "TRIAL"
                        ' Trial data , import as normal
                        Call ImportFileToDataTable(lvItem.SubItems(eListviewColumns.eTable).Text, lvItem.Tag, lvItem.SubItems(eListviewColumns.eException).Text)
                        cLog.p_Message = "ImportedFiles ¬ " & CType(lvItem.Tag, udtStoreProps).iStoreNumber & " ¬ " & CType(lvItem.Tag, udtStoreProps).sFilename

                    Case "ADD"
                        ' Additional count data , import as normal
                        Call ImportFileToDataTable(lvItem.SubItems(eListviewColumns.eTable).Text, lvItem.Tag, lvItem.SubItems(eListviewColumns.eException).Text)
                        cLog.p_Message = "ImportedFiles ¬ " & CType(lvItem.Tag, udtStoreProps).iStoreNumber & " ¬ " & CType(lvItem.Tag, udtStoreProps).sFilename

                    Case "EM"
                        'Emergency Count, do not import - Log
                        cLog.p_Message = "NotImported ¬ " & CType(lvItem.Tag, udtStoreProps).iStoreNumber & " ¬ " & CType(lvItem.Tag, udtStoreProps).sFilename & " ¬ Emergency Count"
                        Call UpdateExceptionInDatabase(CType(lvItem.Tag, udtStoreProps), "EM")

                    Case "TA"
                        'TA Count outside or TA date range - TA Table
                        Call ImportFileToDataTable(lvItem.SubItems(eListviewColumns.eTable).Text, lvItem.Tag, lvItem.SubItems(eListviewColumns.eException).Text)
                        Call UpdateExceptionInDatabase(CType(lvItem.Tag, udtStoreProps), "TA")
                        cLog.p_Message = "ImportedFiles ¬ " & CType(lvItem.Tag, udtStoreProps).iStoreNumber & " ¬ " & CType(lvItem.Tag, udtStoreProps).sFilename & " ¬ Enforced - TA Exception"

                    Case "C1/FU"
                        'C1/Full Count inside TA date range - Out15 Table
                        Call ImportFileToDataTable(lvItem.SubItems(eListviewColumns.eTable).Text, lvItem.Tag, lvItem.SubItems(eListviewColumns.eException).Text)
                        Call UpdateExceptionInDatabase(CType(lvItem.Tag, udtStoreProps), "C1/FU")
                        cLog.p_Message = "ImportedFiles ¬ " & CType(lvItem.Tag, udtStoreProps).iStoreNumber & " ¬ " & CType(lvItem.Tag, udtStoreProps).sFilename & " ¬ Enforced - C1/FU Exception"

                    Case "C2"
                        'C2 count, do not import - Log
                        cLog.p_Message = "NotImported ¬ " & CType(lvItem.Tag, udtStoreProps).iStoreNumber & " ¬ " & CType(lvItem.Tag, udtStoreProps).sFilename & " ¬ C2 Count"
                        Call UpdateExceptionInDatabase(CType(lvItem.Tag, udtStoreProps), "C2")

                End Select

                If bErrors Then
                    MsgBox("Errors have been found in file: " & CType(lvItem.Tag, udtStoreProps).sFilename & ". Check Log File!", MsgBoxStyle.Critical)
                    Exit Try
                End If
                nItemsProcessed += 1
                tspb.Value = (nItemsProcessed / nItemsToProcess) * 100
            Next

            If (My.Application.CommandLineArgs.Count = 1) AndAlso (My.Application.CommandLineArgs(0).ToString = "Debug=1") Then Call DumpDataTable()

            Call BulkImportToSQL()
            Call StampProcessDates()

            '2014-08-12 SC
            '#2015# Call ImportClothingData()
            '#2015# Call ImportSuperCatVerifier()

            '2014-09-04 SC
            '#2015# Call ImportPIQA()
            Cursor = Cursors.Default
            If MsgBox("Import of Data Complete!" & vbNewLine & vbNewLine & _
                      "Do you want to run Stored Procedures now ?", MsgBoxStyle.Information + MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                logFilename = "Dashboard_Load_" + Date.Now.ToString("ddMMMyyyy") + ".txt"

                log_file = New StreamWriter(logFilename, False)
                sFinishTime = DateTime.Now.ToShortTimeString()
                log_file.WriteLine("Started Stored Procedure run for Dashboard Load: {0} {1}", sFinishTime, _
                                DateTime.Now.ToShortDateString())

                If chkSkipQlikViewStagingRefresh.Checked Then
                    Call RunStoredProcedure("SAINSBURY_DASHBOARD_VIEWDATA 0")
                Else
                    ' Default is to run QlikView Staging table refresh immediately after the import
                    Call RunStoredProcedure("SAINSBURY_DASHBOARD_VIEWDATA 1")
                End If

                ' JR - 23/08/2017
                ' Added message box to display the number of RGIS stores and Orridge stores that were uploaded
                Call GetInventoryCompanyStoreCounts(nRgisCount, nOrridgeCount)
                sCaption = "Sainsburys Dashboard Load - " + DateTime.Now.Date.ToString("dd/MM/yyyy")
                sMessage = "A total of " + nRgisCount.ToString() + " RGIS stores, "
                sMessage += "and " + nOrridgeCount.ToString() + " Orridge stores have been uploaded."
                log_file.WriteLine(sMessage)
                log_file.WriteLine("Finished Stored Procedure run for Dashboard Load: {0} {1}", DateTime.Now.ToShortTimeString(), _
                DateTime.Now.ToShortDateString())
                log_file.Close()

                Call MessageBox.Show(sMessage, sCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
                If Not chkSkipEmailNotification.Checked Then
                    Dim email = New clsEmail()

                    ' JR - 07/09/2017
                    ' Added Email message to send on count of RGIS stores and Orridge stores that were uploaded
                    sBody = "Please note the Sainsburys Dashboard data load completed at " + sFinishTime + " on " + DateTime.Now.Date.ToString("dd/MM/yyyy")
                    sBody += vbCrLf + vbCrLf + sMessage
                    'Call frmEmailForm.SendEmail("Sainsburys Dashboard Load - " + Date.Today.ToShortDateString(), sBody, "", "")
                    sEmailTo = System.Configuration.ConfigurationManager.AppSettings.Get("EmailTo")
                    sEmailCc = System.Configuration.ConfigurationManager.AppSettings.Get("EmailCc")
                    email.SendEmail(sEmailTo, sEmailCc, "Sainsburys Dashboard Load - " + Date.Today.ToShortDateString(), sBody, "", "")
                End If
                Call RunInvalidSubCatReport()
                Call RunLoadExceptionsReport()
            End If
        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        Finally
            cLog.WriteToXml()
        End Try

    End Sub



    Private Sub ImportPIQA()
        Call PiqaImport()
    End Sub

    Private Sub RunLoadExceptionsReport()

        Try

            Dim oFrm As New frmLoadExceptions
            oFrm.StartDate = sProcessStartDate
            oFrm.EndDate = sProcessEndDate

            oFrm.ShowDialog()

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub RunInvalidSubCatReport()

        Try
            tspb.Value = 0
            lblStatus.Text = "Running invalid sub cat report"
            Cursor = Cursors.WaitCursor
            sProcessStartDate = If(sProcessStartDate.Length = 0, "", sProcessStartDate)
            sProcessEndDate = If(sProcessEndDate.Length = 0, "", sProcessEndDate)

            Dim sSQL As String = "exec SP_Check_for_Invalid_Subcats " & If(sProcessStartDate.Length = 0 AndAlso sProcessEndDate.Length = 0, "", "'" & sProcessStartDate & "','" & sProcessEndDate & "'")
            Dim dTable As DataTable = SqlQuery(oConn.ConnectionString, sSQL)
            dTable.TableName = "TB_Load_Exceptions"
            Cursor = Cursors.Default
            If dTable IsNot Nothing AndAlso dTable.Rows.Count > 0 Then
                dTable.WriteXml(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) & "\Invalid_SubCats_" & Now.ToString("yyyy_MM_dd_HH_mm_ss") & ".xml")
                MsgBox("One or more invalid sub cats has been detected. File exported to desktop", MsgBoxStyle.Information)
            Else
                MsgBox("No invalid sub cats detected.", MsgBoxStyle.Information)
            End If
            lblStatus.Text = ""
        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub
    Private Sub GetInventoryCompanyStoreCounts(ByRef rgisCount As Integer, ByRef orridgeCount As Integer)

        Dim xoCmd As SqlCommand

        xoCmd = New SqlCommand()

        xoCmd.Connection = oConn.Connection
        xoCmd.CommandType = System.Data.CommandType.StoredProcedure

        xoCmd.Connection.Open()
        xoCmd.CommandText = "dbo.p_GetStoreUploadCounts"
        xoCmd.Parameters.Add("Date_Created", SqlDbType.Date).Value = DateTime.Now.Date
        xoCmd.Parameters.Add("Rgis_Store_Count", SqlDbType.Int, 10).Direction = ParameterDirection.Output
        xoCmd.Parameters.Add("Orridge_Store_Count", SqlDbType.Int, 10).Direction = ParameterDirection.Output
        xoCmd.ExecuteNonQuery()
        xoCmd.Connection.Close()
        orridgeCount = xoCmd.Parameters("Orridge_Store_Count").Value
        rgisCount = xoCmd.Parameters("Rgis_Store_Count").Value

    End Sub

    Private Sub RunStoredProcedure(sSP As String)

        Dim sQuery As String = "exec " & sSP

        Try
            Cursor = Cursors.WaitCursor
            tspb.Value = 0
            bgwRun.ReportProgress(0, "Running Stored Procedures...")
            Call executeQuery(oConn.ConnectionString, sSP)
            bgwRun.ReportProgress(0, "Stored Procedures Finished!")
            Cursor = Cursors.Default

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub StampProcessDates()

        Try

            Dim sSQL As String = "DELETE FROM Process_Dates"
            Call executeQuery(oConn.ConnectionString, sSQL)

            Dim oInsert As New clsInsert
            With oInsert
                .Reset()
                .TableName("Process_Dates")
                .BuildInsert("Process_Start_Date", sProcessStartDate)
                .BuildInsert("Process_End_Date", sProcessEndDate)
                Call executeQuery(oConn.ConnectionString, .GetSql)
            End With

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub UpdateExceptionInDatabase(oStoreProps As udtStoreProps, sException As String)

        Try

            Dim sSQL As String = "DELETE FROM Inventory_Data_OUT15_Exclusions WHERE Inventory_Date='" & CDate(oStoreProps.sInvDate).ToString("yyyy-MM-dd") & "' AND Store_Number='" & oStoreProps.iStoreNumber & "'"
            Call executeQuery(oConn.ConnectionString, sSQL)

            Dim oInsert As New clsInsert
            With oInsert
                .Reset()
                .TableName("Inventory_Data_OUT15_Exclusions")
                .BuildInsert("Store_Number", oStoreProps.iStoreNumber)
                .BuildInsert("Inventory_Date", CDate(oStoreProps.sInvDate).ToString("yyyy-MM-dd"))
                .BuildInsert("ExclusionType", sException)
                Call executeQuery(oConn.ConnectionString, .GetSql)
            End With

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub BulkImportToSQL()

        Try

            If (dtTableOut15.Rows.Count > 0) Then
                bgwRun.ReportProgress(0, "Importing Out15 Data...")
                Call ImportDataTableToDatabase(dtTableOut15, "Inventory_Data_Out15")
            End If

            If (dtTableTaData.Rows.Count > 0) Then
                bgwRun.ReportProgress(0, "Importing TA Data...")
                Call ImportDataTableToDatabase(dtTableTaData, "Inventory_Data_TA_Out15")
            End If

            If (dtTableNOSF.Rows.Count > 0) Then
                bgwRun.ReportProgress(0, "Importing NOSF Data...")
                Call ImportDataTableToDatabase(dtTableNOSF, "NOSF_Inventory_Data_OUT15")
            End If

            If (dtTableNOSFTa.Rows.Count > 0) Then
                bgwRun.ReportProgress(0, "Importing NOSF TA Data...")
                Call ImportDataTableToDatabase(dtTableNOSFTa, "NOSF_Inventory_Data_TA_OUT15")
            End If


        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub ImportDataTableToDatabase(dt As DataTable, sDestinationTable As String)

        Try

            Using cn As New SqlConnection(oConn.ConnectionString)
                cn.Open()
                Using copy As New SqlBulkCopy(cn)

                    For i = 0 To dt.Columns.Count - 1
                        copy.ColumnMappings.Add(dt.Columns(i).ColumnName, dt.Columns(i).ColumnName)
                    Next

                    copy.BulkCopyTimeout = 0
                    copy.DestinationTableName = sDestinationTable
                    copy.WriteToServer(dt)

                End Using
            End Using


        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub DumpDataTable()

        Try

            If dtTableOut15.Rows.Count > 0 Then dtTableOut15.WriteXml(Forms.Application.StartupPath & "\DataInserted_Out15_" & Now.ToString("yyyy_MM_dd hh_mm_ss") & ".xml")
            If dtTableTaData.Rows.Count > 0 Then dtTableTaData.WriteXml(Forms.Application.StartupPath & "\DataInserted_TA_Out15_" & Now.ToString("yyyy_MM_dd hh_mm_ss") & ".xml")
            If dtTableNOSF.Rows.Count > 0 Then dtTableNOSF.WriteXml(Forms.Application.StartupPath & "\DataInserted_NOSF_Out15_" & Now.ToString("yyyy_MM_dd hh_mm_ss") & ".xml")
            If dtTableNOSFTa.Rows.Count > 0 Then dtTableNOSFTa.WriteXml(Forms.Application.StartupPath & "\DataInserted_NOSF_TA_Out15_" & Now.ToString("yyyy_MM_dd hh_mm_ss") & ".xml")

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub ImportFileToDataTable(sTableName As String, oTag As udtStoreProps, sException As String)

        Dim oRead As New IO.StreamReader(oTag.sFullpath, True)

        Try

            Dim dTable As DataTable = GetDataTableForImport(sTableName)
            Dim oLine As String = ""
            Dim iMaxColCount As Integer = -1
            Dim iType As eType = -1
            Dim bOK As Boolean = False
            Dim nTrial As Integer
            Dim nAdditionalCount As Integer

            nTrial = vbNull

            If (IO.Path.GetFileNameWithoutExtension(oTag.sFullpath).ToUpper.StartsWith("PCVAR")) Then
                'Get first line - to be ignored
                oLine = oRead.ReadLine
                'set max columns - if item desc contains "," it screws up array
                iMaxColCount = 11
                iType = eType.ePCVAR
            Else
                'set max columns for .out15 - if item desc contains "," it screws up array
                iMaxColCount = 12
                iType = eType.eOut16
            End If

            While Not oRead.EndOfStream

                Dim sData() As String

                oLine = oRead.ReadLine
                sData = oLine.Split(",")

                Try

                    If sData.GetUpperBound(0) <> iMaxColCount Then
                        'incorrect array - productdesc contains a "," - fix it!
                        sData = ExcelCommaFix(sData, iMaxColCount, iType)
                    End If

                    If (iType = eType.ePCVAR) Then
                        bOK = (oTag.iStoreNumber = sData(ePCVarColumns.eStoreNumber))
                    ElseIf iType = eType.eOut16 Then
                        bOK = (oTag.iStoreNumber = sData(eOut16Columns.eStoreNumber))
                    End If

                    nTrial = Nothing
                    nAdditionalCount = Nothing

                    If bOK Then
                        'correct array now, try importing
                        ' JR - 05/07/2017
                        ' Added flag to indicate if data is trial data
                        ' JR - 01/08/2017
                        ' Added flag to indicate if data is additional count data
                        If sException = "TRIAL" Then
                            nTrial = 1
                        ElseIf sException = "ADD" Then
                            nAdditionalCount = 1
                        End If
                        If iType = eType.ePCVAR Then
                            dTable.Rows.Add(CDate(oTag.sInvDate).ToString("yyyy-MM-dd"), sData(ePCVarColumns.eStoreNumber), 1, 1, 1, sData(ePCVarColumns.eSKU), sData(ePCVarColumns.eItemDesc), sData(ePCVarColumns.eRetailPrice), If(sData(ePCVarColumns.eSuperCat).Length = 0, DBNull.Value, sData(ePCVarColumns.eSuperCat)), If(sData(ePCVarColumns.eSubCat).Length = 0, DBNull.Value, sData(ePCVarColumns.eSubCat)), sData(ePCVarColumns.eOnHand), sData(ePCVarColumns.eActualQty), 0, False, nTrial, nAdditionalCount)
                        ElseIf iType = eType.eOut16 Then
                            dTable.Rows.Add(CDate(oTag.sInvDate).ToString("yyyy-MM-dd"), sData(eOut16Columns.eStoreNumber), 1, 1, 1, sData(eOut16Columns.eSKU), sData(eOut16Columns.eItemDesc), sData(eOut16Columns.eRetailPrice), If(sData(eOut16Columns.eSuperCat).Length = 0, DBNull.Value, sData(eOut16Columns.eSuperCat)), If(sData(eOut16Columns.eSubCat).Length = 0, DBNull.Value, sData(eOut16Columns.eSubCat)), sData(eOut16Columns.eOnHand), sData(eOut16Columns.eActualQty), sData(eOut16Columns.eBackroom), False, nTrial, nAdditionalCount)
                        End If

                    End If

                Catch ex As System.Exception
                    cLog.p_Message = "Errors ¬ " & oTag.sFilename & " ¬ Err Msg: " & ex.Message & " Line (" & oLine & ")"
                    bErrors = True
                End Try

            End While

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        Finally
            Try
                oRead.Close()
                oRead.Dispose()
            Catch ex As System.Exception
                MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
            End Try

        End Try

    End Sub

    Private Function GetDataTableForImport(sTable As String) As DataTable

        Dim dTable As DataTable = dtTableOut15

        Try

            Select Case sTable
                Case eDestinationTables.Inventory_Data_OUT15.ToString
                    dTable = dtTableOut15
                Case eDestinationTables.Inventory_Data_TA_OUT15.ToString
                    dTable = dtTableTaData
                Case eDestinationTables.NOSF_Inventory_Data_OUT15.ToString
                    dTable = dtTableNOSF
                Case eDestinationTables.NOSF_Inventory_Data_TA_OUT15.ToString
                    dTable = dtTableNOSFTa
            End Select

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        Finally
            GetDataTableForImport = dTable
        End Try

    End Function

    Private Sub bgwRun_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bgwRun.ProgressChanged

        Try

            lblStatus.Text = e.UserState.ToString

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub bgwRun_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwRun.RunWorkerCompleted

        Try

            tspb.Visible = False
            lblStatus.Text = ""

            gbDates.Enabled = True
            gbData.Enabled = True
            FileToolStripMenuItem.Enabled = True
            btnRun.Enabled = True

            For Each lvItem As ListViewItem In lvData.Items
                Call ChangeListviewItemText(lvItem, lvItem.SubItems(eListviewColumns.eException).Text)
            Next

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        Finally
            Windows.Forms.Cursor.Current = Cursors.Default
            MsgBox("Completed!", MsgBoxStyle.Information)
        End Try

    End Sub

    Private Sub mnuExceptions_Opening(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles mnuExceptions.Opening

        Try

            e.Cancel = lvData.SelectedItems.Count = 0
            RemoveDupeDataToolStripMenuItem.Enabled = lvData.SelectedItems(0).SubItems(eListviewColumns.eDupe).Text.Length > 0

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub ClearExceptionToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ClearExceptionToolStripMenuItem.Click
        Call SetStatus("")
    End Sub

    Private Sub EmergencyCountToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles EmergencyCountToolStripMenuItem.Click
        Call SetStatus("EM")
    End Sub

    Private Sub AdditionalCountToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AdditionalCountToolStripMenuItem.Click
        Call SetStatus("ADD")
    End Sub


    Private Sub SetStatus(sStatus As String, Optional ByRef oItem As ListViewItem = Nothing)

        Try
            Dim sNewTable As String = ""

            If oItem IsNot Nothing Then
                oItem = ChangeListviewItemText(oItem, sStatus)
            Else
                For Each lvItem As ListViewItem In lvData.SelectedItems
                    lvItem = ChangeListviewItemText(lvItem, sStatus)
                Next
            End If

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Function ChangeListviewItemText(ByRef lvItem As ListViewItem, ByVal sStatus As String) As ListViewItem

        Dim oItem As ListViewItem = lvItem

        Try
            Dim sNewTable As String = ""
            Dim sImpDate As String = ""

            Select Case sStatus
                Case ""
                    sNewTable = GetDestinationTable(oItem.SubItems(0).Text, IO.Path.GetExtension(CType(oItem.Tag, udtStoreProps).sFullpath))
                Case "EM"
                    sNewTable = "[ignore]"
                Case "C1/FU"
                    sNewTable = If(CType(oItem.Tag, udtStoreProps).sFullpath.EndsWith(".csv"), "Inventory_Data_OUT15", "NOSF_Inventory_Data_OUT15")
                Case "C2"
                    sNewTable = "[ignore]"
                Case "TA"
                    sNewTable = If(CType(oItem.Tag, udtStoreProps).sFullpath.EndsWith(".csv"), "Inventory_Data_TA_OUT15", "NOSF_Inventory_Data_TA_OUT15")
                Case "TRIAL"
                    sNewTable = If(CType(oItem.Tag, udtStoreProps).sFullpath.EndsWith(".csv"), "Inventory_Data_OUT15", "NOSF_Inventory_Data_OUT15")
                Case "ADD"
                    sNewTable = If(CType(oItem.Tag, udtStoreProps).sFullpath.EndsWith(".csv"), "Inventory_Data_OUT15", "NOSF_Inventory_Data_OUT15")
            End Select

            oItem.SubItems(eListviewColumns.eException).Text = sStatus
            oItem.SubItems(eListviewColumns.eTable).Text = sNewTable
            oItem.SubItems(eListviewColumns.eDupe).Text = If(Not DoesStoreNumberExistInDatabase(oItem.SubItems(eListviewColumns.eStoreNo).Text, sNewTable, oItem.SubItems(eListviewColumns.eInvDat).Text, sImpDate), "", "DUPE")
            oItem.SubItems(eListviewColumns.eImpDate).Text = sImpDate

            If oItem.SubItems(eListviewColumns.eDupe).Text.Length > 0 Then
                lvItem.BackColor = Color.PaleVioletRed
            Else
                lvItem.BackColor = Color.White
            End If


        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        Finally
            ChangeListviewItemText = oItem
        End Try

    End Function

    Private Sub C1FullCountToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles C1FullCountToolStripMenuItem.Click
        Call SetStatus("C1/FU")
    End Sub

    Private Sub C2CountToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles C2CountToolStripMenuItem.Click
        Call SetStatus("C2")
    End Sub

    Private Sub TACountToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles TACountToolStripMenuItem.Click
        Call SetStatus("TA")
    End Sub
    Private Sub TrialToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles TrialToolStripMenuItem.Click
        Call SetStatus("TRIAL")
    End Sub

    Private Sub lvData_ColumnClick(sender As Object, e As System.Windows.Forms.ColumnClickEventArgs) Handles lvData.ColumnClick
        Call SortListview(lvData, e.Column)
    End Sub

    Private Sub lvData_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles lvData.SelectedIndexChanged
        btnRun.Enabled = lvData.Items.Count > 0
    End Sub

    Private Sub SortListview(ByVal lv As ListView, ByVal iColIndex As Integer)
        Try
            Dim col As ColumnHeader = lv.Columns(iColIndex)
            Dim sort_order As System.Windows.Forms.SortOrder
            Dim iType As Integer = 0

            sort_order = lv.Tag
            Select Case sort_order
                Case Windows.Forms.SortOrder.Ascending
                    sort_order = Windows.Forms.SortOrder.Descending
                Case Windows.Forms.SortOrder.Descending
                    sort_order = Windows.Forms.SortOrder.Ascending
                Case Windows.Forms.SortOrder.None
                    sort_order = Windows.Forms.SortOrder.Ascending
            End Select

            lv.Tag = sort_order

            'Get the type
            iType = lv.Columns(iColIndex).Tag

            'Set the new sort options
            lv.ListViewItemSorter = New ListViewColumnSorter(iColIndex, sort_order, iType)

            ' Perform the sort with these new sort options.
            lv.Sort()

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub InvalidSubCatCheckToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles InvalidSubCatCheckToolStripMenuItem.Click
        Call RunInvalidSubCatReport()
    End Sub

    Private Sub FileToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles FileToolStripMenuItem.Click
        InvalidSubCatCheckToolStripMenuItem.Enabled = txtStartDate.Text.Length > 0 AndAlso txtEndDate.Text.Length > 0
    End Sub

    Private Sub RemoveDupeDataToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RemoveDupeDataToolStripMenuItem.Click
        Call RemoveDupeFromDatabase()
    End Sub

    Private Sub RemoveDupeFromDatabase()
        Cursor = Cursors.WaitCursor
        Try

            Dim sSurveyType As String = GetSurveyType(lvData.SelectedItems(0).SubItems(eListviewColumns.eTable).Text.Trim)
            Dim sStore_Number As String = lvData.SelectedItems(0).SubItems(eListviewColumns.eStoreNo).Text.Trim
            Dim sInventoryDate As String = lvData.SelectedItems(0).SubItems(eListviewColumns.eImpDate).Text.Trim
            Dim sSQL As String = ""

            sSQL = "EXEC p_RemoveSurveyData " &
                   "@Survey_Type = N'" & sSurveyType & "', " &
                   "@Store_Number = " & sStore_Number & ", " &
                   "@Inventory_Date = N'" & sInventoryDate & "'"

            Call executeQuery(oConn.ConnectionString, sSQL)

            'Force Refresh all listview items that have same store number
            For Each lvItem As ListViewItem In lvData.Items
                If lvItem.SubItems(eListviewColumns.eStoreNo).Text = sStore_Number Then
                    Call SetStatus("", lvItem)
                End If
            Next

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)


        End Try
        Cursor = Cursors.Default
    End Sub

    Private Sub RemoveAllDupesFromDatabase()

        Dim oDupes As IEnumerable(Of ListViewItem) = Nothing
        Dim nDupes, nDupesRemoved As Integer
        Dim ret As Integer
        Dim sSurveyType As String
        Dim nStore_Number As Integer
        Dim sStore_Number As String
        Dim dtInventoryDate As Date
        Dim cmd As SqlClient.SqlCommand
        Dim listStoreNums As List(Of Integer)

        Cursor = Cursors.WaitCursor
        tspb.Visible = True
        tspb.Value = 0
        lblStatus.Text = "Duplicates Removal Progress"
        StatusStrip1.Refresh()


        Try
            listStoreNums = New List(Of Integer)
            oDupes = From lvItem In lvData.Items.Cast(Of ListViewItem)() Where lvItem.SubItems(eListviewColumns.eDupe).Text = "DUPE"
            nDupes = oDupes.Count
            nDupesRemoved = 0
            cmd = New SqlClient.SqlCommand("p_RemoveSurveyData", oConn.Connection)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@Survey_Type", SqlDbType.NVarChar)
            cmd.Parameters.Add("@Store_Number", SqlDbType.Int)
            cmd.Parameters.Add("@Inventory_Date", SqlDbType.DateTime)
            cmd.Connection.Open()

            For Each lvDupeItem As ListViewItem In oDupes
                sSurveyType = GetSurveyType(lvDupeItem.SubItems(eListviewColumns.eTable).Text.Trim)
                sStore_Number = lvDupeItem.SubItems(eListviewColumns.eStoreNo).Text.Trim
                nStore_Number = CInt(sStore_Number)
                dtInventoryDate = CDate(lvDupeItem.SubItems(eListviewColumns.eImpDate).Text.Trim)
                'Dim sSQL As String = ""

                ' sSQL = "EXEC p_RemoveSurveyData " &
                '       "@Survey_Type = N'" & sSurveyType & "', " &
                '       "@Store_Number = " & sStore_Number & ", " &
                '       "@Inventory_Date = N'" & sInventoryDate & "'"

                ' Call executeQuery(oConn.ConnectionString, sSQL)

                cmd.Parameters("@Survey_Type").Value = sSurveyType
                cmd.Parameters("@Store_Number").Value = nStore_Number
                cmd.Parameters("@Inventory_Date").Value = dtInventoryDate

                ret = cmd.ExecuteNonQuery

                Call SetStatus("", lvDupeItem)

                If listStoreNums.BinarySearch(sStore_Number) <= -1 Then
                    ' JR - 10/03/2017
                    ' Add number to list as after duplicates are removed from database the corresponding items in the list will need status reset
                    Call listStoreNums.Add(sStore_Number)
                End If


                ' Update progress bar to record duplicate as being removed
                nDupesRemoved += 1
                tspb.Value = (nDupesRemoved / nDupes) * 100
            Next
            'Force Refresh all listview items that have same store number

            For Each lvItem As ListViewItem In lvData.Items
                lvItem.Selected = True
                lvItem.Focused = True
                sStore_Number = lvItem.SubItems(eListviewColumns.eStoreNo).Text
                If listStoreNums.BinarySearch(sStore_Number) >= 0 Then
                    Call SetStatus("", lvItem)
                    Call lvData.Update()
                    Call lvData.Refresh()
                End If
            Next


        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)

        Finally
            cmd.Connection.Close()
        End Try

        Cursor = Cursors.Default
        tspb.Visible = False
        Call MsgBox("A total of " + nDupesRemoved.ToString() + " were removed from the database.", MsgBoxStyle.Information)
        lblStatus.Text = ""
        btnDeDupe.Enabled = False

    End Sub

    Private Function GetSurveyType(ByVal sTablename As String) As String

        Dim sSurveyType As String = "Full"

        Try

            Select Case sTablename
                Case eDestinationTables.Inventory_Data_TA_OUT15.ToString, eDestinationTables.NOSF_Inventory_Data_TA_OUT15.ToString
                    sSurveyType = "TA"
            End Select

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        Finally
            GetSurveyType = sSurveyType
        End Try

    End Function

    Private Sub BatchRebuildDataToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BatchRebuildDataToolStripMenuItem.Click
        Try

            Dim frm As New frmBatchBuild
            frm.ShowDialog()

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub RunWeeklyStoredProcedureToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RunWeeklyStoredProcedureToolStripMenuItem.Click
        Call RunStoredProcedure("SAINSBURY_DASHBOARD_VIEWDATA")
    End Sub

    Private Sub RunClothingStoredProcedureToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RunClothingStoredProcedureToolStripMenuItem.Click
        Call RunStoredProcedure("usp_Clothing_Data_Build")
    End Sub

    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        Call Forms.Application.Exit()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnDeDupe.Click
        Call RemoveAllDupesFromDatabase()
    End Sub

    Private Sub Label3_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub gbDates_Enter(sender As Object, e As EventArgs) Handles gbDates.Enter

    End Sub

    Private Sub chkSkipPreLoad_CheckedChanged(sender As Object, e As EventArgs) Handles chkSkipPreLoad.CheckedChanged
        If chkSkipPreLoad.Checked Then
            chkSkipClothingImport.Checked = False
            chkSkipClothingImport.Enabled = False

            chkSkipSuperCatVerifierImport.Checked = False
            chkSkipSuperCatVerifierImport.Enabled = False

        Else
            chkSkipClothingImport.Enabled = True
            chkSkipSuperCatVerifierImport.Enabled = True
        End If
    End Sub

    Private Sub btnSupercat_Click(sender As Object, e As EventArgs)
        ' Temporary button to test changes to supercat verifier import file
        ImportSuperCatVerifierDataOut59()
    End Sub

    Private Sub chkSkipSuperCatVerifierImport_CheckedChanged(sender As Object, e As EventArgs) Handles chkSkipSuperCatVerifierImport.CheckedChanged

    End Sub

    Private Sub chkSkipClothingImport_CheckedChanged(sender As Object, e As EventArgs) Handles chkSkipClothingImport.CheckedChanged

    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click
        ImportSuperCatVerifierData()

    End Sub
End Class
