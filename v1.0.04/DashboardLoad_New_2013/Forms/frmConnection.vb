
Public Class frmConnection
#Region "Properties"
    Private mSettings As clsConnection
    Private mChanged As Boolean
    Private mTableName As String
    Public Property udpTableName() As String
        Get
            Return mTableName
        End Get
        Set(ByVal value As String)
            mTableName = value
        End Set
    End Property
    Public Property udpConnection() As clsConnection
        Get
            Return mSettings
        End Get
        Set(ByVal value As clsConnection)
            mSettings = value
        End Set
    End Property
#End Region

#Region "Form Events"
    Private Sub frmConnection_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.txtServer.Text = mSettings.Server
            Me.txtDB.Text = mSettings.Database
            Me.radTrusted.Checked = Me.mSettings.TrustedConnection
            Me.radSQL.Checked = Not (Me.mSettings.TrustedConnection)
            Me.txtUser.Text = Me.mSettings.User
            Me.txtPwd.Text = Me.mSettings.Pwd

        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "Button Events"
    Private Sub btnTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTest.Click
        Try
            Dim sConnStr As String

            sConnStr = GenerateConnectionString()

            If TestConnection(sConnStr) Then
                If MsgBox("Connected, would you like to use these settings?", MsgBoxStyle.YesNoCancel + MsgBoxStyle.Question, "Database Connection") = MsgBoxResult.Yes Then
                    Me.DialogResult = Windows.Forms.DialogResult.OK
                    Me.mSettings.ConnectionString = sConnStr
                    Me.mSettings.Server = Me.txtServer.Text
                    Me.mSettings.Database = Me.txtDB.Text
                    Me.mSettings.Provider = "MSDASQL"
                    Me.mSettings.TrustedConnection = Me.radTrusted.Checked

                    Me.mSettings.User = Me.txtUser.Text
                    Me.mSettings.Pwd = Me.txtPwd.Text

                    Call SaveConnectionToRegistry()
                    Me.DialogResult = Windows.Forms.DialogResult.OK
                    Me.Close()
                    Me.Dispose()
                End If
            Else
                MsgBox("Unable to establish connection using on screen details", MsgBoxStyle.Exclamation)
            End If




        Catch ex As Exception
            MsgBox(Err.Description, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
        Me.Dispose()
    End Sub
#End Region

#Region "Other Functions"
    Private Function TestConnection(ByVal sConnstr As String) As Boolean
        Dim bOK As Boolean = False
        Me.Cursor = Cursors.WaitCursor

        Try
            Dim oConn As IDbConnection = Nothing

            If CreateConnection(sConnstr, oConn, "MSDASQL") Then
                bOK = True
                '    Dim dt As DataTable
                '    Dim sSQL As String = _
                '    "SELECT DISTINCT TABLE_NAME " & _
                '    "FROM INFORMATION_SCHEMA.COLUMNS " & _
                '    "WHERE     (TABLE_NAME = '" & mTableName & "') AND (Table_Catalog = '" & Me.txtDB.Text.Replace("'", "''") & "') "

                '    dt = modDB.GetDataTableWithConn(oConn, sSQL, "MSDASQL")

                '    If dt.Rows.Count > 0 Then
                '        Return True
                '    Else
                '        Throw New Exception("Invalid database.")
                '    End If
                'Else
                '    Return False
            End If

        Catch ex As Exception
            Throw New Exception(Err.Description)
        Finally
            TestConnection = bOK
        End Try
        Me.Cursor = Cursors.Default

    End Function

    Private Function GenerateConnectionString() As String
        Try
            Dim sStr As String = "Server = " & Me.txtServer.Text

            sStr = sStr & ";Database = " & Me.txtDB.Text
            sStr = sStr & ";UID = " & Me.txtUser.Text
            sStr = sStr & ";PWD = " & Me.txtPwd.Text
            sStr = sStr & ";Trusted_Connection = " & Me.radTrusted.Checked

            Return sStr
        Catch ex As Exception
            Return ""
        End Try
    End Function
#End Region

#Region "Radiobutton events"
    Private Sub radSQL_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radSQL.CheckedChanged
        Try
            Me.txtUser.Enabled = radSQL.Checked
            Me.txtPwd.Enabled = radSQL.Checked
        Catch ex As Exception

        End Try
    End Sub

    Private Sub radTrusted_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radTrusted.CheckedChanged
        If radTrusted.Checked Then
            Me.txtUser.Text = ""
            Me.txtPwd.Text = ""
        End If
    End Sub
#End Region

End Class