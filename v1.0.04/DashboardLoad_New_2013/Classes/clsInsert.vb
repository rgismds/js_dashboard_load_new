﻿Friend Class clsInsert

    '-- will need to add more for checkboxes, radio buttons etc

#Region "Declares"

    Private Structure udtBuildingOptions
        Dim sTableName As String
        Dim sField As String
        Dim sValue As String
    End Structure

    Private udtOptions As udtBuildingOptions

#End Region

    '-- new constructor
    Friend Sub New()

        With udtOptions
            .sField = ""
            .sValue = ""
            .sTableName = ""
        End With

    End Sub

    '-- resets all the values
    Friend Sub Reset()

        With udtOptions
            .sField = ""
            .sValue = ""
            .sTableName = ""
        End With

    End Sub

    '-- gets the table name for inserting into
    Friend Sub TableName(ByVal sName As String)
        udtOptions.sTableName = sName
    End Sub

    '-- passed a value as a checkbox and if not blank then builds insert query
    Friend Sub BuildInsert(ByVal sColumnName As String, ByVal sValue As CheckBox)

        Try

            udtOptions.sField += sColumnName & ", "
            udtOptions.sValue += "" & Convert.ToByte(sValue.Checked) & ", "

        Catch ex As Exception
        End Try

    End Sub

    '-- passed a value as a boolean and if not blank then builds insert query
    Friend Sub BuildInsert(ByVal sColumnName As String, ByVal sValue As Boolean)

        Try

            udtOptions.sField += sColumnName & ", "
            udtOptions.sValue += "" & Convert.ToByte(sValue) & ", "

        Catch ex As Exception
        End Try

    End Sub

    '-- passed a value as a string and if not blank then builds insert query
    Friend Sub BuildInsert(ByVal sColumnName As String, ByVal sValue As String, Optional ByVal bNulls As Boolean = True)

        Try

            If sValue <> "" Then
                udtOptions.sField += sColumnName & ", "
                udtOptions.sValue += "'" & sValue.Replace("'", "''") & "', "
            ElseIf bNulls Then
                udtOptions.sField += sColumnName & ", "
                udtOptions.sValue += " NULL, "
            End If

        Catch ex As Exception
        End Try

    End Sub

    '-- passed a value as a datetime and builds query
    Friend Sub BuildInsert(ByVal sColumnName As String, ByVal sValue As DateTime)

        Try

            If sValue.ToString() <> "" Then
                udtOptions.sField += sColumnName & ", "
                udtOptions.sValue += " Convert(DateTime,'" & Format(sValue, "yyyy-MM-dd hh:mm:ss") & "', 102), "
            End If

        Catch ex As Exception
        End Try

    End Sub

    ''-- passed a long and if greater than 0 then build query
    'Public Sub BuildInsert(ByVal sColumnName As String, ByVal sValue As Long, Optional ByVal bNulls As Boolean = True)

    '    Try

    '        If IsNumeric(sValue) Then
    '            udtOptions.sField += sColumnName & ", "
    '            udtOptions.sValue += sValue & ", "
    '        ElseIf bNulls Then
    '            udtOptions.sField += sColumnName & ", "
    '            udtOptions.sValue += " NULL, "
    '        End If

    '    Catch ex As Exception
    '    End Try

    'End Sub

    '-- passed a long and if greater than 0 then build query
    Friend Sub BuildInsert(ByVal sColumnName As String, ByVal sValue As Double, Optional ByVal bNulls As Boolean = True, Optional ByVal sNull As String = " NULL, ")

        Try
            If IsNumeric(sValue) Then
                udtOptions.sField += sColumnName & ", "
                udtOptions.sValue += sValue & ", "
            ElseIf bNulls Then
                udtOptions.sField += sColumnName & ", "
                udtOptions.sValue += sNull
            End If

        Catch ex As Exception
        End Try

    End Sub

    '-- builds the sql for returning
    Friend Function GetSql() As String
        Dim strSql As String

        ''-- remove any last ',' from the string
        udtOptions.sField = StripCommas(udtOptions.sField) & " VALUES "

        ''-- remove any last ',' from the string
        udtOptions.sValue = StripCommas(udtOptions.sValue)

        strSql = "INSERT INTO " & udtOptions.sTableName & " " & udtOptions.sField & udtOptions.sValue

        Return strSql

    End Function

    '-- takes out the commas for building from the buildinserts
    Private Function StripCommas(ByVal strValue As String) As String
        Dim sReturn As String

        sReturn = "( " & strValue.Substring(0, Len(strValue) - 2) & " ) "

        Return sReturn
    End Function

End Class

Friend Class clsUpdate

    '-- will need to add more for checkboxes, radio buttons etc

#Region "Declares"

    Private Structure udtBuildingOptions
        Dim sTableName As String
        Dim sField As String
        Dim sValue As String
        Dim sWhere As String
    End Structure

    Private udtOptions As udtBuildingOptions

#End Region

    '-- new constructor
    Friend Sub New()

        With udtOptions
            .sField = ""
            .sValue = ""
            .sTableName = ""
            .sWhere = ""
        End With

    End Sub

    '-- resets all the values
    Friend Sub Reset()

        With udtOptions
            .sField = ""
            .sValue = ""
            .sTableName = ""
            .sWhere = ""
        End With

    End Sub

    '-- gets the table name for inserting into
    Friend Sub TableName(ByVal sName As String)
        udtOptions.sTableName = sName
    End Sub


    '-- gets the table name for inserting into
    Friend Sub Where(ByVal sWhere As String)
        udtOptions.sWhere = sWhere
    End Sub

    '-- passed a value as a checkbox and if not blank then builds insert query
    Friend Sub BuildUpate(ByVal sColumnName As String, ByVal sValue As CheckBox)

        Try

            udtOptions.sField += sColumnName & "¦ "
            udtOptions.sValue += "" & Convert.ToByte(sValue.Checked) & "¦ "

        Catch ex As Exception
        End Try

    End Sub

    '-- passed a value as a boolean and if not blank then builds insert query
    Friend Sub BuildUpate(ByVal sColumnName As String, ByVal sValue As Boolean)

        Try

            udtOptions.sField += sColumnName & "¦ "
            udtOptions.sValue += "" & Convert.ToByte(sValue) & "¦ "

        Catch ex As Exception
        End Try

    End Sub

    '-- passed a value as a string and if not blank then builds insert query
    Friend Sub BuildUpate(ByVal sColumnName As String, ByVal sValue As String, Optional ByVal bNulls As Boolean = True)

        Try

            If sValue <> "" Then
                udtOptions.sField += sColumnName & "¦ "
                udtOptions.sValue += "'" & sValue.Replace("'", "''") & "'¦ "
            ElseIf bNulls Then
                udtOptions.sField += sColumnName & "¦ "
                udtOptions.sValue += " NULL¦ "
            End If

        Catch ex As Exception
        End Try

    End Sub

    '-- passed a value as a datetime and builds query
    Friend Sub BuildUpate(ByVal sColumnName As String, ByVal dValue As DateTime)

        Try

            If dValue.ToString() <> "" Then
                udtOptions.sField += sColumnName & "¦ "
                udtOptions.sValue += " CONVERT(DATETIME, '" & Format(dValue, "yyyy-MM-dd HH:mm:ss") & "',102)¦ "
            End If

        Catch ex As Exception
        End Try

    End Sub

    '-- passed a long and if greater than 0 then build query
    Friend Sub BuildUpate(ByVal sColumnName As String, ByVal sValue As Double, Optional ByVal bNulls As Boolean = True, Optional ByVal sNull As String = " NULL, ")

        Try
            If IsNumeric(sValue) Then
                udtOptions.sField += sColumnName & "¦ "
                udtOptions.sValue += sValue & "¦ "
            ElseIf bNulls Then
                udtOptions.sField += sColumnName & "¦ "
                udtOptions.sValue += sNull
            End If

        Catch ex As Exception
        End Try

    End Sub

    '-- builds the sql for returning
    Friend Function GetSql() As String
        Dim sSql As String = ""
        Dim sFields() As String
        Dim sValues() As String
        Dim sComma As String = ""

        Try

            ''-- remove any last ',' from the string
            udtOptions.sField = StripCommas(udtOptions.sField)
            sFields = udtOptions.sField.Split("¦")

            ''-- remove any last ',' from the string
            udtOptions.sValue = StripCommas(udtOptions.sValue)
            sValues = udtOptions.sValue.Split("¦")

            For i As Integer = 0 To sFields.GetUpperBound(0)
                sSql &= sComma & sFields(i).Trim & " = " & sValues(i).Trim
                sComma = ", "
            Next

            sSql = "UPDATE " & udtOptions.sTableName & " SET " & sSql & " " & udtOptions.sWhere

        Catch ex As Exception
        Finally
            GetSql = sSql
        End Try

    End Function

    '-- takes out the commas for building from the buildinserts
    Private Function StripCommas(ByVal strValue As String) As String
        Dim sReturn As String

        sReturn = strValue.Substring(0, Len(strValue) - 2)
        Return sReturn
    End Function

End Class
