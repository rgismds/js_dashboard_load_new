﻿Imports System.Data.SqlClient

Public Class clsLogItem

    Dim xoCmd As SqlCommand
    Dim DBConn As SqlConnection

    Public Sub New(sConnection As String)
        DBConn = New SqlConnection(oConn.ConnectionString)
        xoCmd = New SqlCommand
    End Sub



    Public Sub InsertLogRecord(nImportTaskId, nImportEventType, sEventDescription)
        If DBConn.State = ConnectionState.Closed Then
            DBConn.Open()
            xoCmd.Connection = DBConn
            xoCmd.CommandType = CommandType.StoredProcedure
            xoCmd.CommandText = "p_ImportEvent_Insert"
            xoCmd.Parameters.Add("ImportTaskId", SqlDbType.Int)
            xoCmd.Parameters.Add("ImportEventType", SqlDbType.Int)
            xoCmd.Parameters.Add("ImportEventDesc", SqlDbType.VarChar)
            xoCmd.Parameters.Add("DateCreated", SqlDbType.DateTime)
        End If
        xoCmd.Parameters("ImportTaskId").Value = nImportTaskId
        xoCmd.Parameters("ImportEventType").Value = nImportEventType
        xoCmd.Parameters("ImportEventDesc").Value = sEventDescription
        xoCmd.Parameters("DateCreated").Value = Now
        xoCmd.ExecuteNonQuery()
    End Sub


End Class
