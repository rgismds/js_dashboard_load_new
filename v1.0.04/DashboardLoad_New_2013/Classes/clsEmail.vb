﻿Imports System.Configuration
Imports Microsoft.Office
Imports Microsoft.Office.Interop.Outlook

Public Class clsEmail


    Public Sub SendEmail(sEmailTo As String, sEmailCc As String, sSubject As String, sBody As String, _
                     sFilename As String, sDisplayname As String)

        Dim oApp As Interop.Outlook._Application
        oApp = New Interop.Outlook.Application

        Dim oMsg As Interop.Outlook._MailItem
        oMsg = oApp.CreateItem(Interop.Outlook.OlItemType.olMailItem)

        oMsg.Subject = sSubject
        oMsg.Body = sBody

        oMsg.To = sEmailTo
        oMsg.CC = sEmailCc


        Dim strS As String = sFilename
        Dim strN As String = sDisplayname
        If sFilename <> "" Then
            Dim sBodyLen As Integer = Int(sBody.Length)
            Dim oAttachs As Interop.Outlook.Attachments = oMsg.Attachments
            Dim oAttach As Interop.Outlook.Attachment

            oAttach = oAttachs.Add(strS, sBodyLen, strN)

        End If

        oMsg.Send()

    End Sub
End Class
