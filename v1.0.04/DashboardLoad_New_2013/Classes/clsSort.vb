Option Explicit On

Imports System.Collections
Imports System.Windows.Forms

Public Class ListViewColumnSorter
    Implements System.Collections.IComparer
    Private m_ColumnToSort As Integer, m_SortType As Integer
    Private m_OrderOfSort As SortOrder
    Private ObjectCompare As CaseInsensitiveComparer

    Public Sub New(ByVal SortCol As Integer, ByVal SortOrder As SortOrder, ByVal SortType As Integer)
        Try
            ' Initialize the column to '0'.
            m_ColumnToSort = SortCol

            ' Initialize the sort order to 'none'.
            m_OrderOfSort = SortOrder

            ' Initialize the sort order to '0'.
            m_SortType = SortType

            ' Initialize the CaseInsensitiveComparer object.
            ObjectCompare = New CaseInsensitiveComparer()

        Catch ex As Exception

        End Try
    End Sub

    '********************************************************************
    ' Purpose:  Compare the items in a certain column of the listview to sort them in asceding/descending order
    ' Author:   Xin Zhao - Microsoft original code from http://support.microsoft.com/kb/319399
    ' Date:     05/03/2008
    ' Inputs:   Items in the listview
    ' Returns:  Sorted items in the listview
    '********************************************************************

    Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements IComparer.Compare
        Dim compareResult As Integer
        Try

            Dim listviewX As ListViewItem
            Dim listviewY As ListViewItem

            ' Cast the objects to be compared to ListViewItem objects.
            listviewX = CType(x, ListViewItem)
            listviewY = CType(y, ListViewItem)

            ' Compare the two items.
            Select Case m_SortType
                Case 0
                    'If the column is in string type
                    compareResult = ObjectCompare.Compare(listviewX.SubItems(m_ColumnToSort).Text, listviewY.SubItems(m_ColumnToSort).Text)
                Case 1
                    'If the column is in number type
                    '"" is less than any other numbers
                    If Len(listviewX.SubItems(m_ColumnToSort).Text) = 0 And Len(listviewY.SubItems(m_ColumnToSort).Text) = 0 Then
                        compareResult = 0
                    ElseIf Len(listviewX.SubItems(m_ColumnToSort).Text) = 0 Then
                        compareResult = -1
                    ElseIf Len(listviewY.SubItems(m_ColumnToSort).Text) = 0 Then
                        compareResult = 1
                    Else
                        compareResult = ObjectCompare.Compare(CDbl(listviewX.SubItems(m_ColumnToSort).Text), CDbl(listviewY.SubItems(m_ColumnToSort).Text))
                    End If
                Case 2
                    'If the column is in date type
                    '"" is the less than any other dates
                    If Len(listviewX.SubItems(m_ColumnToSort).Text) = 0 And Len(listviewY.SubItems(m_ColumnToSort).Text) = 0 Then
                        compareResult = 0
                    ElseIf Len(listviewX.SubItems(m_ColumnToSort).Text) = 0 Then
                        compareResult = -1
                    ElseIf Len(listviewY.SubItems(m_ColumnToSort).Text) = 0 Then
                        compareResult = 1
                    Else
                        compareResult = ObjectCompare.Compare(CDate(listviewX.SubItems(m_ColumnToSort).Text), CDate(listviewY.SubItems(m_ColumnToSort).Text))
                    End If
                Case Else
                    compareResult = ObjectCompare.Compare(listviewX.SubItems(m_ColumnToSort).Text, listviewY.SubItems(m_ColumnToSort).Text)
            End Select

            ' Calculate the correct return value based on the object 
            ' comparison.
            If (m_OrderOfSort = SortOrder.Ascending) Then
                ' Ascending sort is selected, return typical result of 
                ' compare operation.
                compareResult = compareResult

            ElseIf (m_OrderOfSort = SortOrder.Descending) Then
                ' Descending sort is selected, return negative result of 
                ' compare operation.
                compareResult = (-compareResult)

            Else
                ' Return '0' to indicate that they are equal.
                compareResult = 0

            End If

        Catch ex As Exception
        Finally
            Compare = compareResult
        End Try

    End Function

    Public Property SortColumn() As Integer
        Set(ByVal Value As Integer)
            m_ColumnToSort = Value
        End Set
        Get
            Return m_ColumnToSort
        End Get

    End Property

    Public Property Order() As SortOrder
        Set(ByVal Value As SortOrder)
            m_OrderOfSort = Value
        End Set

        Get
            Return m_OrderOfSort
        End Get

    End Property

    Public Property Type() As Integer

        Set(ByVal Value As Integer)
            m_SortType = Value
        End Set

        Get
            Return m_SortType
        End Get

    End Property

End Class



