﻿Public Class clsCboItem

    Public Description As String = ""
    Public DbName As String = ""

    Public Sub New(sDesc As String, sDbName As String)
        Description = sDesc
        DbName = sDbName
    End Sub

    Public Function GetDbName() As String
        Return DbName
    End Function

    Public Overrides Function ToString() As String
        Return Description
    End Function

End Class
