﻿Public Class clsErrorlogging

#Region "Properties"

    ''' <summary>
    ''' Provide a string of cols seperated by "¬"
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>

    Public WriteOnly Property p_Columns() As String
        Set(ByVal value As String)
            sColumns = value
            Call AddColumns()
        End Set
    End Property

    ''' <summary>
    ''' Provide a string of error details seperated by "¬"
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>

    Public WriteOnly Property p_Message() As String
        Set(ByVal value As String)
            sError = value
            Call AddErrorDetails()
        End Set
    End Property

    ''' <summary>
    '''  Provide a string of table names seperated by "¬"
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>

    Public WriteOnly Property p_Tables() As String
        Set(ByVal value As String)
            sTableNames = value
            Call AddTables()
        End Set
    End Property

    Private sPath As String
    Public WriteOnly Property p_Path() As String
        Set(ByVal value As String)
            sPath = value
        End Set
    End Property

#End Region

#Region "Declares"

    Private sError As String = ""
    Private sColumns As String = ""
    Private aCols As New ArrayList
    Private dstErrorLogs As DataSet
    Private sTableNames As String = ""

#End Region

#Region "Functions"

    Sub New()

        Try

            sError = ""
            sColumns = ""
            dstErrorLogs = New DataSet
            sTableNames = ""

        Catch ex As Exception
        End Try

    End Sub

    Public Sub Reset()

        Try

            sError = ""
            sColumns = ""
            aCols.Clear()

            dstErrorLogs.Relations.Clear()

            For Each dTable As DataTable In dstErrorLogs.Tables
                dTable.Constraints.Clear()
            Next

            dstErrorLogs.Tables.Clear()
            dstErrorLogs.Clear()

            sTableNames = ""
            sPath = ""

        Catch ex As Exception
        Finally
        End Try

    End Sub

    Public Sub RemoveTable(ByVal sTableName As String)

        Try
            dstErrorLogs.Tables.Remove(sTableName.Trim)
        Catch ex As Exception
        Finally
        End Try

    End Sub

    Private Sub AddTables()
        Dim sTables As New ArrayList

        Try

            sTables.AddRange(sTableNames.Split("¬"))

            For Each sTable As String In sTables
                dstErrorLogs.Tables.Add(sTable.Trim)
            Next

        Catch ex As Exception
        Finally
        End Try

    End Sub

    Private Sub AddErrorDetails()
        Dim sDetails As New ArrayList
        Dim i As Integer = 0
        Dim sTable As String = ""

        Try
            sDetails.AddRange(sError.Split("¬"))
            sTable = sDetails(0).ToString.Trim
            Dim dRow As DataRow = dstErrorLogs.Tables(sTable).NewRow

            For Each sCol As DataColumn In dstErrorLogs.Tables(sTable).Columns

                dRow(sCol.ColumnName) = sDetails(i + 1).ToString.Trim
                i += 1

            Next

            dstErrorLogs.Tables(sTable).Rows.Add(dRow)

        Catch ex As ApplicationException
        Finally
        End Try

    End Sub

    Private Sub AddColumns()
        Dim sTable As String = ""
        Dim i As Integer = 0

        Try

            aCols.Clear()

            aCols.AddRange(sColumns.Split("¬"))
            sTable = aCols(0).ToString.Trim

            For Each sCol As String In aCols

                If i <> 0 Then
                    dstErrorLogs.Tables(sTable).Columns.Add(sCol.Trim)
                End If

                i += 1

            Next

        Catch ex As ApplicationException
        Finally
        End Try

    End Sub

    Public Sub AddRelation(ByVal sRelationshipName As String, ByVal sParentTable As String, ByVal sChildTable As String, ByVal sCol As String, Optional ByVal bNested As Boolean = True)
        Dim oRelation As DataRelation
        Dim dColumn1 As DataColumn
        Dim dColumn2 As DataColumn

        Try

            dColumn1 = dstErrorLogs.Tables(sParentTable.Trim).Columns(sCol.Trim)
            dColumn2 = dstErrorLogs.Tables(sChildTable.Trim).Columns(sCol.Trim)

            oRelation = New DataRelation(sRelationshipName, dColumn1, dColumn2)

            oRelation.Nested = bNested
            dstErrorLogs.Relations.Add(oRelation)

        Catch ex As Exception
        Finally
        End Try

    End Sub

    Public Sub WriteToXml()

        Try

            dstErrorLogs.WriteXml(sPath.Trim)

        Catch ex As Exception
        End Try

    End Sub

    Public Sub WriteToXml(ByVal sTable As String)

        Try

            dstErrorLogs.Tables(sTable.Trim).WriteXml(sPath.Trim)

        Catch ex As Exception
        End Try

    End Sub

    Public Function Count(ByVal sTable As String, Optional ByVal sWhere As String = "") As Integer
        Dim iCount As Integer = 0

        Try

            If sWhere.Length = 0 Then
                iCount = dstErrorLogs.Tables(sTable).Rows.Count
            Else
                Dim dRows() As DataRow = dstErrorLogs.Tables(sTable).Select(sWhere)
                iCount = dRows.Length
            End If

        Catch ex As Exception
        Finally
            Count = iCount
        End Try
    End Function

    ''' <summary>
    ''' Will remove entry from table if sObject is present in Col 1
    ''' </summary>
    ''' <param name="sTable">Table name</param>
    ''' <param name="sObject">Column 1 string/key to remove from table</param>
    ''' <remarks></remarks>
    ''' 
    Public Sub RemoveData(ByVal sTable As String, ByVal sObject As String)

        Dim dTable As DataTable = dstErrorLogs.Tables(sTable)

        Try

            For i = dTable.Rows.Count - 1 To 0 Step -1
                If dTable.Rows(i)(0) = sObject Then
                    dTable.Rows.RemoveAt(i)
                End If
            Next

        Catch ex As Exception

        End Try

    End Sub

#End Region

End Class
