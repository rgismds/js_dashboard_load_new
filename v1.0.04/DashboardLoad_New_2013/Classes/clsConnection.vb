Public Class clsConnection

    'Public Enum STPL_Provider
    '    eMSDASQL = 0
    '    eSQLOLEDB = 1
    '    eSQLClient = 2
    'End Enum

    Private m_sConnString As String
    Private m_sServer As String
    Private m_sDatabase As String
    Private m_sProvider As String
    Private m_bTrusted As Boolean
    Private m_sUser As String
    Private m_sPWD As String
    Private m_bADONET As Boolean
    Private m_Conn As IDbConnection
    'Private mConnOptions As New STPL_Connection.STPL_Connection_Main
    'Public Property ConnOptions() As STPL_Connection.STPL_Connection_Main
    '    Get
    '        Return mConnOptions
    '    End Get
    '    Set(ByVal value As STPL_Connection.STPL_Connection_Main)
    '        mConnOptions = value
    '    End Set
    'End Property

    Public Property ConnectionString() As String
        Get
            Return m_sConnString
        End Get
        Set(ByVal value As String)
            m_sConnString = value
        End Set
    End Property

    Public Property Server() As String
        Get
            Return m_sServer
        End Get
        Set(ByVal value As String)
            m_sServer = value
        End Set
    End Property

    Public Property Database() As String
        Get
            Return m_sDatabase
        End Get
        Set(ByVal value As String)
            m_sDatabase = value
        End Set
    End Property

    Public Property Provider() As String
        Get
            Return m_sProvider
        End Get
        Set(ByVal value As String)
            m_sProvider = value
        End Set
    End Property

    Public Property User() As String
        Get
            Return m_sUser
        End Get
        Set(ByVal value As String)
            m_sUser = value
        End Set
    End Property

    Public Property Pwd() As String
        Get
            Return m_sPWD
        End Get
        Set(ByVal value As String)
            m_sPWD = value
        End Set
    End Property
    Public Property TrustedConnection() As Boolean
        Get
            Return m_bTrusted
        End Get
        Set(ByVal value As Boolean)
            m_bTrusted = value
        End Set
    End Property
    Public Property Connection() As IDbConnection
        Get
            Return m_Conn
        End Get
        Set(ByVal value As IDbConnection)
            m_Conn = value
        End Set
    End Property
    'Public Property ADONETConnection() As Boolean
    '    Get
    '        Return m_bADONET
    '    End Get
    '    Set(ByVal value As Boolean)
    '        m_bADONET = False
    '    End Set
    'End Property
End Class


